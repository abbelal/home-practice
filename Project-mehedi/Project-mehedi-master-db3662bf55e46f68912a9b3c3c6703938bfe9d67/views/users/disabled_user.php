<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_registration\user_registration;
$objAlluser = new user_registration();

use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

include_once '../header.php';
include_once 'menubar.php';

$allUserData = $objAlluser -> disable_user_list();
    $i = 1;
?>
<!-- Table -->
<div class="panel-flat">
    <!-- Grid -->
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel registration-form">
                <div class="panel-body">
                    <ul class="media-list media-list-linked">
                        <?php
                        foreach ($allUserData as $key => $singleUser) {
                        ?>
                        <div class = "panel">
                            <li class="media success">
                            <div class="media-link cursor-pointer 
                            <?php
                             echo ($i%2==0)?'alpha-brown':'alpha-slate';$i++;
                             ?>" data-toggle="collapse" data-target="#<?php echo $singleUser['unique_id'];?>">
                                <div class="media-left"><img src="../assets/images/user/<?php echo $singleUser['image'];?>" class="img-circle" alt=""></div>
                                <div class="media-body">
                                    <div class="media-heading text-semibold">
                                        <?php echo $singleUser['full_name'] ?>
                                    </div>
                                    <?php if($singleUser['is_admin'] == 1){ ?>
                                    <span class="label label-danger">Admin</span>
                                    <?php }else{ ?>
                                    <span class="label bg-indigo-400">User</span>
                                    <?php } ?>
                                </div>
                                <div class="media-right media-middle text-nowrap">
                                    <i class="icon-menu9 display-block"></i>
                                </div>
                            </div>
                            <div class="collapse" id="<?php echo $singleUser['unique_id'];?>">
                                <div class="contact-details row">
                                    <div class = "text-left col-md-6">
                                        <ul class="list-extended list-unstyled list-icons">
                                        <li><i class="icon-user-check position-left"></i> <?php echo $singleUser['is_active'] == 1 ? 'Active':'Inactive';?></li>
                                        <li><i class="icon-user-tie position-left"></i><?php echo $singleUser['username'];?></li>
                                        <li><i class="icon-database-insert position-left"></i> <?php echo $singleUser['created'];?></li>
                                        <li><i class="icon-mail5 position-left"></i> <a href="#"><?php echo $singleUser['email'];?></a></li>
                                    </ul>
                                    </div>
                                    <div class = "text-right col-md-6">
                                        <div class="btn-group">
                                            <a href="single_user.php?id=<?php echo $singleUser['unique_id'];?>" class="btn btn-default" type="button"><i class="icon-enlarge6  position-left"></i> View Details</a>

                                            <a href="edit_user.php?id=<?php echo $singleUser['unique_id'];?>" class="btn btn-default" type="button"><i class="icon-pencil7 position-left"></i> Edit User</a>

                                            <a href="restore_user.php?id=<?php echo $singleUser['unique_id'];?>" class="btn btn-default" type="button"><i class="icon-reset position-left"></i> Enable User</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </li>
                        </div>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <!-- /grid -->
    <?php include_once 'footer.php' ?>