<?php
include_once "../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();

$obj->prepare($_GET);

$onedata = $obj->show();

//print_r($onedata);

if (isset($_SESSION['Login_data']) && !empty($_SESSION['Login_data'])) {
    ?>
    <html>
    <head>
        <title>Index | Page</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
    <?php include_once "include/log-navbar.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-top: 50px">
                <h3 class="text-center">User List</h3>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th colspan="3" class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?php echo $onedata['id'] ?></td>
                        <td><?php echo $onedata['user_name'] ?></td>
                        <td><?php echo $onedata['email'] ?></td>
                        <td><?php echo $onedata['password'] ?></td>
                        <td><a href="pro-update.php?id=<?php echo $onedata['unique_id'] ?>">Edit</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </body>
    </html>
    <?php
} else {
    $_SESSION['Errors_R'] = "User not found :(";
    header("location:errors.php");
}
?>

