<?php
	
	class DatabaseConnection
		{
		public $conn;
		public function __construct(){
			if(!isset($_SESSION)){
				session_start();
			}

			try {
				$this -> conn = new PDO('mysql:host=localhost;dbname=final_project', 'root', '');
				$this -> conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			} catch (Exception $e) {
				echo "Connection failed: " . $e->getMessage();
			}
		}// Construct \\

		public function session_message($data = ''){
			if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
				echo $_SESSION["$data"];
				unset($_SESSION["$data"]);
			}
		}// session_message

		public function test(){
			echo "hi baby its working";
		}

	}// Class \\
?>