<?php

include_once '../vendor/autoload.php';

use signupForm\resistrationForm\resistrationForm;

$object = new resistrationForm();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    //for user name
    if (!empty($_POST['userName'])) {
        $object->prepare($_POST);
    } else {
        $_SESSION['errName'] = "Enter Username";
        header('location:login.php');
    }

    //for pasword
    if (!empty($_POST['password'])) {
        $object->prepare($_POST);
    } else {
        $_SESSION['errPassword'] = "Enter Password";
        header('location:login.php');
    }
    $object->login();
} else {
    $_SESSION['errMsg'] = "Invalid location";
    header('location:login.php');
}
?>
