<?php
require_once '../../vendor/autoload.php';

use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BITM</title>

    <!-- Global stylesheets -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> -->
    <link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="../assets/js/core/app.js"></script>
    <script type="text/javascript" src="../assets/js/pages/login.js"></script>
    <!-- /theme JS files -->

</head>

<body>

    <!-- Page container -->
    <div class="page-container login-container">
        <?php 
            if(isset($_SESSION['loginFirst']) && !empty($_SESSION['loginFirst'])){
        ?>
            <div class="alert alpha-slate border-slate alert-styled-left">
                <button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button><h6 class="text-center text-slate-800">
                <?php $objLoginUser-> session_message('loginFirst');?></h6>
            </div>
        <?php
            }
         ?>
    
        <!-- Page content -->
        <div class="page-content">


            <!-- Main content -->
            <div class="content-wrapper">



                <!-- Content area -->
                    <!-- Advanced login -->
                    <form action="login_action.php" method="POST">
                        <div class="panel panel-body login-form">
                            <div class="text-center">
                                <div class="icon-object border-teal-800 text-teal-800"><i class="icon-lock"></i></div>
                                <h5 class="content-group-lg">Login to your account <small class="display-block">Enter your Username & Password</small></h5>
                            </div>

                            <div class="form-group has-feedback has-feedback-left">
                                <input type="text" class="form-control" placeholder="Username" name="username">
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group has-feedback has-feedback-left">
                                <input type="password" class="form-control" placeholder="Password" name="password">
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>

                            <?php
                                    if(isset($_SESSION['logginError']) || isset($_SESSION['accountSuspended']) || isset($_SESSION['emptyField'])){
                                ?>
                                    <label class="validation-error-label text-left">
                                <?php 
                                    $objLoginUser->session_message('logginError');
                                    $objLoginUser->session_message('accountSuspended');
                                    $objLoginUser->session_message('emptyField');
                                ?>
                                    <br></label>

                                <?php
                                    }
                                ?>

                            
                            <div class="form-group">
                                <input type="submit" class="btn bg-teal-400 btn-block " value="Login">
                            </div>

                            

                            <span class="help-block text-center no-margin">If you don't have an account contact with BITM admin panel. Only admin can proceed new account<span>
                        </div>
                    </form>
                    <!-- /advanced login -->

                    <?php include_once'footer.php'?>