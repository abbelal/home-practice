                        <ul class="breadcrumb-elements">
                            <li><a href="../trainers/add_trainers.php"><i class="icon-vcard position-left text-primary"></i> Add New Trainer</a></li>

                            <li><a href="../trainers/index.php"><i class="icon-people position-left text-primary"></i> View All Trainers</a></li>

                            <li><a href="../trainers/disabled_trainers.php"><i class="icon-user-cancel position-left text-primary"></i> Disabled Trainers</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">