<?php

namespace ProjectMehedi\trainers;

use PDO;

require_once '../../src/DatabaseConnection/DatabaseConnection.php';
// require(dirname(__FILE__).'../../../../src/DatabaseConnection/DatabaseConnection.php'); 

class trainers extends \DatabaseConnection{

	public $id;
	public $fullName;
	public $team;
	public $course_id;
	public $trainer_status;
	public $EduTitle;
	public $organization;
	public $passingYear;
	public $phone;
	public $email;
	public $website;
	public $houseAddress;
	public $district;
	public $zipCode;
	public $gender;
	public $image;
	public $created;
	public $modified;
	public $deleted;
	public $data;
	public $error;


	public function course_id_title(){

		 $query = "SELECT `id`, `title` FROM `courses` ";
		 $stmt = $this -> conn -> prepare($query);
		 $stmt -> execute();

		 while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		 	$this->data[] = $row;
		 }
		 return $this->data;

	}



	public function prepare($data = ""){

		if(!empty($data['id'])){
			$this->id = $data['id'];
		}
		if(!empty($data['fullName'])){
			$this->fullName = $data['fullName'];
		}
		if(!empty($data['team'])){
			$this->team = $data['team'];
		}
		if(!empty($data['course_id'])){
			$this->course_id = $data['course_id'];
		}
		if(!empty($data['trainer_status'])){
			$this->trainer_status = $data['trainer_status'];
		}
		if(!empty($data['EduTitle'])){
			$this->EduTitle = $data['EduTitle'];
		}
		if(!empty($data['organization'])){
			$this->organization = $data['organization'];
		}
		if(!empty($data['passingYear'])){
			$this->passingYear = $data['passingYear'];
		}
		if(!empty($data['phone'])){
			$this->phone = $data['phone'];
		}
		if(!empty($data['email'])){
			$this->email = $data['email'];
		}
		if(!empty($data['website'])){
			$this->website = $data['website'];
		}
		if(!empty($data['houseAddress'])){
			$this->houseAddress = $data['houseAddress'];
		}
		if(!empty($data['district'])){
			$this->district = $data['district'];
		}
		if(!empty($data['houseAddress'])){
			$this->houseAddress = $data['houseAddress'];
		}
		if(!empty($data['zipCode'])){
			$this->zipCode = $data['zipCode'];
		}
		if(!empty($data['gender'])){
			$this->gender = $data['gender'];
		}
		if(!empty($data['image'])){
			$this->image = $data['image'];
		}

		return $this;

		
	}// prepare \\



	public function required_validation(){

		// Fullname required validation goes here
		if(!empty($this->fullName) && isset($this->fullName)){
			$_SESSION['fullName'] = $this->fullName;
		}else{
			$_SESSION['fullName_required'] = "Fullname is required";
			$this->error = TRUE;
		}
		// team required validation goes here
		if(!empty($this->team) && isset($this->team)){
			$_SESSION['team'] = $this->team;
		}else{
			$_SESSION['team_required'] = "Team is required";
			$this->error = TRUE;
		}
		// course_id required validation goes here
		if(!empty($this->course_id) && isset($this->course_id)){
			$_SESSION['course_id'] = $this->course_id;
		}else{
			$_SESSION['course_id_required'] = "Course is required";
			$this->error = TRUE;
		}
		//trainer_status  required validation goes here
		if(!empty($this->trainer_status) && isset($this->trainer_status)){
			$_SESSION['trainer_status'] = $this->trainer_status;
		}else{
			$_SESSION['trainer_status_required'] = "Trainer Status is required";
			$this->error = TRUE;
		}
		// EduTitle required validation goes here
		if(!empty($this->EduTitle) && isset($this->EduTitle)){
			$_SESSION['EduTitle'] = $this->EduTitle;
		}else{
			$_SESSION['EduTitle_required'] = "Education Title is required";
			$this->error = TRUE;
		}
		// organization required validation goes here
		if(!empty($this->organization) && isset($this->organization)){
			$_SESSION['organization'] = $this->organization;
		}else{
			$_SESSION['organization_required'] = "You can add Organization later";
			
		}
		// passingYear required validation goes here
		if(!empty($this->passingYear) && isset($this->passingYear)){
			$_SESSION['passingYear'] = $this->passingYear;
		}else{
			$_SESSION['passingYear_required'] = "You can add Passing Year later";
		}	
		// phone required validation goes here
		if(!empty($this->phone) && isset($this->phone)){
			$_SESSION['phone'] = $this->phone;
		}else{
			$_SESSION['phone_required'] = "phone is required";
			$this->error = TRUE;
		}
		// email required validation goes here
		if(!empty($this->email) && isset($this->email)){
			if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
				$unique_email_query = "SELECT * FROM `trainers` WHERE `email`= '".$this->email."'";
				$stmt = $this->conn->prepare($unique_email_query);
				$stmt -> execute();

				$email_row = $stmt->fetch(PDO::FETCH_ASSOC);
				if(!empty($email_row)){
					if($this->id != $email_row['unique_id']){
						$_SESSION['email_exists'] = "Email already exits";
						$this->error = TRUE;
					}
				}else{ // email_row emty!!
					$_SESSION['email'] = $this->email;
				}
			}else{ // format check of email
				$_SESSION['email_formateInvalid'] = "Invalid email formate";
				$this->error = TRUE;
			}
		}else{ // empty email
			$_SESSION['email_required'] = "Email must be required";
			$this->error = TRUE;
		}

		// website required validation goes here
		if(!empty($this->website) && isset($this->website)){

			if (filter_var($this->website, FILTER_VALIDATE_URL)){
				$_SESSION['website'] = $this->website;
			}else{
				$_SESSION['website_invalid'] = "Your website url is invalid";
				$this->error = TRUE;
			}
		}else{
			$_SESSION['website_required'] = "You can add website latter";
		}
		// House Address required validation goes here
		if(!empty($this->houseAddress) && isset($this->houseAddress)){
			$_SESSION['houseAddress'] = $this->houseAddress;
		}else{
			$_SESSION['houseAddress_required'] = "House Address is required";
			$this->error = TRUE;
		}
		// district required validation goes here
		if(!empty($this->district) && isset($this->district)){
			$_SESSION['district'] = $this->district;
		}else{
			$_SESSION['district_required'] = "District is required";
			$this->error = TRUE;
		}
		// zipCode required validation goes here
		if(!empty($this->zipCode) && isset($this->zipCode)){
			$_SESSION['zipCode'] = $this->zipCode;
		}else{
			$_SESSION['zipCode_required'] = "ZipCode is required";
			$this->error = TRUE;
		}

		$_SESSION['gender'] = $this->gender;
	
	} // required_validation \\


	public function add_image(){
		if(!empty($_FILES['image']['name']) && isset($_FILES['image'])){
			$this->image = uniqid().$_FILES['image']['name'];
			$image_type = $_FILES['image']['type'];
			$image_temp_location = $_FILES['image']['tmp_name'];
			$image_size = $_FILES['image']['size'];
			$require_extension = array('jpg', 'jpeg', 'gif', 'bmp', 'png');
			$image_extension = strtolower(end(explode('.', $this->image)));

			if(in_array($image_extension, $require_extension) == false){
				$_SESSION['ErrorImageExtension'] = 'Please give only image extension (.jpg, .png)';
				$error = TRUE;
				$this->error = TRUE;

			}
			if($image_size > 2000000){
				$_SESSION['ErrorImageSize'] = " Size must be below 2 MB";
				$error = TRUE;
				$this->error = TRUE;
			}
			if(empty($error) && $this->error == FALSE){
				move_uploaded_file($image_temp_location, "../assets/images/trainer/".$this->image."");
				// $_POST['image'] = $this->image;
			}
		}else{
			$_SESSION['NeedImage'] = "You can add image latter";
		}

	}// add_image \\


	public function addTrainer(){

		if($this->error == FALSE){

			try {

				$edu_array = array(
						$this->EduTitle,
						$this->organization,
						$this->passingYear,
						);
				$edu_status = serialize($edu_array);

				$address_array = array(
						$this->houseAddress,
						$this->district,
						$this->zipCode,
						);
				$address_status = serialize($address_array);

				$query = "INSERT INTO trainers (id, unique_id, full_name, edu_status, team, courses_id, trainer_status, image, phone, email, address, gender, web, is_delete, created) VALUES (:id, :unique_id, :full_name, :edu_status, :team, :courses_id, :trainer_status, :image, :phone, :email, :address, :gender, :web, :is_delete, :created)";

					$stmt = $this->conn -> prepare($query);
					if($stmt -> execute(array(

						':id' => null,
						':unique_id' => uniqid(),
						':full_name' => $this-> fullName,
						':edu_status' => $edu_status,
						':team' => $this-> team ,
						':courses_id' => $this-> course_id,
						':trainer_status' => $this-> trainer_status,
						':image' => (isset($this-> image) ? $this-> image : ''),
						':phone' => (isset($this-> phone) ? $this-> phone : ''),
						':email' => (isset($this-> email) ? $this-> email : ''),
						':address' => $address_status,
						':gender' => $this-> gender,
						':web' => (isset($this-> website) ? $this-> website : ''),
						':is_delete' => '0',
						':created' => date("Y-m-d h:i:s"),
						))){

						header('location:add_trainers.php');
						unset($_SESSION['fullName']);
						unset($_SESSION['team']);
						unset($_SESSION['course_id']);
						unset($_SESSION['trainer_status']);
						unset($_SESSION['EduTitle']);
						unset($_SESSION['organization']);
						unset($_SESSION['passingYear']);
						unset($_SESSION['phone']);
						unset($_SESSION['email']);
						unset($_SESSION['website']);
						unset($_SESSION['houseAddress']);
						unset($_SESSION['district']);
						unset($_SESSION['zipCode']);
						unset($_SESSION['gender']);
						unset($_SESSION['website_required']);
						unset($_SESSION['organization_required']);
						}
						$_SESSION['addSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Trainer Added Successfully. Thank You.</div>';
			} catch (Exception $e) {
				echo 'Error: ' . $e->getMessage();
				
			}
		}else{

			header('location:add_trainers.php');
		}

			
	}// addUser \\

	public function all_trainer(){

		$query = "SELECT `trainers`.*, `courses`.`title` FROM `trainers` LEFT JOIN `courses` ON `trainers`.`courses_id` = `courses`.`id` WHERE `trainers`.`is_delete` = '0'";
		$stmt = $this->conn->prepare($query);
		$stmt -> execute();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$this->data[] = $row;
		}
		return $this->data;

	} // all_user \\


	public function single_Trainer(){

		$query = "SELECT `trainers`.*, `courses`.`title` FROM `trainers` LEFT JOIN `courses` ON `trainers`.`courses_id` = `courses`.`id` WHERE `trainers`.`unique_id` = '".$this->id."'";

		$stmt = $this->conn->prepare($query);
		$stmt -> execute();

		$row = $stmt -> fetch(PDO::FETCH_ASSOC);
		return $row;

	}// Single_user\\



	public function update_trainer(){

		if($this->error == FALSE){

			try {

				$edu_array = array(
						$this->EduTitle,
						$this->organization,
						$this->passingYear,
						);
				$edu_status = serialize($edu_array);

				$address_array = array(
						$this->houseAddress,
						$this->district,
						$this->zipCode,
						);
				$address_status = serialize($address_array);

				$query = "UPDATE trainers SET full_name = :full_name, edu_status = :edu_status, team = :team, courses_id = :courses_id, trainer_status = :trainer_status, image = :image, phone = :phone, email = :email, address = :address, gender = :gender, web = :web, updated = :updated WHERE trainers.unique_id = :id";

					$stmt = $this->conn -> prepare($query);
					if($stmt -> execute(array(

						':full_name' => $this-> fullName,
						':edu_status' => $edu_status,
						':team' => $this-> team ,
						':courses_id' => $this-> course_id,
						':trainer_status' => $this-> trainer_status,
						':image' => (isset($this-> image) ? $this-> image : ''),
						':phone' => (isset($this-> phone) ? $this-> phone : ''),
						':email' => (isset($this-> email) ? $this-> email : ''),
						':address' => $address_status,
						':gender' => $this-> gender,
						':web' => (isset($this-> website) ? $this-> website : ''),
						':updated' => date("Y-m-d h:i:s"),
						':id' => $this-> id,
						))){

						header("location:edit_trainer.php?id=$this->id");
						unset($_SESSION['fullName']);
						unset($_SESSION['team']);
						unset($_SESSION['course_id']);
						unset($_SESSION['trainer_status']);
						unset($_SESSION['EduTitle']);
						unset($_SESSION['organization']);
						unset($_SESSION['passingYear']);
						unset($_SESSION['phone']);
						unset($_SESSION['email']);
						unset($_SESSION['website']);
						unset($_SESSION['houseAddress']);
						unset($_SESSION['district']);
						unset($_SESSION['zipCode']);
						unset($_SESSION['gender']);
						unset($_SESSION['website_required']);
						unset($_SESSION['organization_required']);
						}
						$_SESSION['addSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Trainer Updated Successfully. Thank You.</div>';


			} catch (Exception $e) {
				echo 'Error: ' . $e->getMessage();
			}
		}else{

			header("location:edit_trainer.php?id=$this->id");
		}

			
	}// update_user \\



	public function disable_trainer(){

					try {

				$query = "UPDATE trainers SET is_delete = :is_delete, deleted = :deleted WHERE trainers.unique_id = :id";

					$stmt = $this->conn -> prepare($query);
					$stmt -> execute(array(
						':is_delete' => '1', 
						':deleted' => date("Y-m-d h:i:s"),
						':id' => $this-> id,
						));

						$_SESSION['trainerDisabled'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Trainer Blocked Successfully. Thank You.</div>';

						header("location:../trainers/index.php");

			} catch (Exception $e) {
				echo 'Error: ' . $e->getMessage();
				header("location:../trainers/index.php");
			}
	
	}// disabled_user \\


		public function restore_trainers(){

					try {

				$query = "UPDATE trainers SET is_delete = :is_delete WHERE trainers.unique_id = :id";

					$stmt = $this->conn -> prepare($query);
					$stmt -> execute(array(
						':is_delete' => '0',
						':id' => $this-> id,
						));

						$_SESSION['TrainerRestored'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Trainers Restored Successfully. Thank You.</div>';

						header("location:../trainers/disabled_trainers.php");

			} catch (Exception $e) {
				echo 'Error: ' . $e->getMessage();
				header("location:../trainers/disabled_trainers.php");
			}
	
	}// disabled_user \\


	public function disable_trainers_list(){

		$query = "SELECT `trainers`.*, `courses`.`title` FROM `trainers` LEFT JOIN `courses` ON `trainers`.`courses_id` = `courses`.`id` WHERE `trainers`.`is_delete` = '1'";
		$stmt = $this->conn->prepare($query);
		$stmt -> execute();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$this->data[] = $row;
		}
		return $this->data;

	} // all_user \\


	public function delete_trainer_parmenet(){

		try {

			$query = "DELETE FROM `trainers` WHERE `trainers`.`unique_id` = '".$this->id."'";

			$stmt = $this->conn->prepare($query);
			$stmt -> execute();
				
				$_SESSION['TrainerRestored'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Trainer is Permanently deleted. Thank You.</div>';

				header("location:../trainers/disabled_trainers.php");

		} catch (Exception $e) {
			echo 'Error: ' . $e->getMessage();
			header("location:../trainers/disabled_trainers.php");
		}

	}// delete_trainer_parmenetly \\


}// Class \\
