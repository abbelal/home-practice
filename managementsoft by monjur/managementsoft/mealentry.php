<?php 
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
		require_once('header.php');
		require_once('include/db.php');
?>
			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Meal</a>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>Add Meal</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form method="post" action="insertmeal.php">
							<div class="control-group">
								<label class="control-label" for="date01">Date input</label>
								<div class="controls">
									<input type="date" name="date" class="input-xlarge datepicker" id="date01" />
								</div>
							</div>
							<table class="table table-striped">
								<tr>
									<th>Name</td>
									<th>
										<table class="table tableremoveborder">
											<thead>
												<tr>
													<th>Own</td>
													<th>Guest</td>
												</tr>
											</thead>
										</table>
									</td>
								</tr>
								<?php
									$userobj = new db();
									$user = $userobj->select_data('users', '*', 'status = 1');
									$rowcount = count($user);
									for($row = 0;$row < $rowcount; $row++) {
								?>	
								<tr>
									<td>
										<?php echo $user[$row]['name']; ?>
										<input type="hidden" value="<?php echo $user[$row]['id']; ?>" name="name[]" />
									</td>
									<td>
										<table class="table tableremoveborder">
											<tbody>
												<tr>
													<td>
														<input name="bf[<?php echo $row; ?>]" type="hidden" value="0" />
														<span>Breakfast</span><input name="bf[<?php echo $row; ?>]" type="checkbox" id="inlineCheckbox1" value="0.5" />
													</td>
													<td>
														<select id="selectError1" name="bfguest[]">
															<option>Guest</option>
															<?php
																for($i = 0; $i <= 10; $i++ ) {
																	echo '<option value="'.$i.'">'.$i.'</option>';
																}
															?>
														</select>
													</td>
												</tr>
												<tr>
													<td>
														<input name="ln[<?php echo $row; ?>]" type="hidden" value="0" />
														<span>Lanch</span><input name="ln[<?php echo $row; ?>]" type="checkbox" id="inlineCheckbox1" value="1" />
													</td>
													<td>
														<select id="selectError2" name="lnguest[]">
															<option>Guest</option>
															<?php
																for($i = 0; $i <= 10; $i++ ) {
																	echo '<option value="'.$i.'">'.$i.'</option>';
																}
															?>
														</select>
													</td>
												</tr>
												<tr>
													<td>
														<input name="dn[<?php echo $row; ?>]" type="hidden" value="0" />
														<span>Dinner</span><input name="dn[<?php echo $row; ?>]" type="checkbox" id="inlineCheckbox1" value="0.5" />
													</td>
													<td>
														<select id="selectError3" name="dnguest[]">
															<option>Guest</option>
															<?php
																for($i = 0; $i <= 10; $i++ ) {
																	echo '<option value="'.$i.'">'.$i.'</option>';
																}
															?>
														</select>
													</td>
												</tr>
												
											</tbody>
										</table>
									</td>
								</tr>
								<?php
									}
								?>
							</table>
							<input class="btn btn-default" type="submit" value="Save" name="submit" />
											
						</form>
					</div>
				</div><!--/span-->
			</div><!--/row-->
    
<?php 
	include('footer.php');
}
?>
