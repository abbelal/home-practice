<?php
require_once '../../vendor/autoload.php';

use ProjectMehedi\user\user_registration\user_registration;

use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();
$objLoginUser -> login_check();


$objDeleteUser = new user_registration();
$objDeleteUser -> prepare($_GET);

$objDeleteUser -> restore_user();