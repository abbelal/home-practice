<?php
include_once '../vendor/autoload.php';

use mobileApp\Mobilemodel;

$objectEdit = new Mobilemodel();
$objectEdit->dataPassToProperty($_GET);
$data = $objectEdit->singleDataShow();

$alldata = $_SESSION['formsallData'][$_GET['id']];

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
if (isset($_SESSION['formsallData']) && !empty($_SESSION['formsallData'])) {
    ?>
    <fieldset>
        <legend>Update Mobile Model</legend>
        <form action="update2.php" method="post">
            <a href="index2.php">Back to list</a><br/>
            <label for="">mobile model</label>
            <input type="text" name="mModel" value="<?php if (isset($alldata['mModel'])) {
            echo $alldata['mModel']; } ?>" /></br/>

            <label for="">laptop model</label>
            <input type="text" name="lModel" value="<?php if (isset($alldata['lModel'])) {
            echo $alldata['lModel']; }?>" /></br/>

            <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
            <button type="submit">update</button>
        </form>
    </fieldset>
    <?php
} else {
    $_SESSION['err_msg'] = "<h1>you are wrong...!!</h1>";
    header('location:error.php');
}
?>
