<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\software\software;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objUpdatesoftware = new software();

$objUpdatesoftware -> prepare($_POST);

$objUpdatesoftware -> software_update();