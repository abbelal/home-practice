<?php
include_once '../src/FavouritActor.php';
$objectList = new FavouritActor();
$allList = $objectList->actorList();

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
?>
<br/><a href="create.php">add new</a>
<table border="1">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($allList) && !empty($allList)) {
        foreach ($allList as $singleValue) {
            ?>
            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $singleValue['name'] ?></td>
                <td><a href="view.php?id=<?php echo $singleValue['id'] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $singleValue['id'] ?>">Update</a></td>
                <td><a href="delete.php?id=<?php echo $singleValue['id'] ?>">Delete</a></td>

            </tr>
            <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="3" align="center"> Durh mia....!!! Data nai </td>
        </tr>
        <?php
    }
    ?>

</table>
