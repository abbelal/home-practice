<?php 
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
	require_once('header.php');
	require_once('include/db.php');
	$db = new db();
?>


			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Food</a>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>Add Food</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php
							$success = '';
							if(isset($_POST['save'])) {
								$food = $_POST['food'];
								$data = array(
									'name' => $food
								);
								if($db->insert_data1('food_list', $data)) {
									$success = "New Food Added";
								}
							}
							echo "<h3 style='color: green'>{$success}</h3>"; 
						?>
						<a class="btn btn-primary" id="newfood" href="#addfood">Add Food</a>
						<div style="display:none" id="addfood">
							<form action="#" method="post">
								<input type="text" name="food" /><br />
								<input class="btn btn-primary" type="submit" name="save" value="Add" />
							</form>
						</div>
						<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable">
							<thead>
								<tr>
									<th>SL NL</th>
									<th>NAME</th>
								</tr>
							</thead>
						<?php 
							$foodrow = $db->select_data('food_list');
							$sl = 0;
							for($i = 0; $i < count($foodrow); $i++) {
								$sl++;
								echo "<tr>
									<td>{$sl}</td>
									<td>{$foodrow[$i]['name']}</td>
								</tr>";
							}
						?>
						</table>
					</div>
				</div>
			</div>
				

    
<?php
	}
	include('footer.php'); 
?>