<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 7/19/2016
 * Time: 10:12 AM
 */

include_once "../../../../vendor/autoload.php";
use App\Bitm\ID_129432\Admission\Admission;

$obj=new Admission();

$alldata=$obj->index();


if(isset($_SESSION['massage']) && !empty($_SESSION['massage'])){
    echo $_SESSION['massage'];
    unset($_SESSION['massage']);
}





?>



<html>
<head>
    <title>Index | Data</title>
    <style>
        body{
            width: 60%;
            margin:30px auto;
        }
        h3 {
            margin-left: 25%;
        }
        h3 a:hover {
            color: green;
        }
    </style>
</head>
<body>
<h3><a href="create.php">Add Student</a></h3>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Name</th>
        <th>Semester</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Weber</th>
        <th>Total</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $serial = 1;
    if(isset($alldata) && !empty($alldata)) {

        foreach ($alldata as $Singledata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $Singledata['name'] ?></td>
                <td><?php echo $Singledata['semester'] ?></td>
                <td><?php echo $Singledata['offer'] ?></td>
                <td><?php echo $Singledata['cost'] ?></td>
                <td><?php echo $Singledata['wever'] ?></td>
                <td><?php echo $Singledata['total'] ?></td>
                <td><a href="show.php?id=<?php echo $Singledata['unique_id']?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $Singledata['unique_id']?>">Update</a></td>
                <td><a href="delete.php?id=<?php echo $Singledata['unique_id']?>">Delete</a></td>
            </tr>
            <?php
        }
    }else{ ?>
    <tr>
        <td  colspan="9">No avaible data</td>
    </tr>
         <?php
    }?>

</table>
</body>
</html>