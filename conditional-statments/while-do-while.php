<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>loops</title>
    </head>
    <body>
        <h1>for loop<br/>
            <?php
            for ($i = 1; $i <= 10; $i++)
                echo $i;
            ?>
        </h1>

        <h1>while<br/>
            <?php
            $i = 1;
            while ($i <= 20) {
                echo $i . " ";
                $i++;
            }
            ?>
        </h1>

        <h1>do-while<br/>
            <?php
            $x = 1;
            do {
                echo "the number is : $x".'<br/>';
                $x++;
            } while ($x <= 10)
            ?>
        </h1>
    </body>
</html>
