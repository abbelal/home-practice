<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\labinfo\labinfo;;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

$_POST['id'] = $_SESSION['labinfo_id'];

$objUpdateLabs = new labinfo();

$objUpdateLabs -> prepare($_POST);

$objUpdateLabs -> required_validation();

$objUpdateLabs -> update_labs();

isset($_SESSION['labinfo_id']);