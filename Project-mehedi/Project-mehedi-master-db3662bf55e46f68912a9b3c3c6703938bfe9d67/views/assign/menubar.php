                        <ul class="breadcrumb-elements">
                            <li><a href="../assign/index.php"><i class=" icon-new position-left text-success-800"></i> Assign New Session</a></li>

                            <li><a href="../assign/list.php"><i class="icon-list position-left text-success-800"></i> View All Session</a></li>

                            <li><a href="../assign/trashlist.php"><i class="icon-cancel-square position-left text-success-800"></i> Disabled Session</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->
                <script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>

        <script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="../assets/js/pages/form_layouts.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="../assets/js/pages/datatables_basic.js"></script>


                <!-- Content area -->
                <div class="content">
