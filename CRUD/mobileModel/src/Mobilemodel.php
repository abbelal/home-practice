<?php

namespace mobileApp;

class Mobilemodel {

    public $id = '';
    public $model = '';
    public $laptop = '';
    public $mobileDataList = '';

    public function __construct() {
        session_start();
        $sqlConn = mysql_connect('localhost', 'root', '') or die("Ops..!! You can not connect with MySql");
        mysql_select_db('personalinfo') or die("You can not connect with Database");
    }

    public function dataPassToProperty($data='') {
        if (array_key_exists('mModel', $data) && !empty($data['mModel'])) {
            $this->model = $data['mModel'];
        } else {
            $_SESSION['mblemty'] = "mobile model required";
        }

        if (array_key_exists('lModel', $data) && !empty($data['lModel'])) {
            $this->laptop = $data['lModel'];
        } else {
            $_SESSION['ltpemty'] = "Laptop model required";
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        $_SESSION['formData']= $data;
        return $this;
    }

    public function store() {
        if (!empty($this->model) && !empty($this->laptop)) {
            $query = "INSERT INTO `mobilemodels` (`id`, `unique_id`, `models`,`laptop_model`) VALUES (NULL, '" . uniqid() . "', '$this->model','$this->laptop')";
            if (mysql_query($query)) {
                $_SESSION['message'] = "Data successfully submited";
            } else {
                echo "<h1>Opps...!!! You can not successfully added your model</h1>";
            }
            header('location:create.php');
        } else {
            header('location:create.php');
        }
    }

    public function mobileList() {
        $listGetQuery = "SELECT * FROM `mobilemodels`";
        $queryData = mysql_query($listGetQuery);
        while ($singleQueryData = mysql_fetch_assoc($queryData)) {
            $this->mobileDataList[] = $singleQueryData;
        }
        return $this->mobileDataList;
    }

    public function singleDataShow() {
        $showQuery = "SELECT * FROM `mobilemodels` WHERE `unique_id` =" . "'$this->id'" . "";
        $mydata = mysql_query($showQuery);
        $row = mysql_fetch_assoc($mydata);
        return $row;
    }

    public function dataUpdate() {
        $updateQuery = "UPDATE `mobilemodels` SET `models` = '$this->model' WHERE `mobilemodels`.`unique_id` =" . "'$this->id'" . "";
        if (mysql_query($updateQuery)) {
            $_SESSION['message'] = "successfully updated";
        }
        header("location:edit.php?id=$this->id");
    }

    public function dataDelete() {
        $deleteQuery = "DELETE FROM `mobilemodels` WHERE `mobilemodels`.`unique_id` = " . "'$this->id'" . "";
        if (mysql_query($deleteQuery)) {
            $_SESSION['message'] = "<h1>data deleted have you any problem..?</h1>";
        }
        header("location:index.php");
    }

}

?>
