<?php
include_once '../src/FavouritActor.php';
$objectList = new FavouritActor();
$objectList->dataPassToProperty($_GET);
$data = $objectList->singleView();
?>

<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>single view</title>
    </head>
    <body>
        <table border="1">
            <tr>
                <th>Id</th>
                <th>Name</th>
            </tr>
            <tr>
                <td><?php echo$data['id'] ?></td>
                <td><?php echo$data['name'] ?></td>
            </tr>
        </table>
        <h3><a href="actorList.php">Back to list</a></h3>
    </body>
</html>
