<?php

namespace ProjectMehedi\labinfo;

use PDO;

require_once '../../src/DatabaseConnection/DatabaseConnection.php';
// require(dirname(__FILE__).'../../../../src/DatabaseConnection/DatabaseConnection.php'); 

class labinfo extends \DatabaseConnection{
	
	public $id;
	public $labnum;
	public $course_id;
	public $seat_capacity;
	public $projector_resolution;
	public $ac_status;
	public $table_capacity;
	public $internet_speed;
	public $internet_speed_unite;
	public $os;
	public $pc_hdd;
	public $ups;
	public $trainer_pc_configuration;
	public $created;
	public $modified;
	public $deleted;
	public $data;
	public $error;


	public function course_id_title(){

		 $query = "SELECT `id`, `title` FROM `courses` ";
		 $stmt = $this -> conn -> prepare($query);
		 $stmt -> execute();

		 while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		 	$this->data[] = $row;
		 }
		 return $this->data;

	}



	public function prepare($data = ""){

		if(!empty($data['id'])){
			$this->id = $data['id'];
		}
		if(!empty($data['labnum'])){
			$this->labnum = $data['labnum'];
		}
		if(!empty($data['course_id'])){
			$this->course_id = $data['course_id'];
		}
		if(!empty($data['seat_capacity'])){
			$this->seat_capacity = $data['seat_capacity'];
		}
		if(!empty($data['projector_resolution'])){
			$this->projector_resolution = $data['projector_resolution'];
		}
		if(!empty($data['ac_status'])){
			$this->ac_status = $data['ac_status'];
		}
		if(!empty($data['table_capacity'])){
			$this->table_capacity = $data['table_capacity'];
		}
		if(!empty($data['internet_speed'])){
			$this->internet_speed = $data['internet_speed'];
		}
		if(!empty($data['internet_speed_unite'])){
			$this->internet_speed_unite = $data['internet_speed_unite'];
		}
		if(!empty($data['os'])){
			$this->os = $data['os'];
		}
		if(!empty($data['brand_name'])){
			$this->brand_name = $data['brand_name'];
		}

		if(!empty($data['pc_processor'])){
			$this->pc_processor = $data['pc_processor'];
		}
		if(!empty($data['pc_ram'])){
			$this->pc_ram = $data['pc_ram'];
		}
		if(!empty($data['pc_hdd'])){
			$this->pc_hdd = $data['pc_hdd'];
		}
		if(!empty($data['ups'])){
			$this->ups = $data['ups'];
		}
		if(!empty($data['trainer_pc_configuration'])){
			$this->trainer_pc_configuration = $data['trainer_pc_configuration'];
		}

		return $this;

		
	}// prepare \\



	public function required_validation(){

		// Lab name required validation goes here
		if(!empty($this->labnum) && isset($this->labnum)){
			$_SESSION['lab_no'] = $this->labnum;
		}else{
			$_SESSION['labnum_required'] = "Lab number is required";
			$this->error = TRUE;
		}
		// course required validation goes here
		if(!empty($this->course_id) && isset($this->course_id)){
			$_SESSION['course_id'] = $this->course_id;
		}else{
			$_SESSION['course_id_required'] = "Course name is required";
			$this->error = TRUE;
		}
		//seat_capacity  required validation goes here
		if(!empty($this->seat_capacity) && isset($this->seat_capacity)){
			$_SESSION['seat_capacity'] = $this->seat_capacity;
		}else{
			$_SESSION['seat_capacity_required'] = "Seat Capacity is required";
			$this->error = TRUE;
		}
		//Projector resolution  required validation goes here
		if(!empty($this->projector_resolution) && isset($this->projector_resolution)){
			$_SESSION['projector_resolution'] = $this->projector_resolution;
		}else{
			$_SESSION['projector_resolution_required'] = "You can add Projector resolution later";
		}
		//AC Status required validation goes here
		if(!empty($this->ac_status) && isset($this->ac_status)){
			$_SESSION['ac_status'] = $this->ac_status;
		}else{
			$_SESSION['ac_status_required'] = "You can add AC status later";
		}
		//AC Status required validation goes here
		if(!empty($this->table_capacity) && isset($this->table_capacity)){
			$_SESSION['table_capacity'] = $this->table_capacity;
		}else{
			$_SESSION['table_capacity_required'] = "You can add Table Capacity later";
		}
		//Internet Speed required validation goes here
		if(!empty($this->internet_speed) && isset($this->internet_speed)){
			$_SESSION['internet_speed'] = $this->internet_speed;
		}else{
			$_SESSION['internet_speed_required'] = "You can add internet speed later";
		}
		// OS validation goes here
		if(!empty($this->os) && isset($this->os)){
			$_SESSION['os'] = $this->os;
		}else{
			$_SESSION['os_required'] = "OS is required";
			$this->error = TRUE;
		}
		// Brand validation goes here
		if(!empty($this->brand_name) && isset($this->brand_name)){
			$_SESSION['brand_name'] = $this->brand_name;
		}else{
			$_SESSION['brand_name_required'] = "Brand name is required";
			$this->error = TRUE;
		}
		// Processor validation goes here
		if(!empty($this->pc_processor) && isset($this->pc_processor)){
			$_SESSION['pc_processor'] = $this->pc_processor;
		}else{
			$_SESSION['pc_processor_required'] = "Processor required";
			$this->error = TRUE;
		}
		// RAM validation goes here
		if(!empty($this->pc_ram) && isset($this->pc_ram)){
			$_SESSION['pc_ram'] = $this->pc_ram;
		}else{
			$_SESSION['pc_ram_required'] = "Ram required";
			$this->error = TRUE;
		}
		// RAM validation goes here
		if(!empty($this->pc_hdd) && isset($this->pc_hdd)){
			$_SESSION['pc_hdd'] = $this->pc_hdd;
		}else{
			$_SESSION['pc_hdd_required'] = "Hard-disk required";
			$this->error = TRUE;
		}
		// Trainer PC validation goes here
		if(!empty($this->trainer_pc_configuration) && isset($this->trainer_pc_configuration)){
			$_SESSION['trainer_pc_configuration'] = $this->trainer_pc_configuration;
		}else{
			$_SESSION['trainer_pc_configuration_required'] = "Trainer pc configuration is required";
			$this->error = TRUE;
		}
		// Speed limit assinged as for data keep by session
		
		$_SESSION['internet_speed_unite'] = $_POST['internet_speed_unite'];
		$_SESSION['ups'] = $this->ups;


	
	} // required_validation \\

	public function add_labs(){

		if($this->error == FALSE){

			try {

				$internet_speed = $this-> internet_speed." ".$this-> internet_speed_unite;

				$pc_configuration_array = array(
						$this->brand_name,
						$this->pc_processor,
						$this->pc_ram,
						$this->pc_hdd,
						$this->ups,
						);
				$pc_configuration_serialize = serialize($pc_configuration_array);

				$query = "INSERT INTO labinfo (id, course_id, lab_no, seat_capacity, projector_resolution, ac_status, pc_configuration, os, trainer_pc_configuration, table_capacity, internet_speed, is_delete, created) VALUES (:id, :course_id, :lab_no, :seat_capacity, :projector_resolution, :ac_status, :pc_configuration, :os, :trainer_pc_configuration, :table_capacity, :internet_speed,:is_delete, :created)";

					$stmt = $this->conn -> prepare($query);
					if($stmt -> execute(array(

						':id'=> null ,
						':course_id'=> $this -> course_id ,
						':lab_no'=> $this -> labnum,
						':seat_capacity'=> $this -> seat_capacity,
						':projector_resolution'=> (isset($this-> projector_resolution) ? $this-> projector_resolution: ''),
						':ac_status'=> (isset($this-> ac_status) ? $this-> ac_status : ''),
						':pc_configuration'=> $pc_configuration_serialize ,
						':os'=> $this -> os ,
						':trainer_pc_configuration'=> $this -> trainer_pc_configuration,
						':table_capacity'=> (isset($this-> table_capacity) ? $this-> table_capacity : ''),
						':internet_speed'=> (isset($internet_speed) ? $internet_speed : ''),
						':is_delete' => 0,
						':created'=> date("Y-m-d h:i:s"),
						))){

						header('location:add_labs.php');
						unset($_SESSION['lab_no']);
						unset($_SESSION['course_id']);
						unset($_SESSION['seat_capacity']);
						unset($_SESSION['projector_resolution']);
						unset($_SESSION['ac_status']);
						unset($_SESSION['table_capacity']);
						unset($_SESSION['internet_speed']);
						unset($_SESSION['internet_speed_unite']);
						unset($_SESSION['os']);
						unset($_SESSION['brand_name']);
						unset($_SESSION['pc_processor']);
						unset($_SESSION['pc_ram']);
						unset($_SESSION['pc_hdd']);
						unset($_SESSION['ups']);
						unset($_SESSION['trainer_pc_configuration']);
						}
						$_SESSION['labAddSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>labs Added Successfully. Thank You.</div>';
			} catch (Exception $e) {
				echo 'Error: ' . $e->getMessage();
				
			}
		}else{

			header('location:add_labs.php');
		}

			
	}// addUser \\

	public function all_labs(){

		$query = "SELECT `labinfo`.*, `courses`.`title` FROM `labinfo` LEFT JOIN `courses` ON `labinfo`.`course_id` = `courses`.`id` WHERE `labinfo`.`is_delete` = '0'";
		$stmt = $this->conn->prepare($query);
		$stmt -> execute();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$this->data[] = $row;
		}
		return $this->data;

	} // all_user \\


	public function single_labs(){

		$query = "SELECT `labinfo`.*, `courses`.`title` FROM `labinfo` LEFT JOIN `courses` ON `labinfo`.`course_id` = `courses`.`id` WHERE `labinfo`.`id` = '".$this->id."'";

		$stmt = $this->conn->prepare($query);
		$stmt -> execute();

		$row = $stmt -> fetch(PDO::FETCH_ASSOC);
		return $row;

	}// Single_user\\



		public function update_labs(){

		if($this->error == FALSE){

			try {

				$internet_speed = $this-> internet_speed." ".$this-> internet_speed_unite;

				$pc_configuration_array = array(
						$this->brand_name,
						$this->pc_processor,
						$this->pc_ram,
						$this->pc_hdd,
						$this->ups,
						);
				$pc_configuration_serialize = serialize($pc_configuration_array);

				$query = "UPDATE labinfo SET course_id = :course_id, lab_no = :lab_no, seat_capacity = :seat_capacity, projector_resolution = :projector_resolution, ac_status = :ac_status, pc_configuration = :pc_configuration, os = :os, trainer_pc_configuration = :trainer_pc_configuration, table_capacity = :table_capacity, internet_speed = :internet_speed, updated= :updated WHERE labinfo.id = :id";

					$stmt = $this->conn -> prepare($query);
					if($stmt -> execute(array(

						':id'=> $this-> id ,
						':course_id'=> $this -> course_id ,
						':lab_no'=> $this -> labnum,
						':seat_capacity'=> $this -> seat_capacity,
						':projector_resolution'=> (isset($this-> projector_resolution) ? $this-> projector_resolution: ''),
						':ac_status'=> (isset($this-> ac_status) ? $this-> ac_status : ''),
						':pc_configuration'=> $pc_configuration_serialize ,
						':os'=> $this -> os ,
						':trainer_pc_configuration'=> $this -> trainer_pc_configuration,
						':table_capacity'=> (isset($this-> table_capacity) ? $this-> table_capacity : ''),
						':internet_speed'=> (isset($internet_speed) ? $internet_speed : ''),
						':updated'=> date("Y-m-d h:i:s"),
						))){

						header("location:edit_lab.php?id=$this->id");
						unset($_SESSION['lab_no']);
						unset($_SESSION['course_id']);
						unset($_SESSION['seat_capacity']);
						unset($_SESSION['projector_resolution']);
						unset($_SESSION['ac_status']);
						unset($_SESSION['table_capacity']);
						unset($_SESSION['internet_speed_data']);
						unset($_SESSION['internet_speed_unite']);
						unset($_SESSION['os']);
						unset($_SESSION['brand_name']);
						unset($_SESSION['pc_processor']);
						unset($_SESSION['pc_ram']);
						unset($_SESSION['pc_hdd']);
						unset($_SESSION['ups']);
						unset($_SESSION['trainer_pc_configuration']);
						}
						$_SESSION['labAddSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>labs Added Successfully. Thank You.</div>';
			} catch (Exception $e) {
				echo 'Error: ' . $e->getMessage();
				
			}
		}else{

			header("location:edit_lab.php?id=$this->id");
		}
	}// update labs


	public function disable_labs(){

					try {

				$query = "UPDATE labinfo SET is_delete = :is_delete, deleted = :deleted WHERE labinfo.id = :id";

					$stmt = $this->conn -> prepare($query);
					$stmt -> execute(array(
						':is_delete' => '1', 
						':deleted' => date("Y-m-d h:i:s"),
						':id' => $this-> id,
						));

						$_SESSION['labsDisabled'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>labs Blocked Successfully. Thank You.</div>';

						header("location:../labinfo/index.php");

			} catch (Exception $e) {
				echo 'Error: ' . $e->getMessage();
				header("location:../labinfo/index.php");
			}
	
	}// disabled_user \\


		public function restore_labinfo(){

					try {

				$query = "UPDATE labinfo SET is_delete = :is_delete WHERE labinfo.id = :id";

					$stmt = $this->conn -> prepare($query);
					$stmt -> execute(array(
						':is_delete' => '0',
						':id' => $this-> id,
						));

						$_SESSION['labsRestored'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>lab Restored Successfully. Thank You.</div>';

						header("location:../labinfo/canceled_labs.php");

			} catch (Exception $e) {
				echo 'Error: ' . $e->getMessage();
				header("location:../labinfo/canceled_labs.php");
			}
	
	}// disabled_user \\


	public function disable_labinfo_list(){

		$query = "SELECT `labinfo`.*, `courses`.`title` FROM `labinfo` LEFT JOIN `courses` ON `labinfo`.`course_id` = `courses`.`id` WHERE `labinfo`.`is_delete` = '1'";
		$stmt = $this->conn->prepare($query);
		$stmt -> execute();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$this->data[] = $row;
		}
		return $this->data;

	} // all_user \\


	public function delete_labs_parmenet(){

		try {

			$query = "DELETE FROM `labinfo` WHERE `labinfo`.`id` = '".$this->id."'";

			$stmt = $this->conn->prepare($query);
			$stmt -> execute();
				
				$_SESSION['labsParmentlyDeleted'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>labs is Permanently deleted. Thank You.</div>';

				header("location:../labinfo/canceled_labs.php");

		} catch (Exception $e) {
			echo 'Error: ' . $e->getMessage();
			header("location:../labinfo/canceled_labs.php");
		}

	}// delete_labs_parmenetly \\

	public function test(){

		header("location:edit_lab.php?id=$this->id");
	}


}// Class \\
