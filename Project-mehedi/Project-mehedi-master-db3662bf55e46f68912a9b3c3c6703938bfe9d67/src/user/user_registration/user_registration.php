<?php

namespace ProjectMehedi\user\user_registration;

use PDO;

require_once '../../src/DatabaseConnection/DatabaseConnection.php';

// require(dirname(__FILE__).'../../../../src/DatabaseConnection/DatabaseConnection.php'); 

class user_registration extends \DatabaseConnection {

    public $id;
    public $username;
    public $firstname;
    public $lastname;
    public $fullname;
    public $password;
    public $confrm_password;
    public $email;
    public $confirm_email;
    public $image;
    public $is_admin;
    public $is_active;
    public $is_delete;
    public $created;
    public $modified;
    public $deleted;
    public $data;
    public $error;

    public function prepare($data = "") {

        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['username'])) {
            $this->username = $data['username'];
        }
        if (!empty($data['firstname'])) {
            $this->firstname = $data['firstname'];
        }
        if (!empty($data['lastname'])) {
            $this->lastname = $data['lastname'];
        }
        if (!empty($data['lastname']) || !empty($data['firstname'])) {
            $this->fullname = ucfirst($data['firstname']) . " " . ucfirst($data['lastname']);
        }
        if (!empty($data['password'])) {
            $this->password = $data['password'];
        }
        if (!empty($data['confrm_password'])) {
            $this->confrm_password = $data['confrm_password'];
        }
        if (!empty($data['email'])) {
            $this->email = $data['email'];
        }
        if (!empty($data['confirm_email'])) {
            $this->confirm_email = $data['confirm_email'];
        }
        if (!empty($data['image'])) {
            $this->image = $data['image'];
        }
        if (!empty($data['is_admin'])) {
            $this->is_admin = $data['is_admin'];
        }

        return $this;
    }

// prepare \\

    public function validate() {

        //username validation goes here
        if (!empty($this->username) && isset($this->username)) {
            if (strlen($this->username) >= 5 && strlen($this->username) <= 12) {
                if (preg_match("/^[a-zA-Z0-9_]+$/", $this->username)) {
                    $unique_username_query = "SELECT * FROM `users` WHERE `username` = '" . $this->username . "'";
                    $stmt = $this->conn->prepare($unique_username_query);
                    $stmt->execute();

                    $username_row = $stmt->fetch(PDO::FETCH_ASSOC);
                    if (!empty($username_row)) {
                        if ($this->id != $username_row['unique_id']) {
                            $_SESSION['Uname_exists'] = "This username already exits";
                            $this->error = TRUE;
                            // header('location:add_user.php');
                        }
                    } else { // unique username
                        $_SESSION['username'] = $this->username;
                    }
                } else { // preg_match only text allowed.
                    $_SESSION['Uname_onlyText'] = "Only text and number allowed here";
                    $this->error = TRUE;
                    // header('location:add_user.php');
                }
            } else { // strlen 5 to 12
                $_SESSION['Uname_charLength'] = "Username have to 6 to 12 charecter";
                $this->error = TRUE;
                // header('location:add_user.php');
            }
        } else { // !empty($this->username)
            $_SESSION['Uname_required'] = "Username must be required";
            $this->error = TRUE;
            // header('location:add_user.php');
        }
        // Firstname validation goes here
        if (!empty($this->firstname) && isset($this->firstname)) {
            if (strlen($this->firstname) <= 15) {
                if (preg_match("/^[a-zA-Z ]*$/", $this->firstname)) {
                    $_SESSION['firstname'] = $this->firstname;
                } else {
                    $_SESSION['firstname_onlyText'] = "Only text is allowed here";
                    $this->error = TRUE;
                    // header('location:add_user.php');
                }
            } else { // strlen
                $_SESSION['firstname_charLimit'] = "Name must be within 15 charecter";
                $this->error = TRUE;
                // header('location:add_user.php');
            }
        } else { // empty firstname
            $_SESSION['firstname_required'] = "You can add name later.";
        }

        // Lastname & Lastname validation goes here
        if (!empty($this->lastname) && isset($this->lastname)) {

            if (strlen($this->lastname) <= 15) {
                if (preg_match("/^[a-zA-Z ]*$/", $this->lastname)) {
                    $_SESSION['lastname'] = $this->lastname;
                } else {
                    $_SESSION['lastname_onlyText'] = "Only text is allowed here";
                    $this->error = TRUE;
                    // header('location:add_user.php');
                }
            } else { // strlen
                $_SESSION['lastname_charLimit'] = "Name must be within 15 charecter";
                $this->error = TRUE;
                // header('location:add_user.php');
            }
        } else { // empty firstname
            $_SESSION['lastname_required'] = "You can add name later.";
        }

        // Password validation goes here
        if (!empty($this->password) && isset($this->password)) {
            if (strlen($this->password) >= 5 && strlen($this->password) <= 12) {
                if (preg_match("#[a-zA-Z ]#", $this->password)) {
                    if (preg_match("#[0-9]#", $this->password)) {
                        // $_SESSION['password'] = $this->password;
                    } else {
                        $_SESSION['password_digiteRequired'] = "Password must be have number";
                        $this->error = TRUE;
                        // header('location:add_user.php');
                    }
                } else {
                    $_SESSION['password_charRequired'] = "Password must have charecter";
                    $this->error = TRUE;
                    // header('location:add_user.php');
                }
            } else { // password 5 to 12 char
                $_SESSION['password_charLength'] = "Password must be 5 to 12 charecter";
                $this->error = TRUE;
                // header('location:add_user.php');
            }
        } else { // empty password and isset
            $_SESSION['password_required'] = "Password must be required";
            $this->error = TRUE;
            // header('location:add_user.php');
        }

        // Confirm Password validataion goes here
        if (!empty($this->confrm_password) && isset($this->confrm_password)) {
            if ($this->confrm_password == $this->password) {
                // $_SESSION['confrm_password'] = $this->confrm_password;
            } else {
                $_SESSION['confirmPassword_mismatch'] = "Password have to be matched";
                $this->error = TRUE;
                // header('location:add_user.php');
            }
        } else {
            $_SESSION['confrmPassword_required'] = "Please retype password";
            $this->error = TRUE;
            // header('location:add_user.php');
        }

        // Email validation goes here
        if (!empty($this->email) && isset($this->email)) {
            if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $unique_email_query = "SELECT * FROM `users` WHERE `email`= '" . $this->email . "'";
                $stmt = $this->conn->prepare($unique_email_query);
                $stmt->execute();

                $email_row = $stmt->fetch(PDO::FETCH_ASSOC);
                if (!empty($email_row)) {
                    if ($this->id != $email_row['unique_id']) {
                        $_SESSION['email_exists'] = "Email already exits";
                    }
                } else { // email_row emty!!
                    $_SESSION['email'] = $this->email;
                }
            } else { // format check of email
                $_SESSION['email_formateInvalid'] = "Invalid email formate";
                $this->error = TRUE;
                // header('location:add_user.php');
            }
        } else { // empty email
            $_SESSION['emai_required'] = "Email must be required";
            $this->error = TRUE;
            // header('location:add_user.php');
        }
    }

// validate

    public function add_image() {
        if (!empty($_FILES['image']['name']) && isset($_FILES['image'])) {
            $this->image = uniqid() . $_FILES['image']['name'];
            $image_type = $_FILES['image']['type'];
            $image_temp_location = $_FILES['image']['tmp_name'];
            $image_size = $_FILES['image']['size'];
            $require_extension = array('jpg', 'jpeg', 'gif', 'bmp', 'png');
            $image_extension = strtolower(end(explode('.', $this->image)));

            if (in_array($image_extension, $require_extension) == false) {
                $_SESSION['ErrorImageExtension'] = 'Please give only image extension (.jpg, .png)';
                $error = TRUE;
                $this->error = TRUE;
            }
            if ($image_size > 2000000) {
                $_SESSION['ErrorImageSize'] = " Size must be below 2 MB";
                $error = TRUE;
                $this->error = TRUE;
            }
            if (empty($error) && $this->error == FALSE) {
                move_uploaded_file($image_temp_location, "../assets/images/user/" . $this->image . "");
                // $_POST['image'] = $this->image;
            }
        } else {
            $_SESSION['NeedImage'] = "You can add image latter";
        }
    }

// add_image \\

    public function addUser() {

        if ($this->error == FALSE) {

            try {

                $query = "INSERT INTO users (`id`, `unique_id`, `full_name`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `is_delete`, `created`) VALUES (:id, :unique_id, :full_name, :username, :email, :password, :image, :is_active, :is_admin, :is_delete, :created)";

                $stmt = $this->conn->prepare($query);
                if ($stmt->execute(array(
                            ':id' => null,
                            ':unique_id' => uniqid(),
                            ':full_name' => (isset($this->fullname) ? $this->fullname : ''),
                            ':username' => $this->username,
                            ':email' => $this->email,
                            ':password' => $this->password,
                            ':image' => $this->image, //(isset($this-> image) ? $this-> image : ''),
                            ':is_active' => 1,
                            ':is_admin' => $this->is_admin,
                            ':is_delete' => 0,
                            ':created' => date("Y-m-d h:i:s"),
                        ))) {
                    $_SESSION['addSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>User Added Successfully. Thank You.</div>';

                    header('location:add_user.php');
                    unset($_SESSION['username']);
                    unset($_SESSION['firstname']);
                    unset($_SESSION['lastname']);
                    unset($_SESSION['email']);
                    unset($_SESSION['confirm_email']);
                }
            } catch (Exception $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {

            header('location:add_user.php');
        }
    }

// addUser \\

    public function all_user() {

        $query = "SELECT * FROM `users` WHERE `is_delete` = 0";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->data[] = $row;
        }
        return $this->data;
    }

// all_user \\

    public function single_user() {

        $query = "SELECT * FROM `users` WHERE `unique_id` = '" . $_SESSION['logged']['unique_id'] . "'";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

// Single_user\\

    public function update_user() {

        if ($this->error == FALSE) {

            try {

                $query = "UPDATE users SET full_name = :full_name, username = :username, email = :email, password = :password, image = :image, updated = :updated WHERE users.unique_id = :id";

                $stmt = $this->conn->prepare($query);
                if ($stmt->execute(array(
                            ':full_name' => (isset($this->fullname) ? $this->fullname : ''),
                            ':username' => $this->username,
                            ':email' => $this->email,
                            ':password' => $this->password,
                            ':image' => $this->image,
                            ':updated' => date("Y-m-d h:i:s"),
                            ':id' => $this->id,
                        ))) {

                    header("location:edit_user.php?id=$this->id");
                    unset($_SESSION['username']);
                    unset($_SESSION['firstname']);
                    unset($_SESSION['lastname']);
                    unset($_SESSION['email']);
                    unset($_SESSION['confirm_email']);
                    unset($_SESSION['pass']);
                    unset($_SESSION['img']);
                }
                $_SESSION['updateSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>User Updated Successfully. Thank You.</div>';
            } catch (Exception $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {

            header("location:edit_user.php?id=$this->id");
        }
    }

// update_user \\

    public function disable_user() {

        try {

            $query = "UPDATE users SET is_delete = :is_delete, deleted = :deleted WHERE users.unique_id = :id";

            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':is_delete' => 1,
                ':deleted' => date("Y-m-d h:i:s"),
                ':id' => $this->id,
            ));

            $_SESSION['userDisabled'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>User Blocked Successfully. Thank You.</div>';

            header("location:index.php");
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
            header("location:index.php");
        }
    }

// disabled_user \\

    public function restore_user() {

        try {

            $query = "UPDATE users SET is_delete = :is_delete WHERE users.unique_id = :id";

            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':is_delete' => 0,
                ':id' => $this->id,
            ));

            $_SESSION['userRestored'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>User Restored Successfully. Thank You.</div>';

            header("location:index.php");
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
            header("location:index.php");
        }
    }

// disabled_user \\

    public function disable_user_list() {

        $query = "SELECT * FROM `users` WHERE `is_delete` = 1";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->data[] = $row;
        }
        return $this->data;
    }

// disable_user_list \\
}

// Class \\
