<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_registration\user_registration;

use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

include_once '../header.php';
include_once 'menubar.php';
?>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Error</span> something going wrong</h4>
        </div>
    </div>
</div>
<!-- /page header -->

    <div class="content">

        <!-- Error wrapper -->
        <div class="container-fluid text-center">
            <h1 class="error-title">404</h1>
            <h6 class="text-semibold content-group"><?php
                
                if (!empty($_SESSION["errorMsg"]) && isset($_SESSION["errorMsg"])) {
                    echo $_SESSION["errorMsg"];
                    unset($_SESSION["errorMsg"]);
        
                }
            ?></h6>

            <div class="row">
                <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <a href="dashboard.php" class="btn btn-primary btn-block content-group"><i class="icon-circle-left2 position-left"></i> Go to Dashboard</a>
                            </div>

                        </div>
                </div>
            </div>
        </div>
        <!-- /error wrapper -->
        <?php include_once 'footer.php'; ?>