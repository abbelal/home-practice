<?php

namespace signupForm\profileForm;

use PDO;

class profileForm {

    public $id = '';
    public $fullName = '';
    public $fatherName = '';
    public $motherName = '';
    public $gender = '';
    public $birthDate = '';
    public $mobile = '';
    public $occupation = '';
    public $eduStatus = '';
    public $religion = '';
    public $maritalSts = '';
    public $jobSts = '';
    public $nationality = '';
    public $interest = '';
    public $bio = '';
    public $nID = '';
    public $passportNum = '';
    public $skills = '';
    public $language = '';
    public $profilePic = '';
    public $bloodGrp = '';
    public $faxNum = '';
    public $height = '';
    public $addres = '';
    public $city = '';
    public $zipCode = '';
    public $webUrl = '';
    public $other = '';
    public $created = '';
    public $modified = '';
    public $deleted = '';
    public $dbConnect = '';
    public $dbUser = 'root';
    public $dbPassword = '';

    public function __construct() {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->dbConnect = new PDO("mysql:host=localhost;dbname=resistrationform", $this->dbUser, $this->dbPassword);
    }

    public function prepare($formAllData='') {
        if (array_key_exists('id', $formAllData) && !empty($formAllData['id'])) {
            $this->id = $formAllData['id'];
        }
        if (array_key_exists('fullName', $formAllData) && !empty($formAllData['fullName'])) {
            $this->fullName = $formAllData['fullName'];
        }
        if (array_key_exists('fatherName', $formAllData) && !empty($formAllData['fatherName'])) {
            $this->fatherName = $formAllData['fatherName'];
        }
        if (array_key_exists('motherName', $formAllData) && !empty($formAllData['motherName'])) {
            $this->motherName = $formAllData['motherName'];
        }
        if (array_key_exists('gender', $formAllData) && !empty($formAllData['gender'])) {
            $this->gender = $formAllData['gender'];
        }
        if (array_key_exists('birthDate', $formAllData) && !empty($formAllData['birthDate'])) {
            $this->birthDate = $formAllData['birthDate'];
        }
        if (array_key_exists('mobileNum', $formAllData) && !empty($formAllData['mobileNum'])) {
            $this->mobile = $formAllData['mobileNum'];
        }
        if (array_key_exists('occupation', $formAllData) && !empty($formAllData['occupation'])) {
            $this->occupation = $formAllData['occupation'];
        }
        if (array_key_exists('eduStatus', $formAllData) && !empty($formAllData['eduStatus'])) {
            $this->eduStatus = $formAllData['eduStatus'];
        }
        if (array_key_exists('religion', $formAllData) && !empty($formAllData['religion'])) {
            $this->religion = $formAllData['religion'];
        }
        if (array_key_exists('metiralSts', $formAllData) && !empty($formAllData['metiralSts'])) {
            $this->maritalSts = $formAllData['metiralSts'];
        }
        if (array_key_exists('jobStatus', $formAllData) && !empty($formAllData['jobStatus'])) {
            $this->jobSts = $formAllData['jobStatus'];
        }
        if (array_key_exists('nationality', $formAllData) && !empty($formAllData['nationality'])) {
            $this->nationality = $formAllData['nationality'];
        }
        if (array_key_exists('bio', $formAllData) && !empty($formAllData['bio'])) {
            $this->bio = $formAllData['bio'];
        }
        if (array_key_exists('nationalId', $formAllData) && !empty($formAllData['nationalId'])) {
            $this->nID = $formAllData['nationalId'];
        }
        if (array_key_exists('passportId', $formAllData) && !empty($formAllData['passportId'])) {
            $this->passportNum = $formAllData['passportId'];
        }
        if (array_key_exists('skills', $formAllData) && !empty($formAllData['skills'])) {
            $this->skills = $formAllData['skills'];
        }
        if (array_key_exists('language', $formAllData) && !empty($formAllData['language'])) {
            $this->language = $formAllData['language'];
        }
        if (array_key_exists('interest', $formAllData) && !empty($formAllData['interest'])) {
            $this->interest = $formAllData['interest'];
        }
        if (array_key_exists('profilePic', $formAllData) && !empty($formAllData['profilePic'])) {
            $this->profilePic = $formAllData['profilePic'];
        }
        if (array_key_exists('bloodGrp', $formAllData) && !empty($formAllData['bloodGrp'])) {
            $this->bloodGrp = $formAllData['bloodGrp'];
        }
        if (array_key_exists('faxNum', $formAllData) && !empty($formAllData['faxNum'])) {
            $this->faxNum = $formAllData['faxNum'];
        }
        if (array_key_exists('height', $formAllData) && !empty($formAllData['height'])) {
            $this->height = $formAllData['height'];
        }
        if (array_key_exists('addres', $formAllData) && !empty($formAllData['addres'])) {
            $this->addres = $formAllData['addres'];
        }
        if (array_key_exists('zipCode', $formAllData) && !empty($formAllData['zipCode'])) {
            $this->zipCode = $formAllData['zipCode'];
        }
        if (array_key_exists('city', $formAllData) && !empty($formAllData['city'])) {
            $this->city = $formAllData['city'];
        }
        if (array_key_exists('webUrl', $formAllData) && !empty($formAllData['webUrl'])) {
            $this->webUrl = $formAllData['webUrl'];
        }
    }

    //end prepare method
    //
    //
    //
    //
    //
    //
    
    public function singleUserRow() {
        $selectQuery = "SELECT * FROM `tbl_profile` WHERE `userId`=41";
        $stmt = $this->dbConnect->prepare($selectQuery);
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        return $user;
    }

    //end singleUserRow method
    //
    //
    //
    //
    public function profileUpdate() {
        $id=41;
        $updateQuery = "UPDATE `tbl_profile` SET `fullName` = $this->fullName, `fatherName`=$this->fatherName, `motherName`=$this->motherName, `gender`=:gender, `birthDate`=birthDate, `mobile`=:mobile WHERE `tbl_profile`.`userId` = 41 ";
        $stmt = $this->dbConnect->prepare($updateQuery);
        $stmt->execute();
    }

    //end profile update methode
    //
    //
    //
    //
    //
    //end class
}

?>
