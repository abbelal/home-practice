<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\trainers\trainers;
$objLoginUser = new user_login();
$objLoginUser -> login_check();


$objEditTrainers = new trainers();

$objEditTrainers -> prepare($_GET);

$singleTrainer = $objEditTrainers -> single_Trainer();

$course_id_title = $objEditTrainers -> course_id_title();


if(!empty($singleTrainer) && isset($singleTrainer)){




    function errMsg($data = ""){ // Show the Error session msg from class file
        if(isset($_SESSION["$data"])){
        ?>
            <label class="validation-error-label text-left">
        <?php
            if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
                echo $_SESSION["$data"]."<br></label>";
                unset($_SESSION["$data"]);
            }
        }
    }

    function errMsgSuc($data = ""){ // Show the Error session msg in Green color
        if(isset($_SESSION["$data"])){
        ?>
            <label class="validation-error-label text-success text-left">
        <?php
            if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
                echo $_SESSION["$data"]."<br></label>";
                unset($_SESSION["$data"]);
            }
        }
    }


    function database_value($database_data = ""){
    	global $singleTrainer;
    	if(isset($singleTrainer["$database_data"]) && !empty($singleTrainer["$database_data"])){
            echo $singleTrainer["$database_data"];
        }
    }

    function education($edu_status_array = ""){
    	global $singleTrainer;
    	if(isset($singleTrainer["edu_status"])){
    		$education = unserialize($singleTrainer["edu_status"]);
    		return $education["$edu_status_array"];
        }
    }


	function address($address_array = ""){
    	global $singleTrainer;
    	if(isset($singleTrainer["address"])){
    		$address = unserialize($singleTrainer["address"]);
    		return $address["$address_array"];
        }
    }


    function database_checked($value = "", $key = ""){ // to hold select point of dropdown 
        global $singleTrainer;
        if(isset($singleTrainer["$value"])){
            if($singleTrainer["$value"] == "$key"){
                echo 'selected="selected"';
            }else{
                echo '';
            }
        }
    }

    function database_checked_radio($value = "", $key = ""){ // to hold checked of Radio 
        global $singleTrainer;
        if(isset($singleTrainer["$value"])){
            if($singleTrainer["$value"] == "$key"){
                echo 'checked="checked"';
            }else{
                echo '';
            }
        }
    }


include_once '../header.php';
include_once 'menubar.php';
?>
<!-- Table -->
<?php $objEditTrainers -> session_message('addSuccess'); ?>
<div class="panel-flat">
    <!-- Grid -->
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel registration-form">
                <div class="panel-body">
                    <div class="text-center">
                        <div class="border-primary text-primary">



<div class="thumb thumb-rounded thumb-slide">
		<img alt="" class="img-rounded" style= "width:auto;height:200px;" src="
		<?php
			if(isset($singleTrainer['image']) && !empty($singleTrainer['image'])){
				echo '../assets/images/trainer/'.$singleTrainer['image'];
			}else{
				echo '../assets/images/trainer/nobody.jpg';
			}
		?>
		">
	</div>




                        </div>
                        <h5 class="content-group-lg">Add New Trainer</h5>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Horizontal form -->
                            <div class="panel panel-flat">
                                <div class="panel-body">

                                <form action="update_trainer.php" class="form-horizontal trainers" method="POST" enctype="multipart/form-data">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Full Name</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control"  placeholder="Provide trainers full name" name="fullName" value="<?php database_value('full_name');?>">
                                                    <?php errMsg('fullName_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Select Team</label>
                                                <div class="col-lg-9">
                                                <select class="bootstrap-select" data-width="100%" name="team">

                                                <option></option>

                                                <option value="PHP" <?php database_checked('team','PHP');?>>PHP
                                                </option>

                                                <option value="DotNet" <?php database_checked('team','DotNet');?>>Dot Net
                                                </option>

                                                <option value="JavaScript" <?php database_checked('team','JavaScript');?>>Javascript
                                                </option>

                                                <option value="Design" <?php database_checked('team','Design');?>>Web Design
                                                </option>

                                                <option value="SEO" <?php database_checked('team','SEO');?>>Digital Marketing
                                                </option>

                                                <option value="Android" <?php database_checked('team','Android');?>>Android Development
                                                </option>

                                                <option value="Cloud" <?php database_checked('team','Cloud');?>>Cloud Computing
                                                </option>

                                                <option value="Support" <?php database_checked('team','Support');?>>Support & Management
                                                </option>

                                                </select>

                                                    <?php errMsg('team_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Select Course</label>
                                                <div class="col-lg-9">
                                                    <select class="bootstrap-select" data-width="100%" name="course_id">

                                                        <option></option>

                                            <?php
                                                foreach($course_id_title as $course){
                                            ?>

                                            <option value="<?php echo $course['id'];?>" <?php database_checked('courses_id',$course['id']);?>><?php echo $course['title'];?>
                                            </option>

                                            <?php
                                                }
                                            ?>
                                                    </select>
                                                    <?php errMsg('course_id_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Trainer Status</label>
                                                <div class="col-lg-9">
                                                <select class="bootstrap-select" data-width="100%" name="trainer_status">
                                                
                                                <option></option>

                                                <option value="lead_trainer" <?php database_checked('trainer_status','lead_trainer');?>>Lead Trainer
                                                </option>

                                                <option value="assist_trainer" <?php database_checked('trainer_status','assist_trainer');?>>Assist Trainer
                                                </option>

                                                <option value="lab_assist" <?php database_checked('trainer_status','lab_assist');?>>Lab Assistant
                                                </option>

                                                </select>

                                                <?php errMsg('trainer_status_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Education Title</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="ex: B.Sc in CSE" name="EduTitle" value="<?php echo education("0");?>">
                                                    <?php errMsg('EduTitle_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Organization</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="ex: Dhaka University" name="organization" value="<?php echo education("1");?>">
                                                <?php errMsgSuc('organization_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Passing Year</label>
                                                <div class="col-lg-9">
                                                    <input type="number" class="form-control"  placeholder="Year You have passed" name="passingYear" value="<?php echo education("2");?>">
                                                <?php errMsgSuc('passingYear_required');?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Phone</label>
                                                <div class="col-lg-9">
                                                    <input type="number" class="form-control" placeholder="Trainers contact number" name="phone" value="<?php database_value('phone');?>">
                                                    <?php errMsg('phone_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Email</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="Trainers Email address" name="email" value="<?php database_value('email');?>">
                                                    <?php
                                                        errMsg('email_required');
                                                        errMsg('email_formateInvalid');
                                                        errMsg('email_exists');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Website</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control"  placeholder="Provide web url with http://" name="website" value="<?php database_value('website');?>">
                                                    <?php 
                                                        errMsg('website_invalid');
                                                        errMsgSuc('website_required');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Address</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="Trainers Address" name="houseAddress" value="<?php echo address("0");?>">
                                                    <?php errMsg('houseAddress_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"></label>
                                                <div class="col-lg-9">
                                                    <div class="col-lg-6" style="padding:0px">
                                                        <input type="text" class="form-control"  placeholder="District" name="district" value="<?php echo address("1");?>">
                                                    <?php errMsg('district_required');?>
                                                    </div>
                                                    <div class="col-lg-6" style="padding:0px">
                                                        <input type="number" class="form-control"  placeholder="ZIP Code" name="zipCode" value="<?php echo address("2");?>">
                                                        <?php errMsg('zipCode_required');?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Gender</label>
                                                <div class="col-lg-9">
                                                    <div class="text-left">
                                                        <label class="radio-inline">
                                                            <input type="radio" class="styled" name="gender" value="male" <?php database_checked_radio('gender','male');?>> Male
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" class="styled" name="gender" value="female" <?php database_checked_radio('gender','female');?>> Female
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Add Image</label>
                                                <div class="col-lg-9">
                                                    <div class="form-group has-feedback">
                                                        <input type="file" class="file-input bg-slate" name="image">
                                                        <?php
                                                        $_SESSION['img'] = $singleTrainer['image'];
                                                            errMsg('ErrorImageExtension');
                                                            errMsg('ErrorImageSize');
                                                            errMsgSuc('NeedImage');
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="id" value= "<?php echo $singleTrainer['unique_id'];?>">
                                            
                                        </div>
                                        <div class="text-right">
                                            <button class="btn btn-primary" type="submit">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- /grid -->
    
    
    <script type="text/javascript" src="../assets/js/pages/uploader_bootstrap.js"></script>

    <script type="text/javascript" src="../assets/js/plugins/uploaders/fileinput.min.js"></script>
    <?php include_once 'footer.php';


}else{

	$_SESSION["errorMsg"] = "Not found what you looking for.";
	header("location:error.php");
}





 ?>