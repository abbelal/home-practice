<?php
	require_once('session.php');
	$no_visible_elements=true;
	include('header.php'); 
?>

			<div class="row-fluid">
				<div class="span12 center login-header">
					<h2>Welcome to Mess Management Software</h2>
				</div><!--/span-->
			</div><!--/row-->
			
			<div class="row-fluid">
				<div class="well span5 center login-box">
					<div class="alert alert-info">
						<?php
							if(isset($_SESSION['error'])) {
								$error = $_SESSION['error'];
								echo '<p class="error">'.$error.'</p>';
								session_destroy();
							}
						 ?>
						Please login with your Username and Password.
					</div>
					<form class="form-horizontal" action="log/login.php" method="post">
						<fieldset>
							<div class="input-prepend" title="Username" data-rel="tooltip">
								<span class="add-on"><i class="icon-user"></i></span><input autofocus class="input-large span10" name="email" id="username" type="text" />
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password" data-rel="tooltip">
								<span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="pass" id="password" type="password" />
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend">
							<label class="remember" for="remember"><a href="forgotpassword.php" id="remember">Forgot Password</a></label>
							</div>
							<div class="clearfix"></div>

							<p class="center span5">
							<button type="submit" name="login" class="btn btn-primary">Login</button>
							</p>
						</fieldset>
					</form>
				</div><!--/span-->
			</div><!--/row-->
<?php include('footer.php'); ?>
