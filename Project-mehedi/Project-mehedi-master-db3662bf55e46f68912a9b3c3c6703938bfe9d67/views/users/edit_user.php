<?php
require_once '../../vendor/autoload.php';

use ProjectMehedi\user\user_registration\user_registration;
use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();
$objLoginUser->login_check();


$objEditUser = new user_registration();
$objEditUser->prepare($_GET);


$singleUser = $objEditUser->single_user();
$name = explode(' ', $singleUser['full_name']);

function database_checked_radio($value = "", $key = "") { // to hold checked of Radio 
    global $singleUser;
    if (isset($singleUser["$value"])) {
        if ($singleUser["$value"] == "$key") {
            echo 'checked="checked"';
        } else {
            echo '';
        }
    }
}

if (!empty($singleUser) && isset($singleUser)) {


    include_once '../header.php';
    include_once 'menubar.php';
    ?>
    <!-- Table -->
    <?php $objEditUser->session_message('updateSuccess'); ?>
    <div class="panel-flat">
        <!-- Grid -->

        <div class="row">
            <div class="col-lg-offset-2 col-lg-8">
                <div class="panel registration-form">
                    <div class="panel-body">
                        <div class="text-center">
                            <div class="thumb-rounded">
                                <div class="thumb thumb-rounded thumb-slide">
                                    <img alt="" class="img-circle" src="
                                    <?php
                                    if (isset($singleUser['image']) && !empty($singleUser['image'])) {
                                        echo '../assets/images/user/' . $singleUser['image'];
                                    } else {
                                        echo '../assets/images/user/nobody.jpg';
                                    }
                                    ?>
                                         ">

                                </div>
                            </div>
                            <h5 class="content-group-lg">Update Account Info</h5>
                            <!-- <form action="add_user_action.php" method="POST" enctype="multipart/form-data"> -->
                            <form action='update_user.php' method="POST" enctype="multipart/form-data">
                                <div class="form-group has-feedback">
                                    <input type="text" placeholder="Choose username" class="form-control border-slate" name="username" value="<?php
                                    if (!empty($_SESSION['username'])) {
                                        $objEditUser->session_message('username');
                                    } else {
                                        echo $singleUser['username'];
                                    }
                                    ?>">
                                    <div class="form-control-feedback">
                                        <i class="icon-user-plus text-muted"></i>
                                    </div>
                                    <?php
                                    if (isset($_SESSION['Uname_exists']) || isset($_SESSION['Uname_onlyText']) || isset($_SESSION['Uname_charLength']) || isset($_SESSION['Uname_required'])) {
                                        ?>
                                        <label class="validation-error-label text-left">
                                            <?php
                                            $objEditUser->session_message('Uname_exists');
                                            $objEditUser->session_message('Uname_onlyText');
                                            $objEditUser->session_message('Uname_charLength');
                                            $objEditUser->session_message('Uname_required');
                                            ?>
                                            <br></label>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="help-block text-right">Username is required</span>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <input type="text" placeholder="First name" class="form-control border-slate" name="firstname" value="<?php
                                    if (!empty($_SESSION['firstname'])) {
                                        $objEditUser->session_message('firstname');
                                    } else {
                                        echo $name['0'];
                                    }
                                    ?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-user-check text-muted"></i>
                                            </div>
                                            <?php
                                            if (isset($_SESSION['firstname_onlyText']) || isset($_SESSION['firstname_charLimit'])) {
                                                ?>
                                                <label class="validation-error-label text-left">
                                                    <?php
                                                    $objEditUser->session_message('firstname_onlyText');
                                                    $objEditUser->session_message('firstname_charLimit');
                                                    ?>
                                                    <br></label>
                                                <?php
                                            } elseif (isset($_SESSION['firstname_required'])) {
                                                ?>
                                                <label class="validation-error-label text-success text-left">
                                                    <?php
                                                    $objEditUser->session_message('firstname_required');
                                                    ?>
                                                    <br></label>
                                                <?php
                                            } else {
                                                ?>
                                                <span class="help-block text-right">Firstname is Optional</span>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <input type="text" placeholder="Last name" class="form-control border-slate" name="lastname" value="<?php
                                            if (!empty($_SESSION['lastname'])) {
                                                $objEditUser->session_message('lastname');
                                            } else {
                                                echo $name['1'];
                                            }
                                            ?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-user-check text-muted"></i>
                                            </div>
                                            <?php
                                            if (isset($_SESSION['lastname_onlyText']) || isset($_SESSION['lastname_charLimit'])) {
                                                ?>
                                                <label class="validation-error-label text-left">
                                                    <?php
                                                    $objEditUser->session_message('lastname_onlyText');
                                                    $objEditUser->session_message('lastname_charLimit');
                                                    ?>
                                                    <br></label>
                                                <?php
                                            } elseif (isset($_SESSION['lastname_required'])) {
                                                ?>
                                                <label class="validation-error-label text-success text-left">
                                                    <?php
                                                    $objEditUser->session_message('lastname_required');
                                                    ?>
                                                    <br></label>
                                                <?php
                                            } else {
                                                ?>
                                                <span class="help-block text-right">Lastname is Optional</span>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <input type="password" placeholder="Update password" class="form-control border-slate"  name="password">
                                            <div class="form-control-feedback">
                                                <i class="icon-user-lock text-muted"></i>
                                            </div>
                                            <?php $_SESSION['pass'] = $singleUser['password']; ?>
                                            <?php
                                            if (isset($_SESSION['password_digiteRequired']) || isset($_SESSION['password_charRequired']) || isset($_SESSION['password_charLength']) || isset($_SESSION['password_required'])) {
                                                ?>
                                                <label class="validation-error-label text-left">
                                                    <?php
                                                    $objEditUser->session_message('password_digiteRequired');
                                                    $objEditUser->session_message('password_charRequired');
                                                    $objEditUser->session_message('password_charLength');
                                                    $objEditUser->session_message('password_required');
                                                    ?>
                                                    <br></label>
                                                <?php
                                            } else {
                                                ?>
                                                <span class="help-block text-right">Password is required</span>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <input type="password" placeholder="Repeat password" class="form-control border-slate" name="confrm_password">
                                            <div class="form-control-feedback">
                                                <i class="icon-user-lock text-muted"></i>
                                            </div>
                                            <?php
                                            if (isset($_SESSION['confirmPassword_mismatch']) || isset($_SESSION['confrmPassword_required'])) {
                                                ?>
                                                <label class="validation-error-label text-left">
                                                    <?php
                                                    $objEditUser->session_message('confirmPassword_mismatch');
                                                    $objEditUser->session_message('confrmPassword_required');
                                                    ?>
                                                    <br></label>
                                                <?php
                                            } else {
                                                ?>
                                                <span class="help-block text-right">Password must be matched</span>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            <input type="email" placeholder="Your email" class="form-control border-slate"  name="email" value="<?php
                                            $_SESSION['mail'] = $singleUser['email'];

                                            if (!empty($_SESSION['email'])) {
                                                $objEditUser->session_message('email');
                                            } else {
                                                echo $singleUser['email'];
                                            }
                                            ?>">
                                            <div class="form-control-feedback">
                                                <i class="icon-mention text-muted"></i>
                                            </div>
                                            <?php
                                            if (isset($_SESSION['email_exists']) || isset($_SESSION['email_formateInvalid']) || isset($_SESSION['emai_required'])) {
                                                ?>
                                                <label class="validation-error-label text-left">
                                                    <?php
                                                    $objEditUser->session_message('email_exists');
                                                    $objEditUser->session_message('email_formateInvalid');
                                                    $objEditUser->session_message('emai_required');
                                                    ?>
                                                    <br></label>
                                                <?php
                                            } else {
                                                ?>
                                                <span class="help-block text-right">Email is required</span>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="file" class="file-input bg-slate" name="image">
                                    <?php
                                    $_SESSION['img'] = $singleUser['image'];

                                    if (isset($_SESSION['ErrorImageExtension']) || isset($_SESSION['ErrorImageSize'])) {
                                        ?>
                                        <label class="validation-error-label text-left">
                                            <?php
                                            $objEditUser->session_message('ErrorImageExtension');
                                            $objEditUser->session_message('ErrorImageSize');
                                            ?>
                                            <br></label>
                                        <?php
                                    } elseif (isset($_SESSION['NeedImage'])) {
                                        ?>
                                        <label class="validation-error-label text-success text-left">
                                            <?php
                                            $objEditUser->session_message('NeedImage');
                                            ?>
                                            <br></label>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="help-block text-right">Additional photo is Optional</span>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                                if ($_SESSION['logged']['is_admin'] == 1) {
                                    ?>
                                    <div class="text-left">
                                        <label class="radio-inline">
                                            <input type="radio" class="styled" name="is_admin" value="1" <?php database_checked_radio('is_admin', '1'); ?>>As Admin
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="styled" name="is_admin" value="2" <?php database_checked_radio('is_admin', '2'); ?>>
                                            As user
                                        </label>
                                    </div>
                                    <?php
                                }
                                ?>
                        </div>

                        <input type="hidden" name="id" value="<?php echo $singleUser['unique_id']; ?>">

                        <div class="text-right">
                            <button class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10" type="submit"><b><i class="icon-plus3"></i></b> Update account</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- /grid -->

        <script type="text/javascript" src="../assets/js/pages/uploader_bootstrap.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/uploaders/fileinput.min.js"></script>
        <?php
        include_once 'footer.php';
    } else {

        $_SESSION["errorMsg"] = "Not found what you looking for.";
        header("location:error.php");
    }
    ?>