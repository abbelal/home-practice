<?php
	require_once('session.php');
	require_once('include/db.php');
	require_once('include/validator.php');
	$no_visible_elements = true;
	include('header.php'); 
	
	//check request for reset password 
	$output = '';
	
	if(isset($_POST['reqpass'])) {
		$database = new db();
		$valid = new validator();
		$email = $valid->sanitize($_POST['email'], true);
		$data = array(
			'email' => array(
				'value' => $email,
				'type' => 'email'
			)
		);
		
		$errors = $valid->getData($data);
		
		if (count($errors) == 0) {
			$where = "email = '$email'";
			$check = $database->select_data('users', '*', $where);
			if(count($check) == 1) {
				
				$link = uniqid($check[0]['id']);
	
				$filed_data = array(
					'reset' => 1,
					'activation_code' => $link
				);
				
				if($database->update_data('users', $filed_data, $where)) {
					
					$headers = "From: Mess Management Software";
					$headers .= "Reply-To: aliakbar.reyad@gmail.com \r\n";
					$headers .= "CC: aliakbar.reyad@gmail.com \r\n";
					$headers .= "MIME-Version: 1.0\r\n";
					$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";  

					$subject = "Request For Password Reset";
					$message = '<html><body>';
					$message .= "Please reset your password with below link<br //><a href='resetpassword.php?reset={$link}'>Click this link</a>";
					$message .= '</body></html>';
	
					if(mail($email,$subject,$message,$headers)) {
						$output = "Mail sent OK";
					} 
					else {
					   $output = "Error sending email!";
					}
				}
				else {
					$output =  "internal problem has been occured";
				}
			}
			else {
				$output = "You are not a member";
			}
		}
		else {
			foreach($errors as $val) {
				$output .= "<p class='output'>$val</p>";
			}
		}
	}
?>

			<div class="row-fluid">
				<div class="span12 center login-header">
					<h2>Forgot Password</h2>
				</div><!--/span-->
			</div><!--/row-->
			
			<div class="row-fluid">
				<div class="well span5 center login-box">
					<div class="alert alert-info">
						<?php
							if(empty($output)) {
								echo "Please Provide Your Email.";
							}
							else {
								echo "<span style='color: red'>{$output}</span>";
							}
						?>
					</div>
					<form class="form-horizontal" action="#" method="post">
						<fieldset>
							<div class="input-prepend" title="Username" data-rel="tooltip">
								<span class="add-on"><i class="icon-user"></i></span><input autocomplete="false" autofocus class="input-large span10" name="email" id="username" type="text" />
							</div>
							<div class="clearfix"></div>
							<div class="clearfix"></div>

							<p class="center span5">
							<button type="submit" name="reqpass" class="btn btn-primary">Send Email</button>
							</p>
						</fieldset>
					</form>
				</div><!--/span-->
			</div><!--/row-->
<?php include('footer.php'); ?>
