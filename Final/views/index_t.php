
<?php
include_once ("../vendor/autoload.php");

use App\Project;

$obj = new Project();
$Alldata = $obj->index();
if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Index|Data</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->


        <!-- Core JS files -->
        <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/forms/inputs/touchspin.min.js"></script>

        <script type="text/javascript" src="assets/js/core/app.js"></script>
        <script type="text/javascript" src="assets/js/pages/form_input_groups.js"></script>
        <!-- /theme JS files -->

        <!--my custom css styles-->
        <link href="styleforCreate.css" rel="stylesheet" type="text/css">

    </head>
    <body>
        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt=""></a>

                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="sidebar-control sidebar-main-toggle hidden-xs">
                            <i class="icon-paragraph-justify3"></i>
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/flags/us.png" alt="">
                            <span>English</span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#"><img src="assets/images/flags/us.png" alt=""><span>English</span></a></li>
                            <li><a href="#"><img src="assets/images/flags/bd.png" alt=""><span>Bangla</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">PHP Debugger</span>
                                        <div class="text-size-mini text-muted">
                                            <i class="icon-pin text-size-small"></i> &nbsp; BITM, Dhaka
                                        </div>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list">
                                            <li>
                                                <a href="#"><i class="icon-cog3"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->


                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">



                                    <!-- Forms -->
                                    <li class="navigation-header"><span>Forms</span> <i class="icon-menu" title="Forms"></i></li>
                                    <li>
                                        <a href="#"><i class="icon-pencil3"></i> <span>Form components</span></a>
                                        <ul>
                                            <li><a href="form_inputs_basic.html">Basic inputs</a></li>
                                            <li><a href="form_checkboxes_radios.html">Checkboxes &amp; radios</a></li>
                                            <li class="active"><a href="form_input_groups.html">Input groups</a></li>
                                            <li><a href="form_controls_extended.html">Extended controls</a></li>
                                            <li>
                                                <a href="#">Selects</a>
                                                <ul>
                                                    <li><a href="form_select2.html">Select2 selects</a></li>
                                                    <li><a href="form_multiselect.html">Bootstrap multiselect</a></li>
                                                    <li><a href="form_select_box_it.html">SelectBoxIt selects</a></li>
                                                    <li><a href="form_bootstrap_select.html">Bootstrap selects</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="form_tag_inputs.html">Tag inputs</a></li>
                                            <li><a href="form_dual_listboxes.html">Dual Listboxes</a></li>
                                            <li><a href="form_editable.html">Editable forms</a></li>
                                            <li><a href="form_validation.html">Validation</a></li>
                                            <li><a href="form_inputs_grid.html">Inputs grid</a></li>
                                        </ul>
                                    </li>

                                    <!-- /forms -->



                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->

                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Forms</span> Data</h4>
                            </div>
                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                                <li class="active">Data</li>
                            </ul>
                        </div>
                    </div>
                    <!-- /page header -->


                    <div class="main-content">
                        <div class="table-responsive">
                            <table class="table table-bordered" border="1">
                                <tr>
                                    <th>SL</th>
                                    <th>Full Name</th>
                                    <th>Educational Status</th>
                                    <th>Team</th>
                                    <th>Courses Name</th>
                                    <th>Trainer Status</th>
                                    <th>Image</th>

                                    <th colspan="3">Action</th>
                                </tr>
                                <?php
                                $serial = 1;
                                if (isset($Alldata) && !empty($Alldata)) {

                                    foreach ($Alldata as $Singledata) {
                                        ?>

                                        <tr>
                                            <td><?php echo $serial++ ?></td>
                                            <td><?php echo $Singledata['full_name'] ?></td>
                                            <td><?php echo $Singledata['edu_status'] ?></td>
                                            <td><?php echo $Singledata['team'] ?></td>
                                            <td><?php echo $Singledata['title'] ?></td>
                                            <td><?php echo $Singledata['trainer_status'] ?></td>
                                            <td><img src="<?php echo "images/" . $Singledata['image'] ?>" width="200" height="150"></td>
                                            <td><a href="show.php?id=<?php echo $Singledata['id'] ?>"><i class="icon-file-eye"></i>View</a></td>
                                            <td><a href="edit.php?id=<?php echo $Singledata['id'] ?>"><i class="icon-pencil7"></i>Edit</a></td>
                                            <td><a href="delete.php?id=<?php echo $Singledata['id'] ?>"><i class="icon-trash"></i>Remove</a></td>

                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="15">
                                            No available data
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                        <a class="deletelist" href="restore.php">Click to see deleted list</a>
                        <span id="utility">Download as <a href="pdf.php">PDF</a></span>
                        <a class="addnew" href="create.php">Add New Trainee</a>
						<div class="footer text-muted">
                            &copy; 2015. <a href="#">Trainer Apps</a> by <a target="_blank" href="#">PHP Debugger</a>
                        </div>
                    </div>
                </div>
                <!-- /content wrapper -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
    </body>
</html>
