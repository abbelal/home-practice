<?php
require_once '../../vendor/autoload.php';

use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();

$objLoginUser -> prepare($_POST);

$objLoginUser -> login();
