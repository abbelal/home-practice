<?php

include_once '../vendor/autoload.php';

use signupForm\resistrationForm\resistrationForm;

$object = new resistrationForm();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //user name validation
    if (strlen($_POST['userName']) >= 6 && strlen($_POST['userName']) <= 12) {
        $object->prepare($_POST);
    } else {
        $_SESSION['errName'] = "User name must be 6 to 12 charecter";
        header('location:create.php');
    }

    //email validation
    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) == TRUE) {
       $object->prepare($_POST);
    } else {
        $_SESSION['errEmail'] = "This is not valid Email address";
        header('location:create.php');
    }

    //password validation
    if (strlen($_POST['password']) >= 6 && strlen($_POST['password']) <= 12) {
        $object->prepare($_POST);
    } else {
        $_SESSION['errPassword'] = "password must be 6 to 12 charecter";
        header('location:create.php');
    }

    //repeat password validation
    if ($_POST['repPassword'] == $_POST['password']) {
       $object->prepare($_POST);
    } else {
        $_SESSION['errRepPass'] = "Password not matched";
        header('location:create.php');
    }
    
    $object->signup();

    //
    //
    //
    //
} else {
    $_SESSION['err_msg'] = "<h1>404 page not found</h1>";
    header('location:error.php');
}
?>
