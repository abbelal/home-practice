<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\trainers\trainers;
$objLoginUser = new user_login();
$objLoginUser -> login_check();
$objAllTrainers = new trainers();
$allTrainer = $objAllTrainers -> all_trainer();

include_once '../header.php';
include_once 'menubar.php';

?>
<?php $objAllTrainers -> session_message('trainerDisabled'); ?>
<div class="panel panel-flat">
	<div class="panel-body">
		<h3 class="content-group text-semibold" style="margin:0px!important;">
		Lead Trainer
		<small class="display-block">All Lead trainer for BITM</small>
		</h3>
	</div>
</div>
<div class="row">

	<?php $serial_lead_trainer = 0;
		foreach ($allTrainer as $key => $singleTrainer) {
		if($singleTrainer['trainer_status'] == 'lead_trainer'){
	?>
	<div class="col-lg-3 col-sm-6">
		<div class="thumbnail">
			<div class="thumb">
				<img src="
				<?php
					if(isset($singleTrainer['image']) && !empty($singleTrainer['image'])){
						echo '../assets/images/trainer/'.$singleTrainer['image'];
					}else{
						echo '../assets/images/trainer/nobody.jpg';
					}

				 ?>" alt="">
				<div class="caption-overflow">
					<span>
						<a href="single_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded ">Details <i class="icon-plus3"></i></a>
					</span>
				</div>
			</div>
			<div class="caption">
				<div class="text-center">
					<h6 class="text-semibold no-margin"><?php echo $singleTrainer['full_name'];?>
						<small class="display-block">
							<span class="label bg-teal-400"><?php echo ($singleTrainer['trainer_status'] == 'lead_trainer')?'Lead Trainer':'';?></span>
						</small>
					</h6>
					<?php 
						$eduction = unserialize($singleTrainer['edu_status']);
					?>
					<p><?php echo $eduction[0].", ".$eduction[1]."."?></p>
					<p>Assigned for <?php echo $singleTrainer['title'];?></p>
				</div>
				<ul class="list-unstyled list-icons">
					<li><i class="icon-phone position-left"></i> <?php echo $singleTrainer['phone'];?>
					</li>
					<li><i class="icon-mail5 position-left"></i><a href="mailto:<?php echo $singleTrainer['email'];?>"><?php echo $singleTrainer['email'];?></a>
					</li>
				</ul>
				<div class="text-center">
					
					<a type="button" class="btn btn-default" href="edit_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>"><i class="icon-pencil7  position-left"></i>Edit</a>
					<?php
                        if($_SESSION['logged']['is_admin'] == 1){
                    ?>
					<a type="button" class="btn btn-default" href="delete_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>" onclick = "return confirm('Are you sure to disable this Lead trainer?')"><i class="icon-pencil7 position-left"></i> Disable</a>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>
	<?php
	$serial_lead_trainer++;
			}
			if($serial_lead_trainer % 4 == 0){
		?>
			</div> <!-- row -->
		<div class="row">
	<?php
		}			
	}// foreach
	?>
	</div> <!-- row -->
	<!-- Assistant trainer -->
	<div class="panel panel-flat">
		<div class="panel-body">
			<h3 class="content-group text-semibold" style="margin:0px!important;">
			Assistant Trainer
			<small class="display-block">All Assistant trainer for BITM</small>
			</h3>
		</div>
	</div>
	<div class="row">
	<?php 
		$serial_assist_trainer = 0;
		foreach ($allTrainer as $key => $singleTrainer) {
			
		if($singleTrainer['trainer_status'] == 'assist_trainer'){

	?>
	<div class="col-lg-3 col-sm-6">
		<div class="thumbnail">
			<div class="thumb">
				<img src="
				<?php
					if(isset($singleTrainer['image']) && !empty($singleTrainer['image'])){
						echo '../assets/images/trainer/'.$singleTrainer['image'];
					}else{
						echo '../assets/images/trainer/nobody.jpg';
					}

				 ?>" alt="">
				<div class="caption-overflow">
					<span>
						<a href="single_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded ">Details <i class="icon-plus3"></i></a>
					</span>
				</div>
			</div>
			<div class="caption">
				<div class="text-center">
					<h6 class="text-semibold no-margin"><?php echo $singleTrainer['full_name'];?>
						<small class="display-block">
							<span class="label bg-teal-400"><?php echo ($singleTrainer['trainer_status'] == 'assist_trainer')?'Assistant Trainer':'';?></span>
						</small>
					</h6>
					<?php
						$eduction = unserialize($singleTrainer['edu_status']);
					?>
					<p><?php echo $eduction[0].", ".$eduction[1]."."?></p>
					<p>Assigned for <?php echo $singleTrainer['title'];?></p>
				</div>
				<ul class="list-unstyled list-icons">
					<li><i class="icon-phone position-left"></i> <?php echo $singleTrainer['phone'];?>
					</li>
					<li><i class="icon-mail5 position-left"></i><a href="mailto:<?php echo $singleTrainer['email'];?>"><?php echo $singleTrainer['email'];?></a></a>
					</li>
				</ul>
				<div class="text-center">
					
					<a type="button" class="btn btn-default" href="edit_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>"><i class="icon-pencil7  position-left"></i>Edit</a>
					<?php
                        if($_SESSION['logged']['is_admin'] == 1){
                    ?>
					<a type="button" class="btn btn-default" href="delete_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>" onclick = "return confirm('Are you sure to disable this Lead trainer?')"><i class="icon-pencil7 position-left"></i> Disable</a>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>
	<?php
	$serial_assist_trainer++;
			}
			if($serial_assist_trainer % 4 == 0){
		?>
			</div> <!-- row -->
		<div class="row">
	<?php
		}			
	}// foreach
	?>
	</div> <!-- row -->

	<!-- Lab Assistant -->
	<div class="panel panel-flat">
		<div class="panel-body">
			<h3 class="content-group text-semibold" style="margin:0px!important;">
			Lab Assistant
			<small class="display-block">All Lab Assistant for BITM</small>
			</h3>
		</div>
	</div>
	<div class="row">
	<?php $serial_lab_assist_trainer = 0;
		foreach ($allTrainer as $key => $singleTrainer) {
			if($singleTrainer['trainer_status'] == 'lab_assist'){
	?>
	<div class="col-lg-3 col-sm-6">
		<div class="thumbnail">
			<div class="thumb">
				<img src="
				<?php
					if(isset($singleTrainer['image']) && !empty($singleTrainer['image'])){
						echo '../assets/images/trainer/'.$singleTrainer['image'];
					}else{
						echo '../assets/images/trainer/nobody.jpg';
					}

				 ?>" alt="">
				<div class="caption-overflow">
					<span>
						<a href="single_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded ">Details <i class="icon-plus3"></i></a>
					</span>
				</div>
			</div>
			<div class="caption">
				<div class="text-center">
					<h6 class="text-semibold no-margin"><?php echo $singleTrainer['full_name'];?>
						<small class="display-block">
							<span class="label bg-teal-400"><?php echo ($singleTrainer['trainer_status'] == 'lab_assist')?'Lab Assistant':'';?></span>
						</small>
					</h6>
					<?php 
						$eduction = unserialize($singleTrainer['edu_status']);
					?>
					<p><?php echo $eduction[0].", ".$eduction[1]."."?></p>
					<p>Assigned for <?php echo $singleTrainer['title'];?></p>
				</div>
				<ul class="list-unstyled list-icons">
					<li><i class="icon-phone position-left"></i> <?php echo $singleTrainer['phone'];?>
					</li>
					<li><i class="icon-mail5 position-left"></i><a href="mailto:<?php echo $singleTrainer['email'];?>"><?php echo $singleTrainer['email'];?></a></a>
					</li>
				</ul>
				<div class="text-center">
					
					<a type="button" class="btn btn-default" href="edit_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>"><i class="icon-pencil7  position-left"></i>Edit</a>
					<?php
                        if($_SESSION['logged']['is_admin'] == 1){
                    ?>
					<a type="button" class="btn btn-default" href="delete_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>" onclick = "return confirm('Are you sure to disable this Lead trainer?')"><i class="icon-pencil7 position-left"></i> Disable</a>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>
	<?php
	$serial_lab_assist_trainer++;
			}
			if($serial_lab_assist_trainer % 4 == 0){
		?>
			</div> <!-- row -->
		<div class="row">
	<?php
		}			
	}// foreach
	?>
	</div> <!-- row -->
	<?php  include_once 'footer.php'; ?>