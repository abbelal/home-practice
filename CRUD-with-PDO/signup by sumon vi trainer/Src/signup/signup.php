<?php

namespace nssignup\signup;

use PDO;

class signup {

    public $id = '';
    public $verify = '';
    public $username = '';
    public $email = '';
    public $password = '';
    public $is_active = '';
    public $is_admin = '';
    public $create = '';
    public $modify = '';
    public $delete = '';
    public $conn = '';
    public $dbuser = 'root';
    public $dbpass = '';

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=sumonsignup', $this->dbuser, $this->dbpass);
    }

    public function prepare($data = '') {

        if (array_key_exists('user_name', $data) && !empty($data['user_name'])) {
            $this->user_name = $data['user_name'];
        } else {
            $_SESSION['user_session'] = "User name required";
        }


        if (array_key_exists('pass', $data) && !empty($data['pass'])) {
            $this->pass = $data['pass'];
        } else {
            $_SESSION['pass_session'] = "Password required";
        }

        if (array_key_exists('repass', $data) && !empty($data['repass'])) {
            $this->repass = $data['repass'];
        } else {
            $_SESSION['repass_session'] = "Re-password  required";
        }
        if (array_key_exists('email', $data) && !empty($data['email'])) {
            $this->email = $data['email'];
        } else {
            $_SESSION['email_session'] = "E-mail required";
        }

        return $this;
    }

    public function store() {


        try {

            $query = "INSERT INTO tbl_sumonsignup (id, unique_id, verify, username, email, password, is_active, is_admin, create, modify, delete) VALUES (:id, :unique_id, :verify, :username, :email, :password, :is_active, :is_admin, :created);";

            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':id' => null,
                ':unique_id' => uniqid(),
                ':verify' => uniqid(),
                ':username' => $this->username,
                ':email' => $this->email,
                ':password' => $this->password,
                ':is_active' => 0,
                ':is_admin' => 0,
                ':created' => date("Y-m-d h:i:s"),
            ));
            header('location:index.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

}