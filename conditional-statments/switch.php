<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>loops</title>
    </head>
    <body>
        <h1>switch<br/>
            <?php
            $favcolor = "vilo";
            switch ($favcolor) {
                case 'red':
                    echo "this is red color";
                    break;
                case 'blue':
                    echo "this is blue color";
                    break;
                case 'white':
                    echo "this is white color";
                    break;
                default:
                    echo "your inputed color is not matched";
            }
            ?>
        </h1>

    </body>
</html>
