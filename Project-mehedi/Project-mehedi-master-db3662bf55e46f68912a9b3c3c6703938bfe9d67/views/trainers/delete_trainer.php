<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\trainers\trainers;
$objLoginUser = new user_login();
$objLoginUser -> login_check();


$objDeleteTrainers = new trainers();

$objDeleteTrainers -> prepare($_GET);

$objDeleteTrainers -> disable_trainer();