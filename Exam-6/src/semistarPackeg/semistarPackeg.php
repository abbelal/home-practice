<?php
namespace packegApp\semistarPackeg;
class semistarPackeg {

    public $id = '';
    public $name = '';
    public $semister = '';
    public $offer = '';
    public $weaber = '';
    public $total_cost = '';
    public $sem_cost = '';
    public $first_sem = 9000;
    public $second_sem = 11000;
    public $third_sem = 13000;
    public $DataList = '';

    public function __construct() {
        session_start();
        $sqlConn = mysql_connect('localhost', 'root', '') or die("Ops..!! You can not connect with MySql");
        mysql_select_db('personalinfo') or die("You can not connect with Database");
    }

    public function prepare($data='') {

        if (array_key_exists('name', $data) && !empty($data['name'])) {
            $this->name = $data['name'];
        } else {
            $_SESSION['name_msg'] = "Name Required";
        }

        if (array_key_exists('semister', $data) && !empty($data['semister'])) {
            $this->semister = $data['semister'];
        } else {
            $_SESSION['semister_msg'] = "Semister Required";
        }

        if (array_key_exists('offer', $data) && !empty($data['offer'])) {
            $this->offer = $data['offer'];
        } else {
            $this->offer = 'no';
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

//-----------------------------cost and weaber condition---------------------

        if ($this->semister == '1st') {
            $this->sem_cost = 9000;
            $this->weaber = 0;
            $this->total_cost = 9000;

            if ($this->offer == 'yes') {
                $this->sem_cost = 9000;
                $this->weaber = ($this->first_sem * 10) / 100;
                $this->total_cost = $this->first_sem - $this->weaber;
            }
        } elseif ($this->semister == '2nd') {
            $this->sem_cost = 11000;
            $this->weaber = 0;
            $this->total_cost = 11000;

            if ($this->offer == 'yes') {
                $this->sem_cost = 11000;
                $this->weaber = ($this->second_sem * 10) / 100;
                $this->total_cost = $this->second_sem - $this->weaber;
            }
        } elseif ($this->semister == '3rd') {
            $this->sem_cost = 13000;
            $this->weaber = 0;
            $this->total_cost = 13000;

            if ($this->offer == 'yes') {
                $this->sem_cost = 13000;
                $this->weaber = ($this->third_sem * 10) / 100;
                $this->total_cost = $this->third_sem - $this->weaber;
            }
        }
//-----------------------------end cost and weaber condition---------------------
        $_SESSION['Form_Data']=$data;
        return $this;
        
    }

//    end prepare methode 



    public function store() {
        if ($this->semister == '1st' || $this->semister == '2nd' || $this->semister == '3rd') {
            if (!empty($this->name) && !empty($this->semister)) {
                $seletQuery = "INSERT INTO `semisterpackeg` (`id`, `unique_id` , `name`, `semister`, `offer`,`cost`,`webar`,`total_cost`) VALUES (NULL, '" . uniqid() . "' , '$this->name', '$this->semister', '$this->offer', '$this->sem_cost', '$this->weaber', '$this->total_cost')";
                if (mysql_query($seletQuery)) {
                    $_SESSION['submit_msg'] = "Data successfully submited";
                } else {
                    echo "<h1>Opps...!!! You can not successfully added your model</h1>";
                }
                header('location:create.php');
            }
        } else {
            $_SESSION['condition_msg'] = "<br/>Sorry..! you can use only <b>1st</b> or <b>2nd</b> or <b>3rd</b> as a semister";
            header('location:create.php');
        }
    }

    public function semisterList() {
        $selectQuery = "SELECT * FROM `semisterpackeg`";
        $queryData = mysql_query($selectQuery);
        while ($singleQueryData = mysql_fetch_assoc($queryData)) {
            $this->DataList[] = $singleQueryData;
        }
        return $this->DataList;
    }

    public function singleDataShow() {
        $showQuery = "SELECT * FROM `semisterpackeg` WHERE `unique_id` =" . "'$this->id'" . "";
        $mydata = mysql_query($showQuery);
        $row = mysql_fetch_assoc($mydata);
        return $row;
    }

    public function dataUpdate() {
        if ($this->semister == '1st' || $this->semister == '2nd' || $this->semister == '3rd') {
            if (!empty($this->name) && !empty($this->semister)) {
                $updateQuery = "UPDATE `semisterpackeg` SET `name` = '$this->name', `semister`='$this->semister', `offer`='$this->offer', `cost`='$this->sem_cost', `webar`='$this->weaber', `total_cost`='$this->total_cost' WHERE `semisterpackeg`.`unique_id` =" . "'$this->id'" . "";
                if (mysql_query($updateQuery)) {
                    $_SESSION['message'] = "successfully updated";
                }else {
                    echo "<h1>Opps...!!! You can not successfully added your model</h1>";
                }
                header("location:edit.php?id=$this->id");
            }
        } else {
            $_SESSION['condition_msg'] = "<br/>Sorry..! you can use only <b>1st</b> or <b>2nd</b> or <b>3rd</b> as a semister";
//            header('location:edit.php?id=$this->id');
        }
    }
    
    
    public function delete(){
        $deleteQuery = "DELETE FROM `semisterpackeg` WHERE `semisterpackeg`.`unique_id` = " . "'$this->id'" . "";
        if (mysql_query($deleteQuery)) {
            $_SESSION['message'] = "<h1>data deleted have you any problem..?</h1>";
        }
        header("location:index.php");
    }

}

?>
