<?php
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
		require_once('header.php');
		require_once('include/db.php');
		$database = new db();
		if(isset($_POST['search'])) {
			$cur_date = $_POST['year'].'-'.$_POST['month'];
		}
		else {
			$cur_date = date('Y-m');
		}
		$num_day = date('t');
?>


			<div id="adminbar">
				<ul style="float: left" class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Report</a>
					</li>
				</ul>
				<ul style="float: right" class="breadcrumb">
					<li>
						<?php echo $_SESSION['user']['name']; ?><span class="divider">|</span>
					</li>
					<?php
						if($_SESSION['user']['type'] == 1) {
							$usertype = 'Admin';
						}
						else if($_SESSION['user']['type'] == 2) {
							$usertype = 'Manager';
						}
						else if($_SESSION['user']['type'] == 3) {
							$usertype = 'Member';
						}
					?>
					<li>
						<?php echo $usertype; ?>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>Final Report</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
							<select name="month" id="">
								<option value="">---Select Month---</option>
								<?php
									require_once('include/function.php');
									$search_with_month = getMonthArray();
									foreach($search_with_month as $key=>$value) {
										echo "<option value='{$key}'>{$value}</option>";
									}
								?>
							</select>
							<select name="year" id="">
								<option value="">---Select Year---</option>
								<?php
									for($i = date('Y'); $i > 2010; $i--) {
										echo "<option value='{$i}'>{$i}</option>";
									}
								?>
							</select>
							<input class="btn btn-primary" type="submit" name="search" value="Search" />
						</form>
						<?php
							$total_cost = 0;
							$database->sql = "SELECT sum(total_price) AS manager_cost
										FROM manager_cost where creation_date like '{$cur_date}-%'";
							if($cost = $database->query($database->sql)){
								if($data =  $cost->fetch_assoc()){
									$total_cost += $data['manager_cost'];
								}
							}
							echo "<h4>Manager Cost: {$data['manager_cost']}</h4>";
							$database->sql = "SELECT sum(total_price) AS member_cost
										FROM member_cost where creation_date like '{$cur_date}-%'";
							if($cost = $database->query($database->sql)){
								if($data =  $cost->fetch_assoc()){
									$total_cost += $data['member_cost'];
								}
							}
							echo "<h4>Member Cost: ".$data['member_cost'].'</h4>';
							echo "<h4>Total Cost: ".$total_cost.'</h4>';
							$database->sql = "SELECT sum(total_meal) AS total_meal
										FROM meal_entry where `date` like '{$cur_date}-%'";
							if($meal = $database->query($database->sql)){
								if($data =  $meal->fetch_assoc()){
									$total_meal = $data['total_meal'];
								}
							}
							echo "<h4>Total Meal: ".$total_meal.'</h4>';
							$meal_rate = $total_cost/$total_meal;
							echo "<h4>Meal Rate: ".sprintf("%.2f", $meal_rate).'</h4>';
						?>
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>Deposit</th>
								<th>Bazaar Cost</th>
								<th>Total Deposit</th>
								<th>Total Meal</th>
								<th>Meal Cost</th>
								<th>Debit</th>
								<th>Credit</th>
							</tr>
						</thead>
						<?php 
							$sl = 0;
							$database->sql = "SELECT u.name,u.id, mc.total_price, c.collection_amount, meal.total_meal
							FROM users u LEFT JOIN (
											SELECT sum( total_price ) AS total_price, user_id FROM member_cost
											WHERE creation_date LIKE '{$cur_date}-%'
											GROUP BY user_id
										)mc ON u.id = mc.user_id
										LEFT JOIN (
											SELECT sum( amount ) collection_amount, user_id
											FROM collection
											WHERE collection_date LIKE '{$cur_date}-%'
											GROUP BY user_id
										)c ON c.user_id = u.id
										LEFT JOIN (
											SELECT sum(total_meal) as total_meal, user_id 
											from meal_entry 
											where `date` LIKE '{$cur_date}-%' 
											group by user_id
										) meal on u.id = meal.user_id";
							if($res = $database->query($database->sql)){
								while($data =  $res->fetch_assoc()){
									$sl++;
									$indivual_meal_cost = 0;
									$indivual_deposit = 0;
									$debit = 0;
									$credit = 0;
									$indivual_deposit = $data['total_price'] + $data['collection_amount'];
									$indivual_meal_cost = $data['total_meal'] * $meal_rate;
									if($indivual_meal_cost == $indivual_deposit) {
										$debit = 0;
										$credit = 0;
									}
									else if($indivual_meal_cost > $indivual_deposit) {
										$credit = ($indivual_meal_cost - $indivual_deposit);
									}
									else {
										$debit = ($indivual_deposit - $indivual_meal_cost);
									}
						?>
							<tr>
								<td><?php echo $sl; ?></td>
								<td><?php echo $data['name']; ?></td>
								<td><?php echo $data['collection_amount']; ?></td>
								<td><?php echo $data['total_price']; ?></td>
								<td><?php echo $indivual_deposit; ?></td>
								<td><?php echo $data['total_meal']; ?></td>
								<td><?php echo round($indivual_meal_cost); ?></td>
								<td><?php echo round($debit); ?></td>
								<td><?php echo round($credit); ?></td>
							</tr>
						<?php 
								}
							}
						?>
						</table>
					</div>
				</div><!--/span-->
			</div><!--/row-->
<?php 
	require_once('footer.php');
	}
?>
