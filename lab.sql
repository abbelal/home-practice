-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 14, 2016 at 05:15 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lab`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `title` varchar(111) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `course_type` varchar(50) NOT NULL,
  `course_fee` varchar(111) NOT NULL,
  `is_offer` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `unique_id`, `title`, `duration`, `description`, `course_type`, `course_fee`, `is_offer`, `created`, `updated`, `deleted`) VALUES
(1, '', 'Web application PHP', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '', 'Microsoft.net', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '', 'Web Design', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '', 'Marketing', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_trainer_lab_mapping`
--

CREATE TABLE `course_trainer_lab_mapping` (
  `id` int(11) NOT NULL,
  `course_id` int(111) NOT NULL,
  `batch_no` varchar(111) NOT NULL,
  `lead_trainer` varchar(111) NOT NULL,
  `asst_trainer` varchar(111) NOT NULL,
  `lab_asst` varchar(111) NOT NULL,
  `lab_id` int(111) NOT NULL,
  `start_date` varchar(111) NOT NULL,
  `ending_date` varchar(111) NOT NULL,
  `start_time` varchar(111) NOT NULL,
  `ending_time` varchar(111) NOT NULL,
  `day` varchar(111) NOT NULL,
  `is_running` int(11) NOT NULL,
  `assigned_by` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `installed_softwares`
--

CREATE TABLE `installed_softwares` (
  `id` int(11) NOT NULL,
  `labinfo_id` int(111) NOT NULL,
  `software_title` varchar(111) NOT NULL,
  `version` varchar(111) NOT NULL,
  `software_type` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `labinfo`
--

CREATE TABLE `labinfo` (
  `id` int(11) NOT NULL,
  `course_id` int(100) NOT NULL,
  `lab_no` varchar(111) NOT NULL,
  `seat_capacity` varchar(111) NOT NULL,
  `projector_resolution` varchar(111) NOT NULL,
  `ac_status` varchar(111) NOT NULL,
  `pc_configuration` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `trainer_pc_configuration` varchar(255) NOT NULL,
  `table_capacity` varchar(100) NOT NULL,
  `internet_speed` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE `trainers` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `edu_status` varchar(255) NOT NULL,
  `team` varchar(111) NOT NULL,
  `courses_id` int(11) NOT NULL,
  `trainer_status` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(111) NOT NULL,
  `address` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `web` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainers`
--

INSERT INTO `trainers` (`id`, `unique_id`, `full_name`, `edu_status`, `team`, `courses_id`, `trainer_status`, `image`, `phone`, `email`, `address`, `gender`, `web`, `created`, `updated`, `deleted`, `is_delete`) VALUES
(27, '57afc11024d37', 'this is belal', 'BSE in CSE.Dhaka international university.2019', 'Team3', 3, 'leadtrainer', '57afc1102494f1471136016Belal  (86).jpg', '012369145', 'belal@belal.com', 'shibpur narsingdi dhaka 1230', 'Male ', 'abbelal.tk', '2016-08-14 02:08:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(28, '57afc19a4c5a5', 'rakib hossain', 'BSE in CSE.dhaka model college.2030', 'Team5', 4, 'leadtrainer', '57afc19a4c5a5147113615420140806_233806.jpg', '1254965236', 'rakib@ta.com', 'dhaka kalabagan dhanmondi 12325', 'Male ', 'djhdfg', '2016-08-14 02:08:54', '0000-00-00 00:00:00', '2016-08-14 02:08:45', 1),
(29, '57afc1ba13e96', 'rakib hossain', 'BSE in CSE..', '', 1, '', '57afc1ba13e96147113618620140806_233806.jpg', '1254965236', 'rakib@ta.com', '   ', '', '', '2016-08-14 02:08:26', '0000-00-00 00:00:00', '2016-08-14 02:08:52', 1),
(30, '57afc1dd303bf', 'rakib hossain', 'BSE in CSE..', '', 1, '', '57afc1dd2ffd71471136221example5.png', '1254965236', 'rakib@ta.com', '   ', '', '', '2016-08-14 02:08:01', '0000-00-00 00:00:00', '2016-08-14 02:08:52', 1),
(31, '57afc22133de9', 'rakib hossain', 'BSE in CSE..', '', 1, '', '57afc22133a011471136289example5.png', '1254965236', 'rakib@ta.com', '   ', '', '', '2016-08-14 02:08:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(32, '57afc26e166ce', 'sgs', 'zfdbsdhh.dsh.shs', 'Team3', 4, 'labasst', '57afc26e166ce1471136366750-green-natural-background-pattern-vector-full.png', 'sdhsh', 'rhasan@gmail.com', 'fgjdyj yjtyr fhgjftj fh284', 'Male ', '', '2016-08-14 02:08:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(10) NOT NULL,
  `is_admin` int(10) NOT NULL,
  `is_delete` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_trainer_lab_mapping`
--
ALTER TABLE `course_trainer_lab_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labinfo`
--
ALTER TABLE `labinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainers`
--
ALTER TABLE `trainers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `labinfo`
--
ALTER TABLE `labinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trainers`
--
ALTER TABLE `trainers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
