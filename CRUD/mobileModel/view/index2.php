
<html>
    <head>
        <meta charset="UTF-8">
        <title>mobile model</title>
    </head>
    <body>
        <?php
        include_once '../vendor/autoload.php';
        use mobileApp\Mobilemodel;
        $obj = new Mobilemodel();
        $showList = $obj->mobileList();

        if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        ?>
        <h1><a href="create2.php">Add new model</a></h1>
        <table border="1">
            <tr>
                <th>Id</th>
                <th>mobile_model</th>
                <th>laptop_model</th>
                <th colspan="3">Action</th>
            </tr>
            <?php
            $serial = 1;
            if (isset($_SESSION['formsallData']) && !empty($_SESSION['formsallData'])) {
                foreach ($_SESSION['formsallData'] as $key=> $singledata) {
                    ?>
                    <tr>
                        <td><?php echo $serial++ ?></td>
                        <td><?php echo $singledata['mModel'] ?></td>
                        <td><?php echo $singledata['lModel'] ?></td>
                        <td><a href="show2.php?id=<?php echo $key ?>">View</a></td>
                        <td><a href="edit2.php?id=<?php echo $key ?>">Update</a></td>
                        <td><a href="delete2.php?id=<?php echo $key ?>">Delete</a></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="4" align="center"> Durh mia....!!! Data nai </td>
                </tr>
                <?php
            }
            ?>

        </table>

    </body>
</html>