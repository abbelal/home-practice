<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_registration\user_registration;

use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();
$objLoginUser -> login_check();


$objSingleUser = new user_registration();
$objSingleUser -> prepare($_GET);

$singleUser = $objSingleUser -> single_user();

if(!empty($singleUser) && isset($singleUser)){

include_once '../header.php';
include_once 'menubar.php';
?>
<div class="row">
	<div class="col-lg-4 col-sm-6">
		<div class="thumbnail">
			<div class="thumb thumb-rounded thumb-slide">
				<img alt="" src="
				<?php if(isset($singleUser['image']) && !empty($singleUser['image'])){
					echo '../assets/images/user/'.$singleUser['image'];
				}else{
					echo '../assets/images/user/nobody.jpg';
				}?>
				">
				<div class="caption">
					<span>
						<a data-popup="lightbox" class="btn bg-success-400 btn-icon btn-xs" href="#"><i class="icon-plus2"></i></a>
					</span>
				</div>
			</div>
			
			<div class="caption text-center">
				<h6 class="text-semibold no-margin"><?php echo $singleUser['full_name'];?> <small class="display-block"><?php echo ($singleUser['is_admin']==1)?'Admin':'User'; ?></small></h6>
			</div>
		</div>
	</div>
	<div class="col-lg-8 col-sm-6">
		<div class="panel border-left-lg border-left-danger invoice-grid timeline-content">
			
			<div class="pane-heading text-right">
				<div class="btn-group text-right">
					<a href="edit_user.php?id=<?php echo $singleUser['unique_id'];?>" class="btn btn-default" type="button"><i class="icon-pencil7 position-left"></i> Edit User</a>
					<?php 
                        if($_SESSION['logged']['is_admin'] == 1){
                    ?>
					<a href="delete_user.php?id=<?php echo $singleUser['unique_id'];?>" class="btn btn-default" type="button" onclick="return confirm('Are you sure you want to disable this user?');"><i class="icon-close2 position-left"></i> Block User</a>
					<?php
						}
					?>
				</div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<h5 class="text-semibold no-margin-top"><?php echo $singleUser['full_name'];?></h5>
						<ul class="list list-unstyled">
							<li>User name: &nbsp;<?php echo $singleUser['username'];?></li>
							<li>Password: <span class="text-semibold label bg-grey-400">
							<ul class="icons-list">
									<li class="dropdown">
										<a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">Show Password</a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li><p><?php echo $singleUser['password'];?></p></li>
										</ul>
									</li>
							</ul>
								</span>
							</li>
							<li>Email: <span class="text-semibold"><?php echo $singleUser['email'];?></span></li>
						</ul>
					</div>
					<div class="col-sm-6 col-md-6">
						<ul class="list list-unstyled text-right">
							<li>Status: <span class="text-semibold"><?php echo($singleUser['is_delete'] == 0)?'Active User':'Disabled User';?></span></li>
							<li class="dropdown">
								Position: &nbsp;
								<?php if($singleUser['is_admin'] == 1){ ?>
								<span class="label label-danger">Admin</span>
								<?php }else{ ?>
								<span class="label bg-indigo-400">User</span>
								<?php } ?>
							</li>
							
						</ul>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<ul class="list list-unstyled">
					<li><span class="status-mark border-danger position-left"></span> Created Date: <span class="text-semibold">
				<?php echo $singleUser['created'];?></span></li>
				<li><span class="status-mark border-danger position-left"></span> Last Modified : <span class="text-semibold">
			<?php echo ($singleUser['updated']=='0000-00-00 00:00:00')?'Not Yet Modified': $singleUser['updated'];?></span></li>
			<li><span class="status-mark border-danger position-left"></span> Deleted Date: <span class="text-semibold">
		<?php echo ($singleUser['deleted']=='0000-00-00 00:00:00')?'Not Yet Deleted': $singleUser['deleted'];?></span></li>
		
	</ul>
</div>
</div>
</div>
</div>
<?php include_once 'footer.php';


}else{

	$_SESSION["errorMsg"] = "Not found what you looking for.";
	header("location:error.php");
}





 ?>
