<?php
	require_once('session.php');
	require_once('include/db.php');
	
	$database = new db();
	
	$reset_code = '';
	
	if(isset($_GET['reset'])) {
		$reset_code = $_GET['reset'];
	}
	$where = "activation_code = '$reset_code'";
	
	
	if(isset($_POST['reset'])) {
		$password = $_POST['password'];
		$confirm_password = $_POST['confirm_password'];
		if($password == $confirm_password) {
			$setpassword = array(
				'password' => md5($password),
				'reset' => 0,
				'activation_code' => ''
			);
			if($database->update_data('users', $setpassword, $where)) {
				echo "password changed <a href='index.php'>Login</a>";
			}
			else {
				echo "some problem has occured";
			}
		}
		else {
			echo "something wrong";
		}
	}
		
	
	if($reset_code == '') {
		header('Location:error.php');
	}
	
	
	$no_visible_elements=true;
	include('header.php'); 
	$result = $database->select_data('users', '*', $where);
	if(count($result) == 1) {
		echo $reset_code;
?>

			<div class="row-fluid">
				<div class="span12 center login-header">
					<h2>Welcome to Charisma</h2>
				</div><!--/span-->
			</div><!--/row-->
			
			<div class="row-fluid">
				<div class="well span5 center login-box">
					<div class="alert alert-info">
						<?php
							if(isset($_SESSION['error'])) {
								$error = $_SESSION['error'];
								echo '<p class="error">'.$error.'</p>';
								session_destroy();
							}
						 ?>
						Please enter a password.
					</div>
					<form class="form-horizontal" action="" method="post">
						<fieldset>
							<div class="input-prepend" title="New Password" data-rel="tooltip">
								<span class="add-on"><i class="icon-lock"></i></span><input autofocus class="input-large span10" name="password" id="username" type="password" />
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Confirm Password" data-rel="tooltip">
								<span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="confirm_password" id="password" type="password" />
							</div>
							<div class="clearfix"></div>
							<p class="center span5">
							<button type="submit" name="reset" class="btn btn-primary">Reset Password</button>
							</p>
						</fieldset>
					</form>
				</div><!--/span-->
			</div><!--/row-->
<?php 
	include('footer.php'); 
	}
	else {
		echo "sorry, reset code is wrong";
	}
	
	
	
?>
