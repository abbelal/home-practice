<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        session_start();
        if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }

        ?>
        
        <fieldset>
            <legend>Add your Mobile Model with session</legend>
            <form action="store2.php" method="post">
                <label for="">mobile model</label>
                <input type="text" name="mModel" value="<?php 
                if(isset ($_SESSION['formData']['mModel'])){
                    echo $_SESSION['formData']['mModel'];
                    unset ($_SESSION['formData']['mModel']);
                }
                ?>" />
                <?php
                if (!empty($_SESSION['mblemty'])) {
                    echo $_SESSION['mblemty'];
                    unset($_SESSION['mblemty']);
                }
                ?><br/>

                <label for="">laptop model</label>
                <input type="text" name="lModel" value="<?php 
                if(isset ($_SESSION['formData']['lModel'])){
                    echo $_SESSION['formData']['lModel'];
                    unset ($_SESSION['formData']['lModel']);
                }
                ?>"/>
                       <?php
                       if (!empty($_SESSION['ltpemty'])) {
                           echo $_SESSION['ltpemty'];
                           unset($_SESSION['ltpemty']);
                       }
                       ?><br/>
                <button submit="">Add</button>
            </form>
        </fieldset>
        <a href="index.php">Back to home page</a>

    </body>
</html>