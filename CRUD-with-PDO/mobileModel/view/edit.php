<?php
include_once '../vendor/autoload.php';
use mobileApp\Mobilemodel;
$objectEdit = new Mobilemodel();
$objectEdit->dataPassToProperty($_GET);
$data = $objectEdit->singleDataShow();

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
if (isset($data) && !empty($data)) {
   ?>
    <fieldset>
        <legend>Update Mobile Model</legend>
        <form action="update.php" method="post">
            <a href="index.php">Back to list</a><br/>
            <label for="">update mobile model</label>
            <input type="text" name="mModel" value="<?php echo $data['models'] ?>" />
            <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" />
            <button type="submit">update</button>
        </form>
    </fieldset>
<?php
} else {
    $_SESSION['err_msg'] = "<h1>you are wrong...!!</h1>";
    header('location:error.php');
}
?>
