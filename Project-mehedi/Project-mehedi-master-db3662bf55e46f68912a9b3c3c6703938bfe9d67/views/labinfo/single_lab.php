<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\labinfo\labinfo;;
$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objAddLabs = new labinfo();

$objAddLabs -> prepare($_GET);
$singleLabInfo = $objAddLabs -> single_labs();



if(!empty($singleLabInfo['pc_configuration'])){
	$pc_config = unserialize($singleLabInfo['pc_configuration']);
}
else{
	$pc_config = array('Unknown','Unspecified','Unspecified','Unspecified',2 );
}
include_once '../header.php';
include_once 'menubar.php';
?>
<div class="row">
	<div class="col-md-12">
		
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h3 class="panel-title">Details about lab
				<?php echo $singleLabInfo['lab_no'];
					if($singleLabInfo['is_delete'] == 1){
						echo '<br><span class="label label-danger">Lab is offline</span>';
					}
				?>
				</h3>

				<div class="text-right">
				<div class="btn-group text-right">
					<a href="edit_lab.php?id=<?php echo $singleLabInfo['id'];?>" class="btn btn-default" type="button"><i class="icon-pencil7 position-left"></i> Edit Lab</a>
					<?php 
                        if($_SESSION['logged']['is_admin'] == 1){
                    ?>
					<a href="delete_lab.php?id=<?php echo $singleLabInfo['id'];?>" class="btn btn-default" type="button" onclick = "return confirm('Are you sure to disable this Lab?')"><i class="icon-trash position-left"></i> Disable Lab</a>
					<?php
						}
					?>
				</div>
			</div>


			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-bordered table-lg">
						<tbody>
							<tr class="alpha-grey">
								<td class="col-md-3 col-sm-3">Lab Number</td>
								<td>Lab <?php echo $singleLabInfo['lab_no']; ?></td>
							</tr>
							<tr class="alpha-slate">>
								<td>Assigned Course</td>
								<td><?php echo $singleLabInfo['title']; ?></td>
							</tr>
							<tr class="alpha-grey">
								<td>Seat Capacity</td>
								<td><?php echo $singleLabInfo['seat_capacity'];?> People maximum</td>
							</tr>
							<tr class="alpha-slate">
								<td>Table Capacity</td>
								<td><?php echo $singleLabInfo['table_capacity']; ?> table in total</td>
							</tr>
							<tr class="alpha-grey">
								<td class="col-md-3 col-sm-3">PC Configuration</td>
								<td>
									<?php
									if(isset($singleLabInfo['pc_configuration'])){
									$pc_config = unserialize($singleLabInfo['pc_configuration']);
												
												echo $pc_config[0]." PC, ".$pc_config[1]." Processor, ".$pc_config[2]." GB Ram, ".$pc_config[3]." GB HDD";
									}
									?>
								</td>
								
							</tr>
							<tr class="alpha-slate">
								<td>UPS Status</td>
								<td><?php echo ($pc_config[4]==1)?'UPS Powered':'Not UPS Powred'; ?></td>
							</tr>
							<tr>
								<td>Operating System</td>
								<td>
								<?php if($singleLabInfo['os'] == 'mac'){
								echo '<span class="label text-slate-800 label-rounded label-icon"><i class="icon-apple2"></i></span>  Mac OS X';
								}
								elseif($singleLabInfo['os'] == 'window'){
									echo '<span class="label text-primary label-rounded label-icon"><i class="icon-windows8"></i></span>  Windows 8+';
								}
								else{
									echo '<span class="label text-warning label-rounded label-icon"><i class="icon-tux"></i></span>  Linux based OS';
								}
								?>
									
								</td>
							</tr>
							<tr class="alpha-slate">
								<td>Trainer PC config</td>
								<td><?php echo $singleLabInfo['trainer_pc_configuration']; ?></td>
							</tr>
							<tr>
								<td>Internet Speed</td>
								<td><?php echo $singleLabInfo['internet_speed']; ?></td>
							</tr>
							<tr class="alpha-slate">
								<td>Projector Resulution</td>
								<td><?php echo $singleLabInfo['projector_resolution']; ?></td>
							</tr>
							<tr>
								<td>AC Status</td>
								<td><?php echo $singleLabInfo['ac_status']; ?> AC in this lab</td>
							</tr>
							<tr class="alpha-slate">
								<td>Lab functional Condition</td>
								<td><?php echo ($singleLabInfo['is_delete']=='0')?'LAB is Total Functional':'Lab is not functional right now'; ?></td>
							</tr>
							<tr>
								<td>Created</td>
								<td><?php echo $singleLabInfo['created'];?></td>
							</tr>
							<tr>
								<td>Updated</td>
								<td><?php
									if($singleLabInfo['updated'] == '0000-00-00 00:00:00') {
									echo "No Modify Yet";
									} else {
									echo $singleLabInfo['updated'];
									} ?>
							</tr>
							<tr>
								<td>Desabled</td>
								<td><?php
									if($singleLabInfo['deleted'] == '0000-00-00 00:00:00') {
									echo "No Modify Yet";
									} else {
									echo $singleLabInfo['deleted'];
									} ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div></div>
	<!-- /basic datatable -->
	<?php include_once 'footer.php' ?>