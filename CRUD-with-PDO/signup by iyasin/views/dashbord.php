<?php
include_once "../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();

if (isset($_SESSION['Login_data']) && !empty($_SESSION['Login_data'])){

//echo "<pre>";
//print_r($_SESSION['Login_data']);
//echo "</pre>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<!--contact-form-->
<?php include_once "include/log-navbar.php";?>
<div class="contact-form">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3>Welcome to Dashboard</h3>
                <h4 class="text-success"><?php $obj->Validation("Login"); ?></h4>

            </div>
        </div>
    </div>
</div>
</body>
</html>
<?php
} else {
    $_SESSION['Errors_R'] = "User not found :(";
    header("location:errors.php");
}
?>