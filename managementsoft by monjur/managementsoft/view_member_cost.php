<?php
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
		require_once('header.php');
		require_once('include/db.php');
		$database = new db();
?>


			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Cost</a>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>View Member Cost</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
							<select name="month" id="">
								<option value="">---Select Month---</option>
								<?php
									require_once('include/function.php');
									$search_with_month = getMonthArray();
									foreach($search_with_month as $key=>$value) {
										echo "<option value='{$key}'>{$value}</option>";
									}
								?>
							</select>
							<select name="year" id="">
								<option value="">---Select Year---</option>
								<?php
									for($i = 2014; $i > 2010; $i--) {
										echo "<option value='{$i}'>{$i}</option>";
									}
								?>
							</select>
							<input class="btn btn-primary" type="submit" name="search" value="Search" />
						</form>
						<table class="table table-striped table-bordered bootstrap-datatable datatable dataTable">
							<thead>
								<tr>
									<th>Name</th>
									<th>Bazaar Cost (Monthly)</th>
								</tr>
							</thead>
							<?php
								if(isset($_POST['search'])) {
									$cur_date = $_POST['year'].'-'.$_POST['month'];
									echo $cur_date;
								}
								else {
									$cur_date = date('Y-m');
								}
								$num_day = date('t');
								$database->sql = "SELECT sum( c.total_price ) AS total_amount, u.name AS member 
								FROM member_cost AS c INNER JOIN users AS u ON u.id = c.user_id WHERE c.creation_date LIKE '{$cur_date}-%' GROUP BY c.user_id";
								if($res = $database->query($database->sql)){
									while($data =  $res->fetch_assoc()){
										//print_r($data);
								?>
									<tr>
										<td><?php echo $data['member']; ?></td>
										<td><?php echo $data['total_amount']; ?></td>
									</tr>
								<?php
									}
								}
								else {
									echo $database->error;
								}
					
							?>
						</table>
					</div>
				</div><!--/span-->
			</div><!--/row-->
<?php 
	require_once('footer.php');
	}
?>
