<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\software\software;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objAddsoftware = new software();


$objAddsoftware -> prepare($_POST);

$objAddsoftware -> validate();


$objAddsoftware -> add_software();
