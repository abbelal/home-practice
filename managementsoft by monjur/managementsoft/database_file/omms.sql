-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 20, 2014 at 12:21 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `omms`
--

-- --------------------------------------------------------

--
-- Table structure for table `collection`
--

CREATE TABLE IF NOT EXISTS `collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `amount` int(10) NOT NULL,
  `collection_date` date NOT NULL,
  `collection_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `collection`
--

INSERT INTO `collection` (`id`, `user_id`, `amount`, `collection_date`, `collection_by`) VALUES
(2, 9, 500, '2014-11-06', 9),
(3, 15, 1000, '2014-11-14', 9),
(4, 9, 500, '2014-11-05', 9),
(5, 16, 450, '2014-11-02', 9);

-- --------------------------------------------------------

--
-- Table structure for table `food_list`
--

CREATE TABLE IF NOT EXISTS `food_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `food_list`
--

INSERT INTO `food_list` (`id`, `name`) VALUES
(15, 'Ginger'),
(14, 'Garlic'),
(13, 'Onion'),
(12, 'Pulses'),
(11, 'Lentils'),
(10, 'Rice'),
(9, 'potato'),
(16, 'Salt'),
(17, 'Chilli Powder'),
(18, 'Turmeric Powder'),
(19, 'Onion'),
(20, 'Rice');

-- --------------------------------------------------------

--
-- Table structure for table `manager`
--

CREATE TABLE IF NOT EXISTS `manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `month` varchar(50) NOT NULL,
  `year` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `manager`
--

INSERT INTO `manager` (`id`, `user_id`, `name`, `month`, `year`) VALUES
(4, 16, '', 'November', '2014'),
(5, 15, '', 'October', '2014'),
(6, 15, '', 'June', '2014');

-- --------------------------------------------------------

--
-- Table structure for table `manager_cost`
--

CREATE TABLE IF NOT EXISTS `manager_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` decimal(20,2) NOT NULL,
  `unit_price` decimal(20,2) NOT NULL,
  `total_price` decimal(20,2) NOT NULL,
  `creation_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `manager_cost`
--

INSERT INTO `manager_cost` (`id`, `user_id`, `product_id`, `quantity`, `unit_price`, `total_price`, `creation_date`) VALUES
(1, 16, 15, 5.00, 30.00, 150.00, '2014-11-04'),
(2, 16, 14, 3.00, 25.00, 75.00, '2014-11-06'),
(3, 16, 9, 5.00, 24.00, 115.00, '2014-11-08'),
(4, 16, 11, 3.00, 35.00, 105.00, '2014-11-08'),
(5, 16, 17, 1.00, 50.00, 50.00, '2014-11-08'),
(6, 16, 18, 1.00, 50.00, 50.00, '2014-11-08'),
(7, 16, 16, 2.00, 100.00, 200.00, '2014-11-10'),
(8, 16, 15, 2.50, 65.00, 162.50, '2014-11-10'),
(9, 16, 9, 5.00, 108.00, 540.00, '2014-11-10'),
(10, 16, 13, 5.00, 20.00, 100.00, '2014-10-02'),
(11, 16, 13, 5.00, 20.00, 100.00, '2014-10-06'),
(12, 16, 11, 5.00, 25.00, 125.00, '2014-09-07');

-- --------------------------------------------------------

--
-- Table structure for table `meal_entry`
--

CREATE TABLE IF NOT EXISTS `meal_entry` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `morning` float NOT NULL,
  `afternoon` int(11) NOT NULL,
  `night` float NOT NULL,
  `guest` float NOT NULL,
  `total_meal` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=234 ;

--
-- Dumping data for table `meal_entry`
--

INSERT INTO `meal_entry` (`id`, `user_id`, `date`, `morning`, `afternoon`, `night`, `guest`, `total_meal`) VALUES
(230, '14', '2014-11-01', 0, 0, 0.5, 0, 0.5),
(229, '9', '2014-11-01', 0.5, 1, 0, 0, 1.5),
(228, '14', '2014-10-09', 0.5, 1, 0, 0, 1.5),
(225, '9', '2014-11-02', 0.5, 1, 0.5, 0, 2),
(226, '14', '2014-11-02', 0, 0, 0.5, 0, 0.5),
(227, '9', '2014-10-09', 0.5, 0, 0, 0, 0.5),
(231, '9', '2014-11-03', 0.5, 1, 0, 1, 2.5),
(232, '15', '2014-11-03', 0, 0, 0.5, 1, 1.5),
(233, '16', '2014-11-03', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `member_cost`
--

CREATE TABLE IF NOT EXISTS `member_cost` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` varchar(255) NOT NULL,
  `quantity` decimal(20,2) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creation_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `member_cost`
--

INSERT INTO `member_cost` (`id`, `product_id`, `quantity`, `unit_price`, `total_price`, `user_id`, `creation_date`) VALUES
(28, '13', 5.00, 20, 100, 9, '2014-11-02'),
(29, '11', 5.00, 12, 60, 9, '2014-11-02'),
(30, '9', 2.00, 24, 48, 16, '2014-11-17'),
(31, '16', 5.00, 25, 125, 16, '2014-11-17'),
(32, '12', 5.00, 20, 100, 16, '2014-11-18'),
(33, '10', 25.00, 42, 1038, 9, '2014-10-07'),
(34, '12', 25.00, 12, 300, 9, '2014-11-03'),
(35, '10', 25.00, 12, 300, 9, '2014-11-02');

-- --------------------------------------------------------

--
-- Table structure for table `other_expence`
--

CREATE TABLE IF NOT EXISTS `other_expence` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) NOT NULL,
  `value` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `other_expence`
--

INSERT INTO `other_expence` (`id`, `item_name`, `value`, `date`) VALUES
(1, 'Apple', 0, '0000-00-00 00:00:00'),
(2, 'Orange', 220, '0000-00-00 00:00:00'),
(3, '', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(4) NOT NULL,
  `type` int(100) NOT NULL COMMENT '1 = admin, 2 = manager, 3 = member',
  `reset` tinyint(1) NOT NULL,
  `activation_code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `mobile`, `date`, `status`, `type`, `reset`, `activation_code`) VALUES
(9, 'Ali Akbar Reyad', 'aliakbar.reyad@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '01814812929', '2014-11-11', 1, 1, 0, ''),
(14, 'Mohammed Hasan', 'hasan_fpicmt@yahoo.com', 'd94fb2102e4ea17bb587dd28d038690a', '01815845589', '2014-11-01', 0, 3, 1, ''),
(15, 'MD Belal Hossen', 'belalfeni@gmail.com', 'a415bfc00e4719acc8be3564a2cbf9a6', '01829603962', '2014-11-01', 1, 2, 0, ''),
(16, 'Monjur Alam', 'alammonjur008@gmail.com', '6b456599d5772ace6d7f7d6e1eb06e0e', '01832058014', '2014-11-01', 1, 3, 1, '165467594be891a');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `home` text NOT NULL,
  `district` varchar(255) NOT NULL,
  `thana` varchar(255) NOT NULL,
  `post_office` text NOT NULL,
  `village` text NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `relation` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `father_name`, `home`, `district`, `thana`, `post_office`, `village`, `mobile`, `relation`) VALUES
(1, 1, 'md.ismail', 'noakhali', 'noakhali', 'sadar', 'Palgiri (Darogar Hat)', 'nalpur', '01840343751', 'brother'),
(2, 2, 'MD. Abul Hassem', 'Ashu Miya Khalifa Bari', 'Feni', 'Sonagazi', 'Palgiri (Darogar Hat)', 'Palgiri', '01826106196', 'mother'),
(3, 3, 'Nasir Ahmed', '', 'feni', 'sadar', '', '', '01812424520', 'brother'),
(4, 26, '', '', '', '', '', '', '', ''),
(5, 0, 'Yeasin', '', '', '', '', '', '01840343751', ''),
(6, 0, 'MD. Belal Hossen', '', '', '', '', '', '01826106196', ''),
(7, 0, 'Ali Akbar Reyad', '', '', '', '', '', '01812424520', ''),
(8, 0, 'Muhammad Hasan', '', '', '', '', '', '01815845589', ''),
(9, 0, 'MD Hanif', '', '', '', '', '', '012548695', ''),
(10, 0, 'monjur alam', '', '', '', '', '', '', ''),
(11, 0, 'Belal', '', '', '', '', '', '', ''),
(12, 0, 'Reyad', '', '', '', '', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
