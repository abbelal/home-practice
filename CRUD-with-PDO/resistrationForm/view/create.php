<?php
include 'include/header.php';
include_once '../vendor/autoload.php';

use signupForm\resistrationForm\resistrationForm;

$object = new resistrationForm();
?>

<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Sign Up</title>
    </head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <body>
        <h1 style="text-align: center"> Please Sign in</h1>

        <form style="width:30%; margin: auto;" action="signinProcess.php" method="POST">
            <div class="form-group">
                <label>User Name*</label>
                <input type="text" name="userName" class="form-control" placeholder="User Name" value="<?php $object->errHandling("Name_v"); ?>"/>
                <p class="text-danger" style="font-weight: bold; font-size: 10px;"><?php $object->errHandling('errName'); ?></p>
            </div>

            <div class="form-group">
                <label>Email address*</label>
                <input type="email" name="email" class="form-control" placeholder="Email" value="<?php $object->errHandling("Email_v"); ?>"/>
                <p class="text-danger" style="font-weight: bold; font-size: 10px;"><?php $object->errHandling('errEmail'); ?></p>
            </div>

            <div class="form-group">
                <label>Password*</label>
                <input type="password" name="password" class="form-control" placeholder="Password" value="<?php $object->errHandling("Password_v"); ?>"/>
                <p class="text-danger" style="font-weight: bold; font-size: 10px;"> <?php $object->errHandling('errPassword'); ?> </p>
            </div>

            <div class="form-group">
                <label>Confirm Password*</label>
                <input type="password" name="repPassword" class="form-control" placeholder="Password" value="<?php $object->errHandling("RePassword_v"); ?>">
                <p class="text-danger" style="font-weight: bold; font-size: 10px;"><?php $object->errHandling('errRepPass'); ?></p>
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
        </form>
        <?php
        include 'include/footer.php';
        ?>
    </body>
</html>
