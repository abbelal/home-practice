<?php
	class validator {
		
		public $errors = array();
		protected $data = array();
		protected $key;
		
		public function getData($source) {
			$this->data = $source;
			$this->checkData();
			return $this->errors;
		}
	
		protected function checkData() {
			foreach($this->data as $this->key=>$option) {
				switch($option['type']) {
					case 'text':
						$this->validateString($option['value'], $option['min'],$option['max']);
						break;
					case 'number':
						$this->validNumber($option['value'], $option['min'], $option['max']);
						break;
					case 'email':
						$this->validEmail($option['value']);
						break;
				}
			}
		}
		
		public function sanitize($input, $trim = false) {
			if($trim) {
				$input = trim($input);
			}
			$input = addslashes(strip_tags($input));
			return $input;
		}
		
		protected function validateString($value, $min = 0, $max = 0) {
			if(!is_string($value)) {
				array_push($this->errors, $this->key. "Invalid");
			}
			else if(strlen($value) < $min) {
				array_push($this->errors, $this->key." too short");
			}
			else if($max > 0 && strlen($value) > $max) {
				array_push($this->errors, $this->key."too long");
			}
		}
		
		protected function validNumber($value, $min = 0, $max = 0) {
			if((!(is_numeric($value)) || (strlen($value) < $min || strlen($value) > $max))) {
				array_push($this->errors, "Invalid Phone Number");
			}
		}
		
		protected function validEmail($value) {
			if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
				array_push($this->errors, "Please specify a valid email address");
			}
		}
		
	}
	
	/* call function to check validation
	$data = array(
		'mobile'=>array(
			'value' => 12345678912,
			'type' => 'number',
			'min' => 11,
			'max' => 11
		),
		'password'=>array(
			'value'=>'5kaliakbarr',
			'type'=>'text',
			'min' => 12,
			'max' => 15
		),
		'email'=>array(
			'value'=>'aliakbar.reyad@gmail.com',
			'type'=>'email'
		)
	);
	$valid = new validator();
	$output = $valid->getData($data);
	*/
	// sanitize function calling system
	//$input = sanitize('$inputdata' true);
?>