<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\trainers\trainers;
$objLoginUser = new user_login();
$objLoginUser -> login_check();


$objUpdateTrainers = new trainers();

$objUpdateTrainers -> prepare($_POST);

if(empty($_POST['image'])){
	$_POST['image'] = $_SESSION['img'];
}

$objUpdateTrainers -> prepare($_POST);

$objUpdateTrainers -> required_validation();

$objUpdateTrainers -> add_image();

$objUpdateTrainers -> update_trainer();