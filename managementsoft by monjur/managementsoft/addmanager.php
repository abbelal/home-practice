<?php
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
		require_once('header.php');
		require_once('include/db.php');
		require_once('include/validator.php');
		$database = new db();
		$success = '';
		if(isset($_POST['submit'])) {
			$user_id = $_POST['user_id'];
			/*
			$get_manager_name = $database->select_data('users','name', "id = $user_id");
			if(count($get_manager_name) == 1) {
				$manager_name = $get_manager_name[0]['name'];
			}
			*/
			$month = $_POST['month'];
			$year = $_POST['year'];
			$data = array(
				'user_id' => $user_id,
				//'name' => $manager_name,
				'month' => $month,
				'year' => $year
			);
			if($database->insert_data1('manager', $data)) {
				$success = "Manager Created";
			}
		}
?>


			<div>
				<ul class="breadcrumb">
					<li>
						<a href="members.php">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">New Manager</a>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>Make a New Manager</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
							<h2>
								<?php if(isset($success)) { echo $success; } ?>
							</h2>
						</div>
					</div>
					<div class="box-content">
						<?php
							echo "<h3 style='color: green'>{$success}</h3>";
						?>
						<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
							<select id="user" name="user_id">
									<option>Select a member</option>
									<?php
										$where = "status = 1";
										$user = $database->select_data('users', '*', $where);
										$rowcount = count($user);
										for($row = 0;$row < $rowcount; $row++) {
											$id = $user[$row]['id'];
											$name = $user[$row]['name'];
											echo '<option value="'.$id.'">'.$name.'</option>';
										}
									?>
							</select>
							<?php
								$montharr = array('January', 'February', 'March', 'April', 'May', 'June',
												'July', 'August', 'September', 'October',
												'November', 'December');
							?>
							<select id="month" name="month">
									<option>Select Month</option>
									<?php
										foreach($montharr as $month) {
											echo '<option value="'.$month.'">'.$month.'</option>';
										}
									?>
							</select>
							<span id="minus">-</span>
							<input type="text" name="year" id="year" readonly  />
							<span id="plus">+</span>
							<input class="btn btn-primary" type="submit" value="Make" name="submit" />
						</form>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

    
<?php
	}
	include('footer.php'); 
?>
