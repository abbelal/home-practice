<?php

namespace ProjectMehedi\courses;

use PDO;

require_once '../../src/DatabaseConnection/DatabaseConnection.php';
// require(dirname(__FILE__).'../../../../src/DatabaseConnection/DatabaseConnection.php'); 

class courses extends \DatabaseConnection{
	
    public $id = "";
    public $uniq_id = "";
    public $course_name = "";
    public $course_description = "";
    public $course_duration = "";
    public $course_type = "";
    public $course_fee = "";
    public $course_offer = "";
    public $data = "";
	public $created;
	public $modified;
	public $deleted;
	public $error;




	public function prepare($data = ""){

		   if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['title'])) {
            $this->course_name = $data['title'];
        }

        if (!empty($data['duration'])) {
            $this->course_duration = $data['duration'];
        }

        if (!empty($data['course_type'])) {
            $this->course_type = $data['course_type'];
        } 
        

        if (!empty($data['course_fee'])) {
            $this->course_fee = $data['course_fee'];
        }elseif ( $this->course_type == 'free') {

        	$this->course_fee = 0;
        }else {

            $this->course_fee = 0;
        }

        if (!empty($data['is_offer'])) {
            $this->course_offer = $data['is_offer'];
        }else{
        	$this->course_offer = '0';
        }

        if (!empty($data['description'])) {
            $this->course_description = $data['description'];
        }

		return $this;

		
	}// prepare \\



public function validation()
    {
        if (!empty($this->course_name)) {
            $_SESSION['title'] = $this->course_name;
        } else {
            $_SESSION['title_required'] = "Please enter course name";
            $this->errors = true;
        }

        if (!empty($this->course_duration)) {
            $_SESSION['duration'] = $this->course_duration;
        }else {
            $_SESSION['duration_required'] = "Please select course duration";
            $this->errors = true;
        }

        if (!empty($this->course_type)) {
            if($this->course_type == 'free'){
                if($this->course_fee!= '0'){
                    $_SESSION['course_type_change'] = "Can't add course fee in free course";
                    $this->errors = true;
                }
            }
            elseif($this->course_type == 'paid') {
                if (!empty($this->course_fee) || $this->course_fee != 0) {
                    $_SESSION['course_fee'] = $this->course_fee;
                } else  {
                    $_SESSION['course_fee_required'] = "Please enter course fee";
                    $this->errors = true;
                }
            }
            $_SESSION['course_type'] = $this->course_type;
        } else {
            $_SESSION['course_type_required'] = "Please select course type *";
            $this->errors = true;
        }
        if (!empty($this->course_description)) {
            $_SESSION['description'] = $this->course_description;
        }else {
            $_SESSION['description_required'] = "You can add description later";
            $this->errors = true;
        }
        if ($this->course_offer == 1) {
            $_SESSION['is_offer'] = $this->course_offer;
        }
    }// validation \\

   public function add_course()
    {
          if (!empty($this->errors) == false) {
            try {

                $uniqueId = uniqid();

                $query = "INSERT INTO courses (`id`, `unique_id`, `title`, `duration`, `description`, `course_type`, `course_fee`, `is_offer`, `created`) VALUES (:id, :unique_id, :title, :duration, :description, :course_type, :course_fee, :is_offer, :created)";

                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':id' => null,
                    ':unique_id' => $uniqueId,
                    ':title' => $this->course_name,
                    ':duration' => $this->course_duration,
                    ':description' => $this->course_description,
                    ':course_type' => $this->course_type,
                    ':course_fee' => $this->course_fee,
                    ':is_offer' => $this->course_offer,
                    ':created' => date("Y-m-d h:i:s"),

                ));

                $_SESSION['courseStoreSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Course name ' . $this->course_name . ' is successfully stored. Thank you.</div>';

                unset($_SESSION['title']);
                unset($_SESSION['course_type']);
                unset($_SESSION['is_offer']);
                unset($_SESSION['duration']);
                unset($_SESSION['course_fee']);
                unset($_SESSION['description']);
                
                header("location:add_course.php");
                
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        } else {
            header("location:add_course.php");
        }
    }// store \\


	public function single_courses(){
		
        try {
            $qr = "SELECT * FROM courses WHERE courses.unique_id=" . "'" . $this->id . "'";
            $query = $this->conn->prepare($qr);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            $_SESSION['show_data'] = $row;
            return $row;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

	} // single_courses \\


	    public function ShowAllCourse(){
        $query = "SELECT * FROM `courses` WHERE is_delete = 0";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->data[] = $row;
        }
        return $this->data;

    }// ShowAllCourse





    public function course_update()
    {
        if (!empty($this->errors) == false) {
            try {
                $query = "UPDATE courses SET title = :title, duration = :duration, course_type = :course_type, course_fee = :course_fee,
is_offer = :is_offer, description = :description,  updated= :updated WHERE courses.unique_id = :uid";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':title' => $this->course_name,
                    ':duration' => $this->course_duration,
                    ':course_type' => $this->course_type,
                    ':course_fee' => $this->course_fee,
                    ':is_offer' => $this->course_offer,
                    ':description' => $this->course_description,
                    ':updated' => date("Y-m-d h:i:s"),
                    ':uid' => $this->id,
                ));

                $_SESSION['courseUpdateSuccess'] = '<div class="alert alert-success alert-styled-left"><button
            data-dismiss="alert" class="close"
            type="button"><span>×</span><span class="sr-only">Close</span></button>Course name ' . $this->course_name . ' is
            successfully Updated. Thank you.</div>';

                header("location:edit_course.php?id=$this->id");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("location:edit_course.php?id=$this->id");
        }
    }


	public function disable_course(){

        try {

            $query = "UPDATE courses SET is_delete = :is_delete, deleted = :deleted WHERE courses.unique_id = :uid";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':is_delete' => 1,
                ':deleted' => date("Y-m-d h:i:s"),
                ':uid' => $this->id,
            ));

            $_SESSION['courseDisabledSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Course is successfully Deleted. Thank you.</div>';
            header("location:../courses/index.php");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// Trash \\


		public function restore_course(){

					try {

				$query = "UPDATE courses SET is_delete = :is_delete WHERE courses.unique_id = :id";

					$stmt = $this->conn -> prepare($query);
					$stmt -> execute(array(
						':is_delete' => '0',
						':id' => $this-> id,
						));

				$_SESSION['RestoreSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Course is Restored successfully. Thank you.</div>';

						header("location:../courses/index.php");

			} catch (Exception $e) {
				echo 'Error: ' . $e->getMessage();
				header("location:../courses/index.php");
			}
	
	}// disabled_user \\


	public function disable_course_list(){

		$query = "SELECT * FROM `courses` WHERE is_delete = 1";

		$stmt = $this->conn->prepare($query);
		$stmt -> execute();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$this->data[] = $row;
		}
		return $this->data;

	} // all_user \\


	public function delete_course_parmanent()
    {

        $unique_id = " '$this->id' ";

        $query = "DELETE FROM `courses` WHERE `courses`.`unique_id` = $unique_id";
        $stmt = $this->conn->prepare($query);
        if ($stmt->execute()) {
            $_SESSION['deleteCourseSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Course is parmanently Deleted successfully. Thank you.</div>';
            header('location:disabled_course.php');
        } // execute
    }// delete \\


}// Class \\
