<?php
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
		require_once('header.php');
		require_once('include/db.php');
		require_once('include/validator.php');
?>


			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Add Member</a>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>Add Member</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						 <?php
							 if(isset($_GET['error'])) {
								 echo "<p style='text-align:center; color:red; font-weight:bold;'>{$_GET['error']}</p>";
							}
						?>
						<form method="post" action="addnewmember.php" class="form-horizontal">
							<div class="control-group">
								<label class="control-label" for="name">Name</label>
								<div class="controls">
									<input name="name" class="input-xlarge focused" id="name" type="text" required />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="mobile">Mobile No</label>
								<div class="controls">
									<input name="mobile" class="input-xlarge focused" id="mobile" type="number" required />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="repeatmobile">Repeat Mobile No</label>
								<div class="controls">
									<input name="repeatmobile" class="input-xlarge focused" id="repeatmobile" type="number" required />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="email">Email</label>
								<div class="controls">
									<input name="email" class="input-xlarge focused" id="email" type="email" required />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="pass">Password</label>
								<div class="controls">
									<input name="password" class="input-xlarge focused" id="pass" type="password" required />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="repass">Repeat Password</label>
								<div class="controls">
									<input name="repassword" class="input-xlarge focused" id="repass" type="password" required />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="date01">Date input</label>
								<div class="controls">
									<input name="regdate" type="text" class="input-xlarge datepicker" id="date01" required />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Terms & Condition</label>
								<div class="controls">
									<label class="checkbox inline">
										<input name="terms" type="checkbox" id="inlineCheckbox1" checked="checked" value="agreed" disabled />
									</label>
								</div>
							</div>
							<div class="form-actions">
								<button type="submit" id="sbmt" name="register" class="btn btn-primary">Registration</button>
							</div>
						</form>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

    
<?php
	}
	include('footer.php'); 
?>
