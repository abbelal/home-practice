<?php
include_once '../vendor/autoload.php';
use packegApp\semistarPackeg\semistarPackeg;

$obj = new semistarPackeg();
$datalist = $obj->semisterList();


if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<a href="create.php">Add new data</a>
<table border="1">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Semister</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Weaber</th>
        <th>Total Cost</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($datalist) && !empty($datalist)) {
        foreach ($datalist as $singledata) {
            ?>
            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $singledata['name'] ?></td>
                <td><?php echo $singledata['semister'] ?></td>
                <td><?php echo $singledata['offer'] ?></td>
                <td><?php echo $singledata['cost'] ?></td>
                <td><?php echo $singledata['webar'] ?></td>
                <td><?php echo $singledata['total_cost'] ?></td>
                <td><a href="show.php?id=<?php echo $singledata['unique_id'] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $singledata['unique_id'] ?>">Update</a></td>
                <td><a href="delete.php?id=<?php echo $singledata['unique_id'] ?>">Delete</a></td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="8" align="center"> Durh mia....!!! Data nai </td>
        </tr>
        <?php
    }
    ?>

</table>
