<h1>
    <?php
    $arry = array(
        "google",
        "yahoo",
        "myarray" => array(
            'first name',
            'last name'
        ),
        "info" => array(
            'personal info' => array(
                'home',
                'office'
            )
        )
    );

    echo "<pre>";
    print_r($arry);

    foreach ($arry as $item) {
        if (is_array($item)) {
            print_r($item);
        } else {
            echo $item;
        }
    }
    ?>

</h1>