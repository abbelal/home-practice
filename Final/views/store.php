<?php
include_once ("../vendor/autoload.php");
use App\Project;


$obj = new Project();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//    echo "<pre>";
//    print_r($_POST);
//    print_r($_FILES);
//    die();
    
    
    $error = array();
$image_name = uniqid().time().$_FILES['image']['name'];
$image_type = $_FILES['image']['type'];
$image_tmp_locatoin = $_FILES['image']['tmp_name'];
$image_size = $_FILES['image']['size'];
$my_image_extension = strtolower(end(explode('.', $image_name)));
$required_format = array('jpg', 'jpeg', 'png', 'gif');

if(in_array($my_image_extension, $required_format)){
//    echo "oh ase re";
}else{
    $error[] = "Invalid file format";
}
if($image_size>2000000){
    $error[]= "Image size too large";
}
if(empty($error)){
    move_uploaded_file($image_tmp_locatoin, "images/".$image_name);
    $_POST['image'] = $image_name;
    $obj->prepare($_POST);
    $obj->store();
}else{
    $path="../image/default.png" ;
    move_uploaded_file($path, "images/".$image_name);
    $_POST['image'] = $image_name;
    $obj->prepare($_POST);
    $obj->store();
}
    
    
} else {
    $_SESSION['Err_Msg'] = "Opps ! You're not authoriezed to access this pages";
    header('location:errors.php');
}