<?php
namespace ProjectMehedi\assign;

use PDO;

require_once '../../src/DatabaseConnection/DatabaseConnection.php';


class Assign extends \DatabaseConnection{

    public $dbuser = 'root';
    public $dbpass = '';
    public $conn = "";
    public $id = "";
    public $data = "";
    public $course_id = '';
    public $batch_no = '';
    public $lead_trainer = '';
    public $asst_trainer = '';
    public $lab_asst = '';
    public $lab_id = '';
    public $start_date = '';
    public $ending_date = '';
    public $start_time = '';
    public $ending_time = '';
    public $day = '';
    public $is_running = '';
    public $assigned_by = '';
    public $created = '';
    public $updated = '';
    public $deleted = '';
    public $errors = '';
    public $course = '';
    public $trainer = '';
    public $lab = '';
    public $editData = '';
    public $Data_for_edit = '';
    public $trashlist = '';
    public $assign_date_validate = '';
    public $all_assign;
    public $searchData;
    



    public function prepare($data = "")
    {


        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['course_id'])) {
            $this->course_id = $data['course_id'];
        }

        if (!empty($data['lead_trainer'])) {
            $this->lead_trainer = $data['lead_trainer'];
        }

        if (!empty($data['asst_trainer'])) {
            $this->asst_trainer = $data['asst_trainer'];
        }

        if (!empty($data['lab_asst'])) {
            $this->lab_asst = $data['lab_asst'];
        }

        if (!empty($data['batch_no'])) {
            $this->batch_no = $data['batch_no'];
        }

        if (!empty($data['lab_id'])) {
            $this->lab_id = $data['lab_id'];
        }

        if (!empty($data['start_date'])) {
            $this->start_date = $data['start_date'];
        }

        if (!empty($data['ending_date'])) {
            $this->ending_date = $data['ending_date'];
        }

        if (!empty($data['start_time'])) {
            $this->start_time = $data['start_time'];
        }

        if (!empty($data['ending_time'])) {
            $this->ending_time = $data['ending_time'];
        }

        if (!empty($data['day'])) {
            $this->day = $data['day'];
        }


        return $this;

    } // prepare \\


     public function course_id_title(){

         $query = "SELECT `id`, `title` FROM `courses` WHERE `is_delete` = 0";
         $stmt = $this -> conn -> prepare($query);
         $stmt -> execute();

         while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $this->course[] = $row;
         }
         return $this->course;

    }// course_id_title


     public function trainer_name_status_team(){

         $query = "SELECT `id`, `courses_id`,`trainer_status`, `team`,`full_name` FROM `trainers` WHERE `is_delete` = 0";
         $stmt = $this -> conn -> prepare($query);
         $stmt -> execute();

         while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $this->trainer[] = $row;
         }
         return $this->trainer;

    }// trainer_name_status_team

    public function lab_id_labno(){

         $query = "SELECT `id`, `course_id`, `lab_no` FROM `labinfo` WHERE `is_delete` = 0";
         $stmt = $this -> conn -> prepare($query);
         $stmt -> execute();

         while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $this->lab[] = $row;
         }
         return $this->lab;

    }// lab_id_labno
    
    public function assign_date_day(){

        try {
            
            $query = "SELECT `id`, `start_date`, `ending_date`, `start_time`, `ending_time`, `day`, `lab_id`, `is_running` FROM `course_trainer_lab_mapping`";
            $stmt = $this -> conn -> prepare($query);
            $stmt -> execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
               $this->assign_date_validate[] = $row;
            }
         return $this->assign_date_validate;
            
        } catch (Exception $ex) {
            echo $ex->getCode(). ' : An error occured : ' . $ex->getMessage();
        }

    }// assign_date_day

    
    public function validate(){
       
      $labinfo = $this->lab_id_labno();
      $courseinfo = $this->course_id_title();
      $trainerinfo = $this->trainer_name_status_team();

       $valid =  array(
                'course_id' => $this->course_id,
                'lead_trainer' => $this->lead_trainer,
                'lab_asst' => $this->lab_asst,
                'asst_trainer' => $this->asst_trainer,
                'start_date' => $this->start_date,
                'ending_date' => $this->ending_date,
                'start_time' => $this->start_time,
                'batch_no' => $this->batch_no,
                'lab_id' => $this->lab_id,
                'day' => $this->day,
                'ending_time' => $this->ending_time,
                );

      foreach ($valid as $key => $value) {
          if(!empty($value)){

            foreach($trainerinfo as $trainer){
                if($this->lead_trainer == $trainer['id']){// Lead trainer validation
                    if($trainer['courses_id'] == $this->course_id){
                        $_SESSION["$key"] = $this->lead_trainer;
                    }else{
                    $_SESSION['wrongLeadTrainer'] = "Please select correct Lead Trainer";
                        $this->errors = true;
                    }
                }
                if($this->lab_asst == $trainer['id']){ // Lab assistant validation
                    if($trainer['courses_id'] == $this->course_id){
                        $_SESSION["$key"] = $this->lab_asst;
                    }else{
                    $_SESSION['wrongLabAssist'] = "Please select correct Lab Assistant";
                        $this->errors = true;
                    }
                }
                if($this->asst_trainer == $trainer['id']){ // Assistant trainer validation
                    if($trainer['courses_id'] == $this->course_id){
                        $_SESSION["$key"] = $this->asst_trainer;
                    }else{
                    $_SESSION['wrongAssistTrainer'] = "Please select correct Assistant Trainer";
                        $this->errors = true;
                    }
                }
            }
            
            $_SESSION["$key"] = $value;
          }
          else{
            $_SESSION["$key-required"] = "Sorry This field is required";
            $this->errors = true;
          }

      }
    }// validate \\
    
   

    public function assign_new_session()
    {
        if (!empty($this->errors) == false) {
                   try {
                           
                       $is_run = 1;
                       $user = $_SESSION['logged']['id'];
                       
//                        $date = date_create($this->start_time);
//                        echo date_format($date, 'H:i:s'); 
                        // figure out time and date as database friendly formate for further calculation

                       $start_time = date_format(date_create($this->start_time), 'H:i:s');
                       $end_time = date_format(date_create($this->ending_time), 'H:i:s');
                       $start_date = date_format(date_create($this->start_date), 'Y-m-d');
                       $end_date = date_format(date_create($this->ending_date), 'Y-m-d');
                        
        
                $query = "INSERT INTO course_trainer_lab_mapping (`id`, `course_id`, `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, `ending_date`, `start_time`, `ending_time`, `day`, `is_running`, `assigned_by`, `created`) VALUES (:id, :course_id, :batch_no, :lead_trainer, :asst_trainer, :lab_asst, :lab_id, :start_date, :ending_date, :start_time, :ending_time, :day, :is_running, :assigned_by, :created)";

                $stmt = $this->conn->prepare($query);
                if($stmt->execute(array(
                    ':id' => null,
                    ':course_id' => $this->course_id,
                    ':batch_no' => $this->batch_no,
                    ':lead_trainer' => $this->lead_trainer,
                    ':asst_trainer' => $this->asst_trainer,
                    ':lab_asst' => $this->lab_asst,
                    ':lab_id' => $this->lab_id,
                    ':start_date' => $start_date,
                    ':start_time' => $start_time,
                    ':ending_time' => $end_time,
                    ':ending_date' => $end_date,
                    ':day' => $this->day,
                    ':is_running' => $is_run,
                    ':assigned_by' => $user,
                    ':created' => date("Y-m-d h:i:s"),

                ))){                     
                        unset($_SESSION['course_id']);
                        unset($_SESSION['lead_trainer']);
                        unset($_SESSION['lab_asst']);
                        unset($_SESSION['asst_trainer']);
                        unset($_SESSION['start_date']);
                        unset($_SESSION['ending_date']);
                        unset($_SESSION['start_time']);
                        unset($_SESSION['batch_no']);
                        unset($_SESSION['lab_num']);
                        unset($_SESSION['day']);
                        unset($_SESSION['ending_time']);
                     header("location:index.php");
                }
                $_SESSION['assignStoreSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>New Session is successfully assigned. Thank you.</div>';
               
                

            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
            } else {
               header('location:index.php');
            }

    }// store \\


    public function all_assign_with_other_table()
    {
        try {

            $qr = "SELECT `course_trainer_lab_mapping`.*, `courses`.`title`,`labinfo`.`lab_no`,`users`.`full_name`  FROM `course_trainer_lab_mapping` LEFT JOIN `courses` ON `course_trainer_lab_mapping`.`course_id` = `courses`.`id` LEFT JOIN `labinfo` ON `course_trainer_lab_mapping`.`lab_id` = `labinfo`.`id` LEFT JOIN `users` ON `course_trainer_lab_mapping`.`assigned_by` = `users`.`id` WHERE `course_trainer_lab_mapping`.`is_running` = '1'";
            $stmt = $this->conn->prepare($qr);
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->all_assign[] = $row;
            }
            return $this->all_assign;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// all_assign_with_other_table \\



         public function single_assign_show(){
         try {
             $query = "SELECT `course_trainer_lab_mapping`.*, `courses`.`title`,`course_fee`,`description`,`labinfo`.`lab_no`,`users`.`full_name`  FROM `course_trainer_lab_mapping` LEFT JOIN `courses` ON `course_trainer_lab_mapping`.`course_id` = `courses`.`id` LEFT JOIN `labinfo` ON `course_trainer_lab_mapping`.`lab_id` = `labinfo`.`id` LEFT JOIN `users` ON `course_trainer_lab_mapping`.`assigned_by` = `users`.`id` WHERE `course_trainer_lab_mapping`.`id` =" . "'" . $this->id . "'";

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
             return $row;

         } catch (PDOException $e) {
             echo 'Error: ' . $e->getMessage();
         }
     }// single_assign_show \\
 
    public function update_assign_info(){
        if (!empty($this->errors) == false) {
            
            try{
            
            $start_time = date_format(date_create($this->start_time), 'H:i:s');
            $end_time = date_format(date_create($this->ending_time), 'H:i:s');
            $start_date = date_format(date_create($this->start_date), 'Y-m-d');
            $end_date = date_format(date_create($this->ending_date), 'Y-m-d');
            
            $query = "UPDATE course_trainer_lab_mapping SET course_id = :course_id, batch_no = :batch_no, lead_trainer = :lead_trainer, asst_trainer = :asst_trainer, lab_asst = :lab_asst, lab_id = :lab_id, start_date = :start_date, ending_date = :ending_date, start_time = :start_time, ending_time = :ending_time, day = :day, updated= :updated WHERE course_trainer_lab_mapping.id = :id";

                $stmt = $this->conn->prepare($query);
                
                if($stmt->execute(array(
                    ':course_id' => $this->course_id,
                    ':batch_no' => $this->batch_no,
                    ':lead_trainer' => $this->lead_trainer,
                    ':asst_trainer' => $this->asst_trainer,
                    ':lab_asst' => $this->lab_asst,
                    ':lab_id' => $this->lab_id,
                    ':start_date' => $start_date,
                    ':start_time' => $start_time,
                    ':ending_time' => $end_time,
                    ':ending_date' => $end_date,
                    ':day' => $this->day,
                    ':updated' => date("Y-m-d h:i:s"),
                    ':id' => $this->id,
                ))){
                     $_SESSION['assignUpdateSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Session is successfully Updated. Thank you.</div>';
                    
//                    session_unset($_SESSION['EditPost']);
                    header("location:edit_assign.php?id=$this->id");
                }
            
        } catch (Exception $ex) {
             header("location:edit_assign.php?id=$this->id");
        }
        }else{ // if this error is false
            header("location:edit_assign.php?id=$this->id");
        }
        
    }//update_assign_info



// function for searching \\
    public function search($getData = ""){
        if (!empty($getData['searchText'])) {

            $rawKeywords = trim($getData['searchText']);
            
            $pregFilteredKeywords = preg_replace('#[^A-Za-z 0-9?!]#i','',$rawKeywords);     
            $pregSplitedKeywords = preg_split('/[\s]+/', $pregFilteredKeywords);
            
            $totalKeywords = count($pregSplitedKeywords);
            if($getData['search_field'] == 'full'){
                $query = "SELECT `course_trainer_lab_mapping`.*, `courses`.`title`,`course_type`,`labinfo`.`lab_no`,`trainers`.`full_name`  FROM `course_trainer_lab_mapping` LEFT JOIN `courses` ON `course_trainer_lab_mapping`.`course_id` = `courses`.`id` LEFT JOIN `labinfo` ON `course_trainer_lab_mapping`.`lab_id` = `labinfo`.`id` LEFT JOIN `trainers` ON `course_trainer_lab_mapping`.`lead_trainer` = `trainers`.`id` WHERE title LIKE '%".$pregSplitedKeywords[0]."%'";

                for($i=0 ; $i < $totalKeywords; $i++){
                    $query .= " OR batch_no LIKE '%".$pregSplitedKeywords[$i]."%'";
                    $query .= " OR lab_no LIKE '%".$pregSplitedKeywords[$i]."%'";
                    $query .= " OR full_name LIKE '%".$pregSplitedKeywords[$i]."%'";
                    if($i > 0){
                      $query .= " OR title LIKE '%".$pregSplitedKeywords[$i]."%'";  
                    }
                }// for loop
            }// if searchfield == full

            if($getData['search_field'] == 'course'){
                $query = "SELECT * FROM `courses` WHERE title LIKE '%".$pregSplitedKeywords[0]."%'";

                for($i=0 ; $i < $totalKeywords; $i++){
                    $query .= " OR course_type LIKE '%".$pregSplitedKeywords[$i]."%'";
                    $query .= " OR course_fee LIKE '%".$pregSplitedKeywords[$i]."%'";
                    if($i > 0){
                      $query .= " OR title LIKE '%".$pregSplitedKeywords[$i]."%'";  
                    }
                    if($pregSplitedKeywords[$i] == 'offered' || $pregSplitedKeywords[$i] == 'offer'){
                        $query .= " OR  is_offer LIKE '%1%'";
                    }
                    
                }// for loop
            }// if searchfield == course

            if($getData['search_field'] == 'trainer'){
                $query = "SELECT `trainers`.*, `courses`.`title` FROM `trainers` LEFT JOIN `courses` ON `trainers`.`courses_id` = `courses`.`id` WHERE full_name LIKE '%".$pregSplitedKeywords[0]."%'";

                for($i=0 ; $i < $totalKeywords; $i++){
                    $query .= " OR team LIKE '%".$pregSplitedKeywords[$i]."%'";
                    $query .= " OR email LIKE '%".$pregSplitedKeywords[$i]."%'";
                    $query .= " OR phone LIKE '%".$pregSplitedKeywords[$i]."%'";
                    $query .= " OR address LIKE '%".$pregSplitedKeywords[$i]."%'";
                    $query .= " OR web LIKE '%".$pregSplitedKeywords[$i]."%'";
                    $query .= " OR title LIKE '%".$pregSplitedKeywords[$i]."%'";
                    
                    if($i > 0){
                      $query .= " OR full_name LIKE '%".$pregSplitedKeywords[$i]."%'";  
                    }
                    if($pregSplitedKeywords[$i] == 'disable trainer' || $pregSplitedKeywords[$i] == 'disabled' || $pregSplitedKeywords[$i] == 'disable' || $pregSplitedKeywords[$i] == 'deleted' || $pregSplitedKeywords[$i] == 'deleted trainer'){
                        $query .= " OR  trainers.is_delete LIKE '%1%'";
                    }
                }// for loop
            }// if searchfield == trainer

          $stmt=$this->conn->prepare($query);
          $stmt->execute ();
          if ($stmt->rowCount() > 0) {
                while ($result = $stmt -> fetch(PDO::FETCH_ASSOC)) {
                    $this->searchData[] = $result;
                }
                return $this->searchData;

            } else {
                $_SESSION['searchError'] = 'There was nothing to show for your "'.$rawKeywords.'" keyword. Please search with right keyword.';
            }

        }else{
            $_SESSION['searchError'] = 'You give a empty keyword for search in session, please provide right keyword what you want to search';
        }
    }

    // end of search function \\


    public function assign_disable()
    {
        try {
                        
            $query = "UPDATE `course_trainer_lab_mapping` SET is_running = :is_running, deleted = :deleted WHERE course_trainer_lab_mapping.id = :id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':is_running' => 0,
                ':deleted' => date("Y-m-d h:i:s"),
                ':id' => $this->id,
            ));

            $_SESSION['Success'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Course is successfully Disabled. Thank you.</div>';
            header('location:list.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// Trash \\



    public function restore_session()
    {
        try {

            $query = "UPDATE `course_trainer_lab_mapping` SET is_running = :is_running WHERE course_trainer_lab_mapping.id = :id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':is_running' => 1,
                ':id' => $this->id,
            ));

            $_SESSION['Success'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Session is Restored successfully. Thank you.</div>';
            header('location:list.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// restore \\


    
    
    public function disabled_session_list()
    {
        try {
            $qr = "SELECT `course_trainer_lab_mapping`.*, `courses`.`title`,`labinfo`.`lab_no`,`users`.`full_name`  FROM `course_trainer_lab_mapping` LEFT JOIN `courses` ON `course_trainer_lab_mapping`.`course_id` = `courses`.`id` LEFT JOIN `labinfo` ON `course_trainer_lab_mapping`.`lab_id` = `labinfo`.`id` LEFT JOIN `users` ON `course_trainer_lab_mapping`.`assigned_by` = `users`.`id` WHERE `course_trainer_lab_mapping`.`is_running` = '0'";
            $query = $this->conn->prepare($qr);
            $query->execute();
            
            while($row = $query->fetch(PDO::FETCH_ASSOC)){
                $this->trashlist[] = $row;
            }
            return $this->trashlist;
            
            
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// show \\



    public function delete_session()
    {

        $query = "DELETE FROM `course_trainer_lab_mapping` WHERE `course_trainer_lab_mapping`.`id` =" . "'" . $this->id . "'";

        $stmt = $this->conn->prepare($query);
        if ($stmt->execute()) {
            $_SESSION['Success'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Course is Deleted successfully. Thank you.</div>';
            header('location:trashlist.php');
        } // execute
    }// delete \\




    public function date_validate(){
       
      $assignTable = $this->assign_date_day();

      if($this->errors == false){
      
      foreach($assignTable as $singleAssign){

        if($this->lab_id == $singleAssign['lab_id']){

        if($singleAssign['day'] == $this -> day){

                $startFormatDateDB = date_format(date_create($singleAssign['start_date']),'Y-m-d');
                $startExplodeDateDB = explode('-', $startFormatDateDB);
                $startDateDB = "$startExplodeDateDB[0]$startExplodeDateDB[1]$startExplodeDateDB[2]";

                $endFormatDateDB = date_format(date_create($singleAssign['ending_date']),'Y-m-d');
                $endExplodeDateDB = explode('-', $endFormatDateDB);
                $endDateDB = "$endExplodeDateDB[0]$endExplodeDateDB[1]$endExplodeDateDB[2]";

                $startFormatDatePOST = date_format(date_create($this->start_date),'Y-m-d');
                $startExplodeDatePOST = explode('-', $startFormatDatePOST);
                $startDatePost = "$startExplodeDatePOST[0]$startExplodeDatePOST[1]$startExplodeDatePOST[2]";

                $endFormatDatePOST = date_format(date_create($this->ending_date),'Y-m-d');
                $startExplodeDatePOST = explode('-', $endFormatDatePOST);
                $endDatePost = "$startExplodeDatePOST[0]$startExplodeDatePOST[1]$startExplodeDatePOST[2]";


                // date validate start here.....  
                if($startDatePost != $endDatePost){

                    if($startDatePost < $endDatePost){

                        $date_validated = 0;
                        //       # - - - #
                        // *----*
                        if($startDatePost < $startDateDB && $startDatePost < $endDateDB && $endDatePost < $startDateDB && $endDatePost < $endDateDB){
                                // here date is validated.... code running
                        }
                        //       # - - - #
                        //    *-----*
                        if($startDatePost < $startDateDB && $startDatePost < $endDateDB && $endDatePost >= $startDateDB && $endDatePost <= $endDateDB){
                                $date_validated = 1;
                        
                        }
                        //       # - - - #
                        //             *------*
                        if($startDatePost >= $startDateDB && $startDatePost <= $endDateDB && $endDatePost > $startDateDB && $endDatePost > $endDateDB){
                                $date_validated = 1;                        
                        }
                        //       # - - - #
                        //                  *------*
                        if($startDatePost > $startDateDB && $startDatePost > $endDateDB && $endDatePost > $startDateDB && $endDatePost > $endDateDB){
                                // here date is validated.... code running
                        }
                        //       # - - - #
                        //         *---*
                        if($startDatePost >= $startDateDB && $startDatePost < $endDatePost && $endDatePost > $startDateDB && $endDatePost <= $endDateDB){
                                $date_validated = 1;                        
                        }
                        //       # - - - #
                        //     *--------------*
                        if($startDatePost < $startDateDB && $startDatePost < $endDatePost && $endDatePost > $startDateDB && $endDatePost > $endDateDB){
                                $date_validated = 1;                        
                        }

                    } // start date must < end date
                    else{
                        
                        $_SESSION['startDateGreaterThenEndDate'] = "please correct the date, you can't go to past";
                        $this->errors = true;
                    }
                }else{ // start date must not == to end date
                    $_SESSION['startDateEqualEndDate'] = "Start Date can't equal to End Date";
                    $this->errors = true;
                }

                if($date_validated == 1){
                    // time validation goes here
                    $time_validated = 0;

                $startFormatTimeDB = date_format(date_create($singleAssign['start_time']),'H:i:s');
                $startExplodeTimeDB = explode(':', $startFormatTimeDB);
                $startTimeDB = "$startExplodeTimeDB[0]$startExplodeTimeDB[1]";


                $endFormatTimeDB = date_format(date_create($singleAssign['ending_time']),'H:i:s');
                $endExplodeTimeDB = explode(':', $endFormatTimeDB);
                $endTimeDB = "$endExplodeTimeDB[0]$endExplodeTimeDB[1]";

                $startFormatTimePOST = date_format(date_create($this->start_time),'H:i:s');
                $startExplodeTimePOST = explode(':', $startFormatTimePOST);
                $startTimePost = "$startExplodeTimePOST[0]$startExplodeTimePOST[1]";

                $endFormatTimePOST = date_format(date_create($this->ending_time),'H:i:s');
                $endExplodeTimePOST = explode(':', $endFormatTimePOST);
                $endTimePost = "$endExplodeTimePOST[0]$endExplodeTimePOST[1]";

                if($startTimePost != $endTimePost){

                    if($startTimePost < $endTimePost){

                        $time_validated = 0;
                        
                        //       # - - - #
                        // *----*
                        if($startTimePost < $startTimeDB && $startTimePost < $endTimeDB && $endTimePost <= $startTimeDB && $endTimePost < $endTimeDB){
                                // here date is validated.... code running
                        }
                        //       # - - - #
                        //    *-----*
                        if($startTimePost < $startTimeDB && $startTimePost < $endTimeDB && $endTimePost > $startTimeDB && $endTimePost <= $endTimeDB){
                                $time_validated = 1;                        
                        }
                        //       # - - - #
                        //             *------*
                        if($startTimePost >= $startTimeDB && $startTimePost < $endTimeDB && $endTimePost > $startTimeDB && $endTimePost > $endTimeDB){
                                $time_validated = 1;                        
                        }
                        //       # - - - #
                        //                  *------*
                        if($startTimePost > $startTimeDB && $startTimePost >= $endTimeDB && $endTimePost > $startTimeDB && $endTimePost > $endTimeDB){
                                // here date is validated.... code running
                        }
                        //       # - - - #
                        //         *---*
                        if($startTimePost >= $startTimeDB && $startTimePost < $endTimePost && $endTimePost > $startTimeDB && $endTimePost <= $endTimeDB){
                                $time_validated = 1;                        
                        }
                        //       # - - - #
                        //     *--------------*
                        if($startTimePost < $startTimeDB && $startTimePost < $endTimePost && $endTimePost > $startTimeDB && $endTimePost > $endTimeDB){
                                $time_validated = 1;                        
                        }

                        if($time_validated == 1){

                            if($singleAssign['id'] != $this->id){

                                $_SESSION['dateErrStr'] = "Sorry! already A Session  started at " .date('Y-m-d',strtotime($startFormatDateDB));
                                $_SESSION['timeErrStr'] = "Those Session is running from " .date('h:i A',strtotime($startFormatTimeDB));
                                $_SESSION['dateErrEnd'] = "You can Try after date " .date('Y-m-d',strtotime($endFormatDateDB));
                                $_SESSION['timeErrEnd'] = "And booked till " .date('h:i A',strtotime($endFormatTimeDB));
                                    
                                    $this->errors = true;
                                    
                                if($singleAssign['is_running'] == 0){
                                    $_SESSION['disabled_session'] = "This Session is now disabled. You can add new Session after delete these session";
                                    }

                            }// if id == $this->id (for update purpose)

                        } // $time_validated = 1

                        // $average = round(($startTimePost + $endTimePost) / 2)+1;
                        // for ($p = $startTimeDB; $p <= $endTimeDB; $p++) {
                        //     if($average > $endTimePost){
                        //         $endTimePost = $average;
                        //     }else{
                        //         $startTimePost = $average;
                        //     }
                        //     for ($m = $startTimePost; $m <= $endTimePost; $m++) {

                        //         if ($m == $p) {
                        //             if($singleAssign['id'] != $this->id){
                        //                 $_SESSION['dateErrStr'] = "Sorry! already A Session  started at " .date('Y-m-d',strtotime($startFormatDateDB));
                        //                 $_SESSION['timeErrStr'] = "Those Session is ".$singleAssign['id']." running from " .date('h:i A',strtotime($startFormatTimeDB));
                        //                 $_SESSION['dateErrEnd'] = "You can Try after date " .date('Y-m-d',strtotime($endFormatDateDB));
                        //                 $_SESSION['timeErrEnd'] = "And booked till " .date('h:i A',strtotime($endFormatTimeDB));
                        //                 $time_validated = 1;
                        //                 $this->errors = true;
                        //                 break 2;
                        //             }else{
                        //                 continue;
                        //             }// if id == $this->id (for update purpose)

                        //         } //if m == p
                        //     }// for Time  comes from Post
                        // }// for Time  comes from Database
                    }// start time must < to end time.
                    else{
                        $_SESSION['startGreaterThenEnd'] = "Wrong time formate. You can't go to past.";
                        $this->errors = true;
                    }// else
                }else{ // start time must not == to end date
                    $_SESSION['startTimeEqualEndTime'] = "Start Time can't equal to End time";
                    $this->errors = true;
                }

                }// if date_validate is false

            }// if day DB == day POST
            }// if lab number == database lab number
        } // foreach
        //$execute_time_end = microtime(true);
        //$validTime = $execute_time_end - $execute_time_start;
        //echo "<br>execute_time_total = : ".$validTime.'<br>';
        }// errors from validate is false
      
    }// date_validate



















} // Course\\\

?>


