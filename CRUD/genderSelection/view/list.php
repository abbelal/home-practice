<?php
include_once '../src/GenderSelection.php';
$objectList = new GenderSelection();
$list = $objectList->dataList();
?>

<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Data List</title>
    </head>
    <body>
        <h3><a href="create.php">Back to Insert page</a></h3>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Gender</th>
                <th colspan="3">Action</th>
            </tr>

            <?php
            $serial = 1;
            foreach ($list as $singledata) {
                ?>
                <tr>
                    <td><?php echo $serial++ ?></td>
                    <td><?php echo $singledata['gender'] ?></td>
                    <td><a href="view.php?<?php echo $singledata['id'] ?>">View</a></td>
                    <td><a href="">Update</a></td>
                    <td><a href="">Delete</a></td>
                </tr>
                <?php
            }
            ?>

        </table>
    </body>
</html>
