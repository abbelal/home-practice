<?php

include_once '../vendor/autoload.php';

use mobileApp\Mobilemodel;

$deleteObject = new Mobilemodel();
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $deleteObject->dataPassToProperty($_GET);
    $deleteObject->dataDelete();
} else {
    $_SESSION['err_msg'] = "<h1>You have no right to acces this file</h1>";
    header('location:error.php');
}
?>
