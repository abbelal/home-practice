	// $("input:radio").click(function(){
	// 	var diva = "#"+$(this).val();
	// 	if(diva == "#1"){
	// 		document.getElementById("course_price").disabled = true;
	// 	}else{
	// 		document.getElementById("course_price").disabled = false;
	// 	}
	// });

	    $(function () {
        $("input[name='course_type']").click(function () {
            if ($("#free_course").is(":checked")) {
                document.getElementById("course_price").disabled = true;
            } else {
                document.getElementById("course_price").disabled = false;
            }
        });
    });


// time pickup
	$('#startTime').timepicker({
		minuteStep: 5,
		showInputs: false,
		disableFocus: true
	});

	$('#endTime').timepicker({
		minuteStep: 5,
		showInputs: false,
		disableFocus: true
	});

// Date pickup
	$('#startDate').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true
	});

	$('#endDate').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true
	});