<?php

namespace ProjectMehedi\software;

use PDO;

require_once '../../src/DatabaseConnection/DatabaseConnection.php';
// require(dirname(__FILE__).'../../../../src/DatabaseConnection/DatabaseConnection.php'); 

class software extends \DatabaseConnection{
	
    public $id = "";
    public $labinfo_id = "";
    public $software_title = "";
    public $version = "";
    public $software_type = "";
    public $data = "";
	public $created;
	public $modified;
	public $deleted;
	public $error;


    public function lab_id_labno(){

         $query = "SELECT `id`, `lab_no` FROM `labinfo` ";
         $stmt = $this -> conn -> prepare($query);
         $stmt -> execute();

         while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $this->data[] = $row;
         }
         return $this->data;

    }

	public function prepare($data = ""){

		if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['labinfo_id'])) {
            $this->labinfo_id = $data['labinfo_id'];
        }

        if (!empty($data['software_title'])) {
            $this->software_title = $data['software_title'];
        }

        if (!empty($data['version'])) {
            $this->version = $data['version'];
        } 
        

        if (!empty($data['software_type'])) {
            $this->software_type = $data['software_type'];
        }

		return $this;

		
	}// prepare \\

    public function validate(){

       $valid =  array('labinfo_id' => $this->labinfo_id,
                'software_title' => $this->software_title,
                'software_type' => $this->software_type,
                );

      foreach ($valid as $key => $value) {
          if(empty($value)){
            $_SESSION["$key-required"] = "$key is required";
            $this->errors = true;
          }else{
            $_SESSION["$key"] = $value;
          }
      }
    }


   public function add_software()
    {
          if (!empty($this->errors) == false) {
            try {

                $query = "INSERT INTO installed_softwares (`id`, `labinfo_id`, `software_title`, `version`, `software_type`,`is_delete`, `created`) VALUES (:id, :labinfo_id, :software_title, :version, :software_type,:is_delete, :created)";

                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':id' => null,
                    ':labinfo_id' => $this->labinfo_id,
                    ':software_title' => $this->software_title,
                    ':version' => $this->version,
                    ':software_type' => $this->software_type,
                    ':is_delete' => 0,
                    ':created' => date("Y-m-d h:i:s"),

                ));

                $_SESSION['softwareStoreSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Software ' . $this->software_title . ' is successfully added. Thank you.</div>';

                unset($_SESSION['labinfo_id']);
                unset($_SESSION['software_title']);
                unset($_SESSION['version']);
                unset($_SESSION['description']);
                unset($_SESSION['software_type']);
                
                header("location:add_software.php");
                
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        } else {
            header("location:add_software.php");
        }
    }// store \\


	public function single_softwares(){
		
        try {

            $qr = "SELECT `installed_softwares`.*, `labinfo`.`lab_no` FROM `installed_softwares` LEFT JOIN `labinfo` ON `installed_softwares`.`labinfo_id` = `labinfo`.`id` WHERE `installed_softwares`.`id` = '".$this->id."'";
            $query = $this->conn->prepare($qr);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            $_SESSION['show_data'] = $row;
            return $row;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

	} // single_softwares \\


	    public function ShowAllsoftware(){

        $query = "SELECT `installed_softwares`.*, `labinfo`.`lab_no` FROM `installed_softwares` LEFT JOIN `labinfo` ON `installed_softwares`.`labinfo_id` = `labinfo`.`id` WHERE `installed_softwares`.`is_delete` = 0";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->data[] = $row;
        }
        return $this->data;

    }// ShowAllsoftware





    public function software_update()
    {
        if (!empty($this->errors) == false) {
            try {
                 $query = "UPDATE installed_softwares SET id = :id, labinfo_id = :labinfo_id, software_title = :software_title, version = :version, software_type = :software_type, is_delete = :is_delete, updated = :updated WHERE installed_softwares.id = :id";

                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':id' => $this->id,
                    ':labinfo_id' => $this->labinfo_id,
                    ':software_title' => $this->software_title,
                    ':version' => $this->version,
                    ':software_type' => $this->software_type,
                    ':is_delete' => 0,
                    ':updated' => date("Y-m-d h:i:s"),

                ));

                $_SESSION['softwareStoreSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Software ' . $this->software_title . ' is successfully Updated. Thank you.</div>';

                unset($_SESSION['labinfo_id']);
                unset($_SESSION['software_title']);
                unset($_SESSION['version']);
                unset($_SESSION['description']);
                unset($_SESSION['software_type']);
                
                header("location:edit_software.php?id=$this->id");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("location:edit_software.php?id=$this->id");
        }
    }


	public function disable_software(){

        try {

            $query = "UPDATE installed_softwares SET is_delete = :is_delete, deleted = :deleted WHERE installed_softwares.id = :uid";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':is_delete' => 1,
                ':deleted' => date("Y-m-d h:i:s"),
                ':uid' => $this->id,
            ));

            $_SESSION['softwareDisabledSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected software is successfully Uninstalled. Thank you.</div>';
            header("location:../software/index.php");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
            header("location:../software/index.php");
        }
    }// Trash \\


		public function restore_software(){

					try {

				$query = "UPDATE installed_softwares SET is_delete = :is_delete WHERE installed_softwares.id = :id";

					$stmt = $this->conn -> prepare($query);
					$stmt -> execute(array(
						':is_delete' => '0',
						':id' => $this-> id,
						));

				$_SESSION['ReinstalledSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected software is Reinstalled successfully. Thank you.</div>';

						header("location:../software/index.php");

			} catch (Exception $e) {
				echo 'Error: ' . $e->getMessage();
				header("location:../software/index.php");
			}
	
	}// disabled_user \\


	public function disable_software_list(){

		$query = "SELECT `installed_softwares`.*, `labinfo`.`lab_no` FROM `installed_softwares` LEFT JOIN `labinfo` ON `installed_softwares`.`labinfo_id` = `labinfo`.`id` WHERE `installed_softwares`.`is_delete` = 1";

		$stmt = $this->conn->prepare($query);
		$stmt -> execute();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$this->data[] = $row;
		}
		return $this->data;

	} // all_user \\


	public function delete_software_parmanent()
    {


        $query = "DELETE FROM `installed_softwares` WHERE `installed_softwares`.`id` = $this->id";
        $stmt = $this->conn->prepare($query);
        if ($stmt->execute()) {
            $_SESSION['deletesoftwareSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected software is parmanently Deleted successfully. Thank you.</div>';
            header('location:uninstall_softwares.php');
        } // execute
    }// delete \\


}// Class \\
