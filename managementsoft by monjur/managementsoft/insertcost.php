<?php
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: ../index.php');
	}
	else {
		require_once('header.php');
		require_once('include/db.php');
		$database = new db();
		
		if(isset($_POST['savecost'])) {
			$count = count($_POST['product']);
			$date = $_POST['date'];
			$date = explode('/',$date);
			$date = "{$date[2]}-{$date[0]}-{$date[1]}";
			$member = $_POST['member'];
			$month = date('F');
			$year = date('Y');
			$where = "month = '$month' and year = '$year'";
			$manager = $database->select_data('manager', 'user_id', $where);
			
			if($member == 2) {
				$memberid = $manager[0]['user_id'];
			}
			if($member == 3) {
				$memberid = $_SESSION['id'];
			}
			for($row = 0; $row < $count; $row++) {
				$productid = $_POST['productid'][$row];
				$quantity = $_POST['quantity'][$row];
				$unit_price = $_POST['unit_price'][$row];
				$total_price = $_POST['price'][$row];
				
				$data = array(
					'product_id' => $productid,
					'quantity' => $quantity,
					'unit_price' => $unit_price,
					'total_price' => $total_price,
					'user_id' => $memberid,
					'creation_date' => $date
				);
				$table = '';
				if($member == 2) {
					$table = 'manager_cost';
				}
				if($member == 3) {
					$table = 'member_cost';
				}
				
				if($database->insert_data1($table, $data)) {
					$report = "data successfully inserted";
				}
				else {
					$report = "internal problem has been occured";
				}
			}
			echo $report;
		}
?>


			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Cost</a>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>Add Cost</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form action="" method="post">
							<div class="hidedatemember">
								<input type="text" class="input-xlarge datepicker" id="date" />
								<input type="radio" id="member1" name="member" value="2">Dining Manager
								<input type="radio" id="member2" name="member" value="3">Me
							</div>
							<input type="hidden" name="form_id" value="<?php echo uniqid('frm_dining_',true); ?>" />
							<table class="table">
								<thead>
								<tr>
									<th>Select Product</th>
									<th >Quantity</th>
									<th>Unit Price</th>
									<th>Discount</th>
									<th>Total Price</th>
								</tr>
								</thead>
								<tr>
									<td>
										<select name="product_item" id="product">
										<?php
											$viewitem = $database->select_data('food_list');
											if($viewitem) {
												foreach($viewitem as $product) {
													echo "<option value='{$product['id']}'>{$product['name']}</option>";
												}
											}
											else {
												echo "something wrong";
											}
										?>
										</select>
									</td>
									<td><input type="text" name="amount" id="amount"  style="width:100px;" required></td>
									<td><input onkeyup="totalprice()" type="text" name="unit_price" id="price"  required /></td>
									<td><input onkeyup="totalprice()" type="text" name="discount" id="discount" style="width:100px;" required /></td>
									<td><input type="number" name="totalcost" id="totalcost" readonly /></td>
								</tr>
								<tr>
									<td><input class="btn btn-default create_list" type="submit" value="Add List" name="add"></td>
								</tr>
							</table>
						</form>
						<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
							<div class="datewithuser"></div>
							<table class="table add_field_row">
								<tr>
									<th>Product Name</th>
									<th>Quantity</th>
									<th>Unit Price</th>
									<th>Total Price</th>
								</tr>
							</table>
							<p class="add_submit_button"></p>
						</form>
					</div>
				</div><!--/span-->
			</div><!--/row-->
<?php 
	require_once('footer.php');
	}
?>
