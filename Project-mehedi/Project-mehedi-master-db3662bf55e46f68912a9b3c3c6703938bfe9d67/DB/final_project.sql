-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 30, 2016 at 10:40 অপরাহ্ণ
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `final_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `title` varchar(111) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `course_type` varchar(50) NOT NULL,
  `course_fee` varchar(111) NOT NULL,
  `is_offer` int(10) NOT NULL,
  `is_delete` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `unique_id`, `title`, `duration`, `description`, `course_type`, `course_fee`, `is_offer`, `is_delete`, `created`, `updated`, `deleted`) VALUES
(23, '57ad650a0fb33', 'Digital Marketing', '3_months', 'After completing this training program, the trainee will have a proper knowledge on Digital Marketing and will able to work any organization as Digital Marketing Professional. Or, he can choose his profession as a freelancer and can work in outsourcing marketplace.\r\n', '2', '0', 1, 1, '2016-08-12 07:56:26', '2016-08-15 09:24:25', '2016-08-16 09:14:54'),
(24, '57ad652c3e71f', 'Web Application Development- Dot Net', '3_months', 'After completing this training program, the trainee will be able to work as a professional web application developer in a local market as well as offshore market place.', '1', '15000', 0, 0, '2016-08-12 07:57:00', '2016-08-20 04:01:51', '2016-08-20 04:02:12'),
(25, '57ad6555d741f', 'Web Application Development- PHP', '3_months', 'After completing this training program, the trainee will be able to work as a professional web application developer in a local market as well as offshore market place.', '1', '2000', 1, 1, '2016-08-12 07:57:41', '2016-08-15 09:20:27', '2016-08-15 09:56:58'),
(26, '57ad657098b53', 'Practical SEO', '2_months', 'After completing this training program, the trainee will have a proper knowledge on Digital Marketing and will able to work any organization as Internet Marketing Professional. Or, he can choose his profession as a freelancer and can work in outsourcing marketplace.', '2', '0', 1, 1, '2016-08-12 07:58:08', '2016-08-15 09:34:56', '2016-08-15 09:58:29'),
(27, '57ad6599a62a7', 'Affiliate Marketing  & E-commerce', '2_months', 'After completing this training program, the trainee will have a proper knowledge on Digital Marketing and will able to work any organization as Internet Marketing Professional. Or, he can choose his profession as a freelancer and can work in outsourcing marketplace.', '2', '0', 0, 0, '2016-08-12 07:58:49', '2016-08-15 10:18:22', '2016-08-15 07:58:06'),
(28, '57ad65ba7ecd2', ' Mobile Application Development-Android', '3_months', 'Opportunities are same in the local market as well as offshore market to work as a mobile application developer.', '1', '20000', 1, 0, '2016-08-12 07:59:22', '0000-00-00 00:00:00', '2016-08-13 10:27:07'),
(29, '57ad65f34df45', 'Server Management and Cloud Management', '2_months', 'After completing this training program, the trainee will have a proper knowledge on Digital Marketing and will able to work any organization as Digital Marketing Professional. Or, he can choose his profession as a freelancer and can work in outsourcing marketplace.\r\n', '1', '20000', 1, 0, '2016-08-12 08:00:19', '0000-00-00 00:00:00', '2016-08-15 09:47:11'),
(30, '57ad661fec9de', 'IT support Technical', '2_months', 'After completing this training program, the trainee will have a proper knowledge on IT support Technical and will able to work any organization.', '2', '0', 0, 0, '2016-08-12 08:01:03', '0000-00-00 00:00:00', '2016-08-15 09:54:59'),
(31, '57ad6739e1ba0', 'Customer Support & Service', '1_month', 'After completing this program, the trainee will be able to work in any local company (Call Centre, Telecommunication & IT company) as customer support executive.', '1', '5000', 1, 0, '2016-08-12 08:05:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '57af674fe74ce', 'Software Design/Architecture', '3_months', 'After completion, participants will understand OOP design principles, software design & architecture. They will learn to recognize code smells and refactoring for improving code quality. They will obtain an excellent understanding of several design principles and design patterns.', '1', '12000', 1, 0, '2016-08-13 08:30:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, '57af75193a92d', 'JavaScript Fundamental ', '1_month', 'After completion of this course, students will gain:\r\nSolid foundation on programming techniques using JavaScriptUnderstanding on graceful degradation, browser compatibility, unobtrusive JavaScript   that give insight of modern librariesKnowledge on jQuery-less world of JavaScript which ultimately help to learn jQuery (and any other library) better', '1', '5000', 1, 0, '2016-08-13 09:29:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, '57af75dece424', 'Android Application Development', '3_months', 'This course is designed for those who are trying to build their career in the android apps development arena. Participants will be trained on real life time projects, technology tools and methodologies that are very much relevant to Android Application Development. By the end of this course you will have a comprehensive knowledge of developing professional android apps using java language. You will be able to use Eclipse IDE with SDK', '2', '0', 0, 0, '2016-08-13 09:32:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '57af777d87965', 'Managing Software Projects', '2_months', 'Mid-level training; 3 or 4 yearsâ€™ experience as Software Engineer. One will practically learn all mentioned tools and techniques by completing a project through this practical training course.', '2', '0', 1, 0, '2016-08-13 09:39:41', '0000-00-00 00:00:00', '2016-08-13 11:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `course_trainer_lab_mapping`
--

CREATE TABLE `course_trainer_lab_mapping` (
  `id` int(11) NOT NULL,
  `course_id` int(111) NOT NULL,
  `batch_no` varchar(111) NOT NULL,
  `lead_trainer` varchar(111) NOT NULL,
  `asst_trainer` varchar(111) NOT NULL,
  `lab_asst` varchar(111) NOT NULL,
  `lab_id` int(111) NOT NULL,
  `start_date` varchar(111) NOT NULL,
  `ending_date` varchar(111) NOT NULL,
  `start_time` varchar(111) NOT NULL,
  `ending_time` varchar(111) NOT NULL,
  `day` varchar(111) NOT NULL,
  `is_running` int(11) NOT NULL,
  `assigned_by` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_trainer_lab_mapping`
--

INSERT INTO `course_trainer_lab_mapping` (`id`, `course_id`, `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, `ending_date`, `start_time`, `ending_time`, `day`, `is_running`, `assigned_by`, `created`, `updated`, `deleted`) VALUES
(7, 25, '0070', 'Mian Zadid', 'Bikash', 'Monir', 303, '2016-08-11', '2016-08-06', '03:03:00', '16:04:00', 'day2', 1, 'BITM Admin', '2016-08-13 06:41:18', '2016-08-16 06:31:45', '2016-08-20 04:09:56'),
(8, 25, '987', 'Mian Zadid', 'Mishu', 'Monir', 403, '2016-08-19', '2016-08-13', '03:03:00', '18:54:15', 'day2', 0, 'BITM Admin', '2016-08-13 06:54:15', '2016-08-15 07:41:48', '2016-08-16 06:44:19'),
(9, 25, '987', 'Mian Zadid', 'Sujon', 'Monir', 403, '2016-09-14', '2016-09-29', '03:03:00', '02:02:00', 'day2', 1, 'BITM Admin', '2016-08-13 06:59:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 24, '005', 'Zohirul Alam Tiemoon', 'Rafid', 'Babu', 303, '2016-09-14', '2016-12-29', '09:03:00', '02:02:00', 'day1', 1, 'BITM Admin', '2016-08-13 07:26:05', '2016-08-16 06:37:16', '0000-00-00 00:00:00'),
(11, 25, '321', 'Mian Zadid', 'Bikash', 'Monir', 403, '2016-08-25', '2016-08-25', '11:22:00', '11:11:00', 'day1', 1, 'BITM Admin', '2016-08-14 06:19:10', '2016-08-16 09:32:17', '0000-00-00 00:00:00'),
(12, 25, '222', 'Mian Zadid', 'Habib', 'Monir', 401, '2016-08-05', '2016-08-17', '11:11:00', '11:11:00', 'day2', 1, 'BITM Admin', '2016-08-14 07:34:46', '0000-00-00 00:00:00', '2016-08-16 06:37:34');

-- --------------------------------------------------------

--
-- Table structure for table `installed_softwares`
--

CREATE TABLE `installed_softwares` (
  `id` int(11) NOT NULL,
  `labinfo_id` int(111) NOT NULL,
  `software_title` varchar(111) NOT NULL,
  `version` varchar(111) NOT NULL,
  `software_type` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `installed_softwares`
--

INSERT INTO `installed_softwares` (`id`, `labinfo_id`, `software_title`, `version`, `software_type`, `created`, `updated`, `deleted`) VALUES
(1, 402, 'Xampp', '1.8.04', 'Apache Mysql', '2016-08-30 01:52:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 403, 'ddd', '333', 'sxx', '2016-08-30 01:53:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 405, 'Xampp', '1.8.04', 'Apache Mysql', '2016-08-30 01:57:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `labinfo`
--

CREATE TABLE `labinfo` (
  `id` int(11) NOT NULL,
  `course_id` int(100) NOT NULL,
  `lab_no` varchar(111) NOT NULL,
  `seat_capacity` varchar(111) NOT NULL,
  `projector_resolution` varchar(111) NOT NULL,
  `ac_status` varchar(111) NOT NULL,
  `pc_configuration` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `trainer_pc_configuration` varchar(255) NOT NULL,
  `table_capacity` varchar(100) NOT NULL,
  `internet_speed` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `labinfo`
--

INSERT INTO `labinfo` (`id`, `course_id`, `lab_no`, `seat_capacity`, `projector_resolution`, `ac_status`, `pc_configuration`, `os`, `trainer_pc_configuration`, `table_capacity`, `internet_speed`, `created`, `updated`, `deleted`) VALUES
(1, 25, '301', '30', '', '', 'xampp', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 24, '302', '30', '', '', 'Visual_studio', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 22, '303', '30', '', '', 'photoshop', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 24, '401', '30', '', '', 'Visual_studio', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 22, '402', '30', '', '', 'photoshop', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 25, '403', '30', '', '', 'xampp', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 22, '501', '25', '', '', 'photoshop', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 24, '502', '25', '', '', 'Visual_studio', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 26, '503', '25', '', '', 'Google Analytics', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE `trainers` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `edu_status` varchar(255) NOT NULL,
  `team` varchar(111) NOT NULL,
  `courses_id` int(11) NOT NULL,
  `trainer_status` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(111) NOT NULL,
  `address` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `web` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainers`
--

INSERT INTO `trainers` (`id`, `unique_id`, `full_name`, `edu_status`, `team`, `courses_id`, `trainer_status`, `image`, `phone`, `email`, `address`, `gender`, `web`, `created`, `updated`, `deleted`) VALUES
(41, '57c5974cc2fe7', 'Mian Zadid (PHP)', 'a:3:{i:0;s:10:"B.Sc in IT";i:1;s:23:"North South Universtily";i:2;s:3:"212";}', 'PHP', 32, 'lead_trainer', '57c5b79fcf871Generic_Male_Profile.jpg', '12121212', 'mehedi.thedue@gmail.com', 'a:3:{i:0;s:9:"mirpur 10";i:1;s:5:"Dhaka";i:2;s:4:"1212";}', 'male', 'http://www.zia.com', '2016-08-30 04:25:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '57c5dc4223097', 'Sajedur Rahman (PHP)', 'a:3:{i:0;s:11:"B.Sc in CSE";i:1;s:21:"Prime Asia University";i:2;s:4:"2011";}', 'PHP', 25, 'assist_trainer', '57c5dc42187f485_002.jpg', '01711392507', 'sajedur@rahman.com', 'a:3:{i:0;s:9:"Gulshan 2";i:1;s:5:"Dhaka";i:2;s:4:"1217";}', 'male', 'http://sajed.me', '2016-08-30 09:19:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, '57c5dde532b05', 'Jalal Uddin (Dotnet)', 'a:3:{i:0;s:11:"B.Sc in CSE";i:1;s:23:"Asia Pacific University";i:2;s:4:"2012";}', 'DotNet', 24, 'assist_trainer', '57c5dde532a4073.jpg', '0198698534', 'jalal@uddin.com', 'a:3:{i:0;s:11:"Khamar Bari";i:1;s:5:"Dhaka";i:2;s:4:"1218";}', 'male', '', '2016-08-30 09:26:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, '57c5e8b146677', 'Bikash Ray (SEO)', 'a:3:{i:0;s:10:"B.Sc in IT";i:1;s:21:"Prime Asia University";i:2;s:4:"2011";}', 'SEO', 26, 'assist_trainer', '57c5e8b1386a846_002.jpg', '015523569', 'bikash@ray.com', 'a:3:{i:0;s:10:"New Market";i:1;s:5:"Dhaka";i:2;s:4:"1217";}', 'male', '', '2016-08-30 10:12:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, '57c5e9c7f1c78', 'Maya Chokroborty', 'a:3:{i:0;s:11:"B.Sc in CSE";i:1;s:45:"American University of Science and Engneering";i:2;s:4:"2013";}', 'Android', 28, 'assist_trainer', '57c5e9c7f1b2c60.jpg', '01555658537', 'maya@chak.com', 'a:3:{i:0;s:12:"Elefant Road";i:1;s:5:"Dhaka";i:2;s:4:"1214";}', 'female', 'http://mayachak.me', '2016-08-30 10:17:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, '57c5eaa24e3ce', 'Asad Al Farabi (Networking)', 'a:3:{i:0;s:11:"B.Sc in CSE";i:1;s:21:"Prime Asia University";i:2;s:4:"2012";}', 'Support', 30, 'assist_trainer', '57c5eaa24e30637_002.jpg', '01736985632', 'farabi@asad.com', 'a:3:{i:0;s:12:"Mohammdadpur";i:1;s:5:"Dhaka";i:2;s:4:"1212";}', 'male', 'http://farabi.me', '2016-08-30 10:20:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(10) NOT NULL,
  `is_admin` int(10) NOT NULL,
  `is_delete` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `full_name`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `is_delete`, `created`, `updated`, `deleted`) VALUES
(2, '57beb04044231', 'Mehedis Hasan', 'mehedi344', 'sisee@mais.com', 'asdf1234', '57c40a98b33e9Generic_Male_Profile.jpg', 1, 2, 0, '2016-08-25 10:45:52', '2016-08-29 12:12:40', '0000-00-00 00:00:00'),
(3, '57beb11514f8e', 'Hasan Khal', 'Hasaee', 'asdf@fdsa.com', 'asdf1234', '57c343e84cf67phsoto.jpg', 1, 1, 0, '2016-08-25 10:49:25', '2016-08-28 10:04:56', '2016-08-28 09:59:28'),
(4, '57beb1e88305b', 'Asdf Asdf', 'Another', 'mehedi@hasan.com', 'asdf', '395071_300x300.jpeg', 1, 1, 0, '2016-08-25 10:52:56', '0000-00-00 00:00:00', '2016-08-28 09:59:36'),
(5, '57beb3205963d', 'Limani Akhter', 'Lima23', 'check@profile.com', 'asdf1234', '57c3ffd2a2ff5395071_300x300.jpeg', 1, 1, 0, '2016-08-25 10:58:08', '2016-08-29 11:40:29', '0000-00-00 00:00:00'),
(7, '57c06a1407e1c', '', 'asdfmehes', 'mehedi.thedue@gmail.com', 'asdf', '', 1, 1, 1, '2016-08-26 06:11:00', '0000-00-00 00:00:00', '2016-08-28 10:10:24'),
(8, '57c0788f79058', 'Aadsf Asdf', 'nup', 'asdf@fdsa.com', 'asdf', '57c0788f78f82photo.jpg', 1, 1, 1, '2016-08-26 07:12:47', '0000-00-00 00:00:00', '2016-08-29 11:55:53'),
(9, '57c078e3d07ad', 'Asdf Asdf', 'adf3233', 'ads@aa.com', 'asdf1234', '57c3feea5ea1bimages.png', 1, 1, 0, '2016-08-26 07:14:11', '2016-08-29 11:22:50', '0000-00-00 00:00:00'),
(10, '57c1d94fd9c93', 'Sumon Aslamii', 'sumon12', 'sumon@aslasm.coms', 'asdf1234', '57c3367d158fffututrestep-generic-male-176x264.jpg', 1, 2, 0, '2016-08-27 08:17:51', '2016-08-28 09:07:44', '0000-00-00 00:00:00'),
(11, '57c1daa02863c', 'Aslam Habib', 'aslam65a', 'aslam@habib.com', 'asdf1234', '57c32fb7bfac7silhouette-woman.png', 1, 1, 0, '2016-08-27 08:23:28', '2016-08-28 08:38:47', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_trainer_lab_mapping`
--
ALTER TABLE `course_trainer_lab_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labinfo`
--
ALTER TABLE `labinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainers`
--
ALTER TABLE `trainers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `course_trainer_lab_mapping`
--
ALTER TABLE `course_trainer_lab_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `labinfo`
--
ALTER TABLE `labinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `trainers`
--
ALTER TABLE `trainers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
