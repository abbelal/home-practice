<?php 
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
	require_once('header.php');
	require_once('include/db.php');
	$db = new db();
?>


			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Deposit</a>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>View Deposit</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php
							$num_day = date('t');
							$cur_date = date('Y-m');
							$db->sql = "select sum(amount) as amount from collection where collection_date LIKE '{$cur_date}-%'";
							if($res = $db->query($db->sql)){
								if($data =  $res->fetch_assoc()){
									echo "Total Amount = ".$data['amount'];
								}
							}
						?>
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
							<thead>
								<tr>
									<th>Name</th>
									<th>Amount</th>
									<th>Month/Year</th>
								</tr>
							</thead>
							<?php
	  
									$db->sql = "select users.name, sum(collection.amount) as amount, collection.user_id from collection inner join users 
											on collection.user_id = users.id where collection_date LIKE '{$cur_date}-%' group by user_id"; 
			
									if($res = $db->query($db->sql)){
										$fancyid = 0;
										while($data =  $res->fetch_assoc()){
											$fancyid++;
											echo "<tr>
												<td>{$data['name']}</td>";
							?>
								<td>
										<a id="inline" href="#data<?php echo $fancyid; ?>"><?php echo $data['amount']; ?></a>
										<div style="display:none; width: 700px;">
											<div id="data<?php echo $fancyid; ?>">
											<table class="table">
												<tr>
													<th>SL NO</th>
													<th>AMOUNT</th>
													<th>DATE</th>
												</tr>
											<?php
												
												$db->sql = "select amount, collection_date from collection 
														where user_id = {$data['user_id']} and collection_date LIKE '{$cur_date}-%'";
													if($allres = $db->query($db->sql)) {
														$sl = 0;
														while($amounthistory =  $allres->fetch_assoc()){
															$sl++;
															echo "<tr>
																<td>{$sl}</td>
																<td>{$amounthistory['amount']}</td>
																<td>{$amounthistory['collection_date']}</td>
															</tr>";
														}
													}
												
												?>	
											</table>
										</div>
									</div>
									<?php
										echo "</td>";
										echo "<td>{$cur_date}</td>";
										echo '</tr>';
									}
								}
							?>
							</table>
					</div>
				</div>
			</div>
				

    
<?php
	}
	include('footer.php'); 
?>