<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>conditional statement</title>
    </head>
    <body style="text-align:center;">
        <h1>if condition<br/>
            <?php
            $a = 10;
            $b = 20;

            if ($a < 10) {
                echo "welcome";
            } else {
                echo "not welcome";
            }
            ?>
        </h1>

        <h1> if-else if condition<br/>
            <?php
            $a = 10;
            $b = 20;

            if ($a < 10) {
                echo "welcome";
            } else if ($b > 30) {
                echo "most welcome";
            } else {
                echo "nothing";
            }
            ?>
        </h1>

        <h1> if-else if condition<br/>
            <?php
            $t = date("H");
            echo $t.'<br/>';
            if ($t < "20") {
                echo "Have a good day!";
            } else {
                echo "Have a good night!";
            }
            ?>
        </h1>

    </body>
</html>
