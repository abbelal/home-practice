<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\assign\Assign;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objAssignUpdate = new Assign();


$objAssignUpdate -> prepare($_POST);


$objAssignUpdate -> validate();

$objAssignUpdate -> date_validate();

$objAssignUpdate -> update_assign_info();
